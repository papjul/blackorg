<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Twig;

use App\Entity\Album;
use App\Entity\CaseEntity;
use App\Entity\Chapter;
use App\Entity\Character;
use App\Entity\Contact;
use App\Entity\Dvd;
use App\Entity\Episode;
use App\Entity\Gadget;
use App\Entity\Situation;
use App\Entity\SituationType;
use App\Entity\Submission;
use App\Entity\SubmittableEntityInterface;
use App\Entity\Track;
use App\Entity\Volume;
use App\Submission\SubmissionFormHandler;
use App\Util\Markdown;
use Doctrine\ORM\EntityManagerInterface;
use IntlDateFormatter;
use Locale;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class AppExtension.
 */
class AppExtension extends AbstractExtension
{
    /**
     * AppExtension constructor.
     */
    public function __construct(private readonly Markdown $parser, private readonly UrlGeneratorInterface $urlGenerator, private readonly EntityManagerInterface $entityManager, private readonly SubmissionFormHandler $submissionFormHandler)
    {
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('add_leading_zero', $this->addLeadingZero(...)),
            new TwigFilter('markdown_to_html', $this->markdownToHtml(...), ['is_safe' => ['html']]),
            new TwigFilter('rubify', $this->rubify(...), ['is_safe' => ['html']]),
            new TwigFilter('show_difference', $this->showDifference(...)),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('case_css_class', $this->caseCssClass(...)),
            new TwigFunction('file_exists', 'file_exists'),
            new TwigFunction('get_admin_entity_path', $this->getAdminEntityPath(...)),
            new TwigFunction('get_entity_path', $this->getEntityPath(...)),
            new TwigFunction('get_album_path', $this->getAlbumPath(...)),
            new TwigFunction('get_case_path', $this->getCasePath(...)),
            new TwigFunction('get_chapter_path', $this->getChapterPath(...)),
            new TwigFunction('get_character_path', $this->getCharacterPath(...)),
            new TwigFunction('get_dvd_path', $this->getDvdPath(...)),
            new TwigFunction('get_episode_path', $this->getEpisodePath(...)),
            new TwigFunction('get_gadget_path', $this->getGadgetPath(...)),
            new TwigFunction('get_situation_path', $this->getSituationPath(...)),
            new TwigFunction('get_track_path', $this->getTrackPath(...)),
            new TwigFunction('get_volume_path', $this->getVolumePath(...)),
            new TwigFunction('get_pending_contact_messages_count', [$this->entityManager->getRepository(Contact::class), 'getPendingsCount']),
            new TwigFunction('get_pending_submissions_count', [$this->entityManager->getRepository(Submission::class), 'getPendingsCount']),
        ];
    }

    /**
     * Add leading zeros to a number, if necessary.
     *
     *
     * @var int $threshold Threshold for adding leading zeros (number of digits
     *          that will prevent the adding of additional zeros)
     * @var int The number to add leading zeros
     */
    public function addLeadingZero($value, int $threshold = 2): string
    {
        return sprintf('%0' . $threshold . 's', $value);
    }

    /**
     * @param $content
     * @param string $method ('text' or 'inline')
     */
    public function markdownToHtml($content, string $method = 'text'): string
    {
        return $this->parser->toHtml(strval($content), $method);
    }

    /**
     * @param $content
     */
    public function rubify($content): string
    {
        return $this->parser->toFurigana(strval($content));
    }

    /**
     * @param $element
     */
    public function showDifference($element): mixed
    {
        if (is_object($element)) {
            if ('DateTime' == $element::class) {
                $intl = new IntlDateFormatter(Locale::getDefault(), IntlDateFormatter::SHORT, IntlDateFormatter::NONE);

                return $intl->format($element);
            } elseif (\Doctrine\ORM\PersistentCollection::class == $element::class || \Doctrine\Common\Collections\ArrayCollection::class == $element::class) {
                $array = [];
                foreach ($element as $entity) {
                    if (is_a($entity, SituationType::class)) {
                        $array[] = $entity;
                    } else {
                        $entityPath = $this->getEntityPath($entity);
                        if (null !== $entityPath) {
                            $array[] = '[' . $entity . '](' . $entityPath . ')';
                        } else {
                            $array[] = $entity;
                        }
                    }
                }

                return $array;
            } elseif (method_exists($element, '__toString')) {
                return $element->__toString();
            } else {
                return 'Impossible d’afficher l’élément de type ' . $element::class;
            }
        } elseif (is_array($element)) {
            return $element;
        } elseif (is_bool($element)) {
            return $element ? 'Oui' : 'Non';
        } else {
            return $element;
        }
    }

    public function getEntityPath(SubmittableEntityInterface $submittableEntity): ?string
    {
        if (is_a($submittableEntity, CaseEntity::class)) {
            return $this->getCasePath($submittableEntity);
        } elseif (is_a($submittableEntity, Album::class)) {
            return $this->getAlbumPath($submittableEntity);
        } elseif (is_a($submittableEntity, Chapter::class)) {
            return $this->getChapterPath($submittableEntity);
        } elseif (is_a($submittableEntity, Character::class)) {
            return $this->getCharacterPath($submittableEntity);
        } elseif (is_a($submittableEntity, Dvd::class)) {
            return $this->getDvdPath($submittableEntity);
        } elseif (is_a($submittableEntity, Episode::class)) {
            return $this->getEpisodePath($submittableEntity);
        } elseif (is_a($submittableEntity, Gadget::class)) {
            return $this->getGadgetPath($submittableEntity);
        } elseif (is_a($submittableEntity, Situation::class)) {
            return $this->getSituationPath($submittableEntity);
        } elseif (is_a($submittableEntity, Track::class)) {
            return $this->getTrackPath($submittableEntity);
        } elseif (is_a($submittableEntity, Volume::class)) {
            return $this->getVolumePath($submittableEntity);
        } else {
            return null;
        }
    }

    public function getCasePath(CaseEntity $case): ?string
    {
        if ('manga' == $case->getType()) {
            return $this->urlGenerator->generate('manga_case_view', ['number' => $case->getNumber()]);
        } elseif ('anime' == $case->getType()) {
            return $this->urlGenerator->generate('anime_case_view', ['number' => $case->getNumber()]);
        } elseif ('movie' == $case->getType()) {
            return $this->urlGenerator->generate('movie_view', ['number' => $case->getNumber()]);
        } elseif ('movie_special' == $case->getType()) {
            return $this->urlGenerator->generate('movie_special_view', ['number' => $case->getNumber()]);
        } elseif ('ova' == $case->getType()) {
            return $this->urlGenerator->generate('ova_view', ['number' => $case->getNumber()]);
        } elseif ('magic_file' == $case->getType()) {
            return $this->urlGenerator->generate('magic_file_view', ['number' => $case->getNumber()]);
        } elseif ('dc_special' == $case->getType()) {
            return $this->urlGenerator->generate('dc_special_view', ['number' => $case->getNumber()]);
        } elseif ('mk_special' == $case->getType()) {
            return $this->urlGenerator->generate('mk_special_view', ['number' => $case->getNumber()]);
        } elseif ('mk1412' == $case->getType()) {
            return $this->urlGenerator->generate('mk1412_view', ['number' => $case->getNumber()]);
        } elseif ('tanpenshu' == $case->getType()) {
            return $this->urlGenerator->generate('tanpenshu_view', ['number' => $case->getNumber()]);
        } elseif ('drama_special' == $case->getType()) {
            return $this->urlGenerator->generate('drama_special_view', ['number' => $case->getNumber()]);
        } elseif ('drama_episode' == $case->getType()) {
            return $this->urlGenerator->generate('drama_episode_view', ['number' => $case->getNumber()]);
        } elseif ('wps' == $case->getType()) {
            return $this->urlGenerator->generate('wps_case_view', ['number' => $case->getNumber()]);
        } else {
            return null;
        }
    }

    public function getAlbumPath(Album $album): ?string
    {
        if ('opening' === $album->getType() || 'ending' === $album->getType() || 'movie' === $album->getType()) {
            return $this->urlGenerator->generate('music');
        } else {
            return $this->urlGenerator->generate('album', ['id' => $album->getId()]);
        }
    }

    public function getChapterPath(Chapter $chapter): ?string
    {
        if (null !== $chapter->getVolume()) {
            return $this->getVolumePath($chapter->getVolume()) . '#ch' . $chapter->getNumber();
        } else {
            return $this->urlGenerator->generate('chapters') . '#ch' . $chapter->getNumber();
        }
    }

    public function getVolumePath(Volume $volume): ?string
    {
        if ('original' === $volume->getType()) {
            return $this->urlGenerator->generate('volume_original_view', ['number' => $volume->getNumber()]);
        } elseif ('tokubetsu' === $volume->getType()) {
            return $this->urlGenerator->generate('volume_tokubetsu_view', ['number' => $volume->getNumber()]);
        } elseif ('movie' === $volume->getType()) {
            return $this->urlGenerator->generate('volume_movie_view', ['number' => $volume->getNumber()]);
        } elseif ('wps' === $volume->getType()) {
            return $this->urlGenerator->generate('volume_wps_view', ['number' => $volume->getNumber()]);
        } else {
            return null;
        }
    }

    public function getCharacterPath(Character $character): ?string
    {
        return $this->urlGenerator->generate('character_view', ['shortname' => $character->getShortname()]);
    }

    public function getDvdPath(Dvd $dvd): ?string
    {
        return $this->urlGenerator->generate('dvd_view', ['part' => $dvd->getPart()]) . '#vol' . $dvd->getNumber();
    }

    public function getEpisodePath(Episode $episode): ?string
    {
        return $this->urlGenerator->generate('episode_view', ['number' => $episode->getNumber()]);
    }

    public function getGadgetPath(Gadget $gadget): ?string
    {
        return $this->urlGenerator->generate('gadget_view', ['id' => $gadget->getId()]);
    }

    public function getSituationPath(Situation $situation): ?string
    {
        return $this->getCasePath($situation->getCase()) . '#situation' . $situation->getNumber();
    }

    public function getTrackPath(Track $track): ?string
    {
        if ('opening' === $track->getAlbum()->getType()) {
            return $this->getAlbumPath($track->getAlbum()) . '#op' . $track->getNumber();
        } elseif ('ending' === $track->getAlbum()->getType()) {
            return $this->getAlbumPath($track->getAlbum()) . '#ed' . $track->getNumber();
        } elseif ('movie' === $track->getAlbum()->getType()) {
            return $this->getAlbumPath($track->getAlbum()) . '#movie' . $track->getNumber();
        } else {
            return $this->getAlbumPath($track->getAlbum()) . '#track' . $track->getNumber();
        }
    }

    public function getAdminEntityPath(string $type): ?string
    {
        $section = $this->entityManager->getRepository($this->submissionFormHandler->getEntityFQCN($type))->getRouteSectionName();
        return $this->urlGenerator->generate('admin_section', ['section' => $section]);
    }

    public function caseCssClass(CaseEntity $case): string
    {
        if ('manga' == $case->getType() || 'wps' == $case->getType()) {
            return 'success';
        } elseif ('anime' == $case->getType()) {
            return 'danger';
        } elseif ('movie' == $case->getType() || 'movie_special' == $case->getType()) {
            return 'primary';
        } elseif ('ova' == $case->getType() || 'magic_file' == $case->getType() || 'dc_special' == $case->getType()
            || 'mk_special' == $case->getType() || 'mk1412' == $case->getType() || 'tanpenshu' == $case->getType()) {
            return 'warning';
        } elseif ('drama_special' == $case->getType() || 'drama_episode' == $case->getType()) {
            return 'secondary';
        } else {
            return '';
        }
    }

    /**
     * Returns the canonical name of this helper.
     *
     * @return string The canonical name
     *
     * @api
     */
    public function getName(): string
    {
        return 'layout';
    }
}

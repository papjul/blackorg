<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Form;

use App\Entity\CaseEntity;
use App\Entity\Dvd;
use App\Entity\Episode;
use App\Entity\EpisodePending;
use App\Entity\Track;
use App\Form\Type\MarkdownType;
use ReflectionMethod;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EpisodeType.
 */
class EpisodeFormType extends AbstractSubmittableEntityFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('number', IntegerType::class, [
            'label' => 'episodes_number',
            'required' => true,
            'trim' => true,
            'attr' => ['maxlength' => 4, 'min' => 1, 'max' => 9999],
        ])
            ->add('season', ChoiceType::class, [
                'label' => 'episodes_season',
                'choices' => $options['entity_manager']->getRepository(Episode::class)->getEpisodeSeasons(),
                'required' => true,
            ])
            ->add('date', DateType::class, [
                'label' => 'episodes_date',
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('date_remastered', DateType::class, [
                'label' => 'episodes_date_remastered',
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('rating', NumberType::class, [
                'label' => 'episodes_rating',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 5, 'min' => 0, 'max' => 99, 'step' => 0.01],
            ])
            ->add('next_hint', TextType::class, [
                'label' => 'episodes_next_hint',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Indice donné à la fin de l’épisode. Laisser vide si pas encore connu'],
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 50],
            ])
            ->add('title_fr', TextType::class, [
                'label' => 'episodes_title_fr',
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('part', ChoiceType::class, [
                'label' => 'episodes_part',
                'choices' => array_flip($options['entity_manager']->getRepository(Episode::class)->getEpisodeParts()),
                'placeholder' => 'Épisode unique',
                'required' => false,
            ])
            ->add('part_title', TextType::class, [
                'label' => 'episodes_part_title',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 50],
            ])
            ->add('title_jp_romaji', TextType::class, [
                'label' => 'episodes_title_jp_romaji',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Conformément à la politique du site, les titres doivent respecter la méthode Hepburn, avec utilisation de macrons pour les lettres accentuées.'],
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('title_jp_kanji', TextType::class, [
                'label' => 'episodes_title_jp_kanji',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Merci d’ajouter des furigana si possible.'],
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('dvd', EntityType::class, [
                'label' => 'episodes_dvd',
                'class' => Dvd::class,
                'placeholder' => '—',
                'choices' => $options['entity_manager']->getRepository(Dvd::class)->findBy([], ['part' => 'ASC', 'number' => 'ASC']),
                'choice_value' => 'id',
                'choice_label' => 'displayName',
                'required' => false,
            ])
            ->add('adn_id', IntegerType::class, [
                'label' => 'episodes_adn_id',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Identifiant Anime Digital Network si existant (sinon laisser vide), numéro à relever sur l’URL https://animedigitalnetwork.fr/video/detective-conan/XXXX'],
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 5, 'min' => 1, 'max' => 99999],
            ])
            ->add('special', CheckboxType::class, [
                'label' => 'episodes_special',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Épisode de durée supérieure (40 minutes ou plus)'],
                'required' => false,
            ])
            ->add('opening', EntityType::class, [
                'label' => 'episodes_opening',
                'class' => Track::class,
                'placeholder' => '—',
                'choices' => $options['entity_manager']->getRepository(Track::class)->getOpenings(),
                'choice_value' => 'id',
                'required' => false,
            ])
            ->add('ending', EntityType::class, [
                'label' => 'episodes_ending',
                'class' => Track::class,
                'placeholder' => '—',
                'choices' => $options['entity_manager']->getRepository(Track::class)->getEndings(),
                'choice_value' => 'id',
                'required' => false,
            ])
            ->add('cases', EntityType::class, [
                'label' => 'episodes_cases',
                'class' => CaseEntity::class,
                'placeholder' => '—',
                'choices' => $options['entity_manager']->getRepository(CaseEntity::class)->findAll(),
                'choice_value' => 'id',
                'choice_label' => 'displayName',
                'multiple' => true,
                'required' => false,
            ])
            ->add('summary', MarkdownType::class, [
                'label' => 'episodes_summary',
                'required' => false,
                'trim' => true,
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function ($event) use ($options) {
                $data = $event->getData();
                $form = $event->getForm();

                if (null === $data) {
                    return;
                }

                if ('add' == $options['page_action']) {
                    $reflectionMethod = new ReflectionMethod($options['data_class'], 'getCurrent');
                    $repository = $reflectionMethod->getReturnType()->getName();
                    $oEntity = $options['entity_manager']->getRepository($repository)->findOneBy(['number' => intval($data->getNumber())]);
                    if (!is_null($oEntity)) {
                        $form->get('number')->addError(new FormError('Il existe déjà un épisode avec ce numéro.'));
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', EpisodePending::class);
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Form;

use App\Entity\Volume;
use App\Entity\VolumePending;
use App\Form\Type\MarkdownType;
use ReflectionMethod;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VolumeForm.
 */
class VolumeFormType extends AbstractSubmittableEntityFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('number', IntegerType::class, [
            'label' => 'volumes_number',
            'required' => true,
            'trim' => true,
            'attr' => ['maxlength' => 3, 'min' => 1, 'max' => 999],
        ])
            ->add('type', ChoiceType::class, [
                'label' => 'volumes_type',
                'choices' => array_flip($options['entity_manager']->getRepository(Volume::class)->getVolumeTypes()),
                'required' => true,
            ])
            ->add('date_jp', DateType::class, [
                'label' => 'volumes_date_jp',
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('date_fr', DateType::class, [
                'label' => 'volumes_date_fr',
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('summary', MarkdownType::class, [
                'label' => 'volumes_summary',
                'required' => false,
                'trim' => true,
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function ($event) use ($options) {
                $data = $event->getData();
                $form = $event->getForm();

                if (null === $data) {
                    return;
                }

                $reflectionMethod = new ReflectionMethod($options['data_class'], 'getCurrent');
                $repository = $reflectionMethod->getReturnType()->getName();
                $oEntity = $options['entity_manager']->getRepository($repository)->findOneBy(['number' => intval($data->getNumber()), 'type' => $data->getType()]);
                if (('add' == $options['page_action'] && !is_null($oEntity)) || ('edit' == $options['page_action'] && !is_null($oEntity) && $oEntity->getId() != $options['page_id'])) {
                    $form->get('number')->addError(new FormError('Il existe déjà un tome de ce type avec ce numéro.'));
                    $form->get('type')->addError(new FormError('Il existe déjà un tome de ce type avec ce numéro.'));
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', VolumePending::class);
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Form;

use App\Entity\Album;
use App\Entity\TrackPending;
use ReflectionMethod;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TrackType.
 */
class TrackFormType extends AbstractSubmittableEntityFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('number', IntegerType::class, [
            'label' => 'tracks_number',
            'required' => true,
            'trim' => true,
            'attr' => ['maxlength' => 3, 'min' => 1, 'max' => 999],
        ])
            ->add('album', EntityType::class, [
                'label' => 'tracks_album',
                'class' => Album::class,
                'choices' => $options['entity_manager']->getRepository(Album::class)->findAll(),
                'choice_value' => 'id',
                'choice_label' => 'name',
                'required' => true,
            ])
            ->add('time', TextType::class, [
                'label' => 'tracks_time',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 5],
            ])
            ->add('date', DateType::class, [
                'label' => 'tracks_date',
                'widget' => 'single_text',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Date de sortie en version complète (si applicable à ce type de musique)'],
                'required' => false,
            ])
            ->add('date_first_use', DateType::class, [
                'label' => 'tracks_date_first_use',
                'widget' => 'single_text',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Date de première utilisation dans l’anime (uniquement opening/ending)'],
                'required' => false,
            ])
            ->add('artist', TextType::class, [
                'label' => 'tracks_artist',
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('title_jp', TextType::class, [
                'label' => 'tracks_title_jp',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Conformément à la politique du site, les titres doivent respecter la méthode Hepburn, avec utilisation de macrons pour les lettres accentuées'],
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('title_en', TextType::class, [
                'label' => 'tracks_title_en',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('title_fr', TextType::class, [
                'label' => 'tracks_title_fr',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('youtube_id', TextType::class, [
                'label' => 'tracks_youtube_id',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'L’ID d’une vidéo YouTube correspond à XXXXXXXXXXX dans https://www.youtube.com/watch?v=XXXXXXXXXXX'],
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 11],
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function ($event) use ($options) {
                $data = $event->getData();
                $form = $event->getForm();

                if (null === $data) {
                    return;
                }

                $reflectionMethod = new ReflectionMethod($options['data_class'], 'getCurrent');
                $repository = $reflectionMethod->getReturnType()->getName();
                $oEntity = $options['entity_manager']->getRepository($repository)->findOneBy(['album' => intval($data->getAlbum()->getId()), 'number' => intval($data->getNumber())]);
                if (('add' == $options['page_action'] && !is_null($oEntity)) || ('edit' == $options['page_action'] && !is_null($oEntity) && $oEntity->getId() != $options['page_id'])) {
                    $form->get('album')->addError(new FormError('Il existe déjà une musique avec ce numéro de piste dans cet album.'));
                    $form->get('number')->addError(new FormError('Il existe déjà une musique avec ce numéro de piste dans cet album.'));
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', TrackPending::class);
    }
}

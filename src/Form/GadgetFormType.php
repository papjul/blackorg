<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Form;

use App\Entity\CaseEntity;
use App\Entity\Character;
use App\Entity\GadgetPending;
use App\Form\Type\MarkdownType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GadgetType.
 */
class GadgetFormType extends AbstractSubmittableEntityFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('complete_name_fr', TextType::class, [
            'label' => 'gadgets_complete_name_fr',
            'required' => true,
            'trim' => true,
            'attr' => ['maxlength' => 100],
        ])
            ->add('name_fr', TextType::class, [
                'label' => 'gadgets_name_fr',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Utilisé dans l’affichage des gadgets en page de recherche par exemple'],
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('name_jp', TextType::class, [
                'label' => 'gadgets_name_jp',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Conformément à la politique du site, les titres doivent respecter la méthode Hepburn, avec utilisation de macrons pour les lettres accentuées'],
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('created_by', EntityType::class, [
                'label' => 'gadgets_created_by',
                'class' => Character::class,
                'choices' => $options['entity_manager']->getRepository(Character::class)->findAll(),
                'choice_value' => 'id',
                'choice_label' => 'htmlOptionLabel',
                'group_by' => 'characterCategory.displayName',
                'required' => false,
            ])
            ->add('first_use', EntityType::class, [
                'label' => 'gadgets_first_use',
                'class' => CaseEntity::class,
                'choices' => $options['entity_manager']->getRepository(CaseEntity::class)->findAll(),
                'choice_value' => 'id',
                'choice_label' => 'displayName',
                'required' => true,
            ])
            ->add('shortdesc', MarkdownType::class, [
                'label' => 'gadgets_shortdesc',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 255],])
            ->add('desc', MarkdownType::class, [
                'label' => 'gadgets_desc',
                'required' => false,
                'trim' => true,
            ])
            ->add('used_by', EntityType::class, [
                'label' => 'gadgets_used_by',
                'class' => Character::class,
                'choices' => $options['entity_manager']->getRepository(Character::class)->findAll(),
                //'expanded' => true, // TODO: Make this an option
                'multiple' => true,
                'required' => false,
                'choice_value' => 'id',
                'choice_label' => 'htmlOptionLabel',
                'group_by' => 'characterCategory.displayName',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', GadgetPending::class);
    }
}

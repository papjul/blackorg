<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Form;

use App\Entity\DvdPending;
use ReflectionMethod;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DvdFormType.
 */
class DvdFormType extends AbstractSubmittableEntityFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('part', IntegerType::class, [
            'label' => 'dvds_part',
            'required' => true,
            'trim' => true,
            'attr' => ['maxlength' => 2, 'min' => 1, 'max' => 99],
        ])
            ->add('number', IntegerType::class, [
                'label' => 'dvds_number',
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 2, 'min' => 1, 'max' => 99],
            ])
            ->add('ref', TextType::class, [
                'label' => 'dvds_ref',
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 15],
            ])
            ->add('date', DateType::class, [
                'label' => 'dvds_date',
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('price', MoneyType::class, [
                'currency' => 'JPY',
                'label' => 'dvds_price',
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 5, 'min' => 1, 'max' => 99999],
            ])
            ->add('ttc', CheckboxType::class, [
                'label' => 'dvds_ttc',
                'required' => false,
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function ($event) use ($options) {
                $data = $event->getData();
                $form = $event->getForm();

                if (null === $data) {
                    return;
                }

                $reflectionMethod = new ReflectionMethod($options['data_class'], 'getCurrent');
                $repository = $reflectionMethod->getReturnType()->getName();
                $oEntity = $options['entity_manager']->getRepository($repository)->findOneBy(['part' => intval($data->getPart()), 'number' => intval($data->getNumber())]);
                if (('add' == $options['page_action'] && !is_null($oEntity)) || ('edit' == $options['page_action'] && !is_null($oEntity) && $oEntity->getId() != $options['page_id'])) {
                    $form->get('number')->addError(new FormError('Il existe déjà un DVD avec cette partie et ce numéro.'));
                    $form->get('part')->addError(new FormError('Il existe déjà un DVD avec cette partie et ce numéro.'));
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', DvdPending::class);
    }
}

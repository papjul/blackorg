<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Form;

use App\Entity\CaseEntity;
use App\Entity\CasePending;
use App\Entity\Character;
use App\Entity\Gadget;
use App\Form\Type\MarkdownType;
use ReflectionMethod;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CaseType.
 */
class CaseFormType extends AbstractSubmittableEntityFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('number', IntegerType::class, [
            'label' => 'cases_number',
            'required' => true,
            'trim' => true,
            'attr' => ['maxlength' => 4, 'min' => 1, 'max' => 9999],
        ])
            ->add('title', TextType::class, [
                'label' => 'cases_title',
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'cases_type',
                'choices' => array_flip($options['entity_manager']->getRepository(CaseEntity::class)->getCaseTypes()),
                'required' => true,
            ])
            ->add('arc', ChoiceType::class, [
                'label' => 'cases_arc',
                'choices' => array_flip($options['entity_manager']->getRepository(CaseEntity::class)->getArcs()),
                'required' => true,
            ])
            ->add('date', DateType::class, [
                'label' => 'cases_date',
                'widget' => 'single_text',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Date à laquelle cette affaire a été découverte le plus tôt, date de prépublication pour un chapitre, date de diffusion pour un épisode/film, etc.'],
                'required' => true,])
            ->add('summary', MarkdownType::class, [
                'label' => 'cases_summary',
                'required' => false,
                'trim' => true,])
            ->add('plot_chardev', MarkdownType::class, [
                'label' => 'cases_plot_chardev',
                'required' => false,
                'trim' => true,
            ])
            ->add('plot_romance', MarkdownType::class, [
                'label' => 'cases_plot_romance',
                'required' => false,
                'trim' => true,
            ])
            ->add('plot_org', MarkdownType::class, [
                'label' => 'cases_plot_org',
                'required' => false,
                'trim' => true,
            ])
            ->add('plot_essential', MarkdownType::class, [
                'label' => 'cases_plot_essential',
                'required' => false,
                'trim' => true,
            ])
            ->add('plot_recommended', MarkdownType::class, [
                'label' => 'cases_plot_recommended',
                'required' => false,
                'trim' => true,
            ])
            ->add('plot_minor', MarkdownType::class, [
                'label' => 'cases_plot_minor',
                'required' => false,
                'trim' => true,
            ])
            ->add('gadgets', EntityType::class, [
                'label' => 'cases_gadgets',
                'class' => Gadget::class,
                'choices' => $options['entity_manager']->getRepository(Gadget::class)->findAll(),
                //'expanded' => true, // TODO: Make this an option
                'multiple' => true,
                'required' => false,
                'choice_value' => 'id',
                'choice_label' => 'htmlOptionLabel',
                'group_by' => 'createdBy.displayName',
            ])
            ->add('characters', EntityType::class, [
                'label' => 'cases_characters',
                'class' => Character::class,
                'choices' => $options['entity_manager']->getRepository(Character::class)->findAll(),
                //'expanded' => true, // TODO: Make this an option
                'multiple' => true,
                'required' => false,
                'choice_value' => 'id',
                'choice_label' => 'htmlOptionLabel',
                'group_by' => 'characterCategory.displayName',
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function ($event) use ($options) {
                $data = $event->getData();
                $form = $event->getForm();

                if (null === $data) {
                    return;
                }

                if ('add' == $options['page_action']) {
                    $reflectionMethod = new ReflectionMethod($options['data_class'], 'getCurrent');
                    $repository = $reflectionMethod->getReturnType()->getName();
                    $oEntity = $options['entity_manager']->getRepository($repository)->findOneBy(['number' => intval($data->getNumber()), 'type' => $data->getType()]);
                    if (!is_null($oEntity)) {
                        $form->get('number')->addError(new FormError('Il existe déjà une affaire de ce type avec ce numéro.'));
                        $form->get('type')->addError(new FormError('Il existe déjà une affaire de ce type avec ce numéro.'));
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', CasePending::class);
    }
}

<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Validator\Constraints\NoBannedWords;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class RegistrationFormType.
 */
class RegistrationFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'Nom d’utilisateur',
                    'constraints' => [new NoBannedWords()],
                ]
            )
            ->add('email', EmailType::class, [
                'label' => 'Adresse de courriel',
                'constraints' => [new NotBlank()],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped' => false,
                'options' => [
                    'attr' => [
                        'autocomplete' => 'new-password',
                    ],
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci de fournir un mot de passe',
                    ]),
                    new Length([
                        'min' => 8,
                        'minMessage' => 'Ton mot de passe doit être d’au moins {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
                'first_options' => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Mot de passe (confirmation)'],
                'invalid_message' => 'Les mots de passe ne correspondent pas',
            ])
            ->add('acceptedCla', CheckboxType::class, [
                'label' => 'J’accepte l’<a href="/cla">accord de licence de contributeur</a>.',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Pour des raisons légales, tu dois accepter.',
                    ]),
                ],
            ])
            ->add('acceptedPrivacy', CheckboxType::class, [
                'label' => 'J’accepte la <a href="/privacy">politique de confidentialité</a>.',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Pour des raisons légales, tu dois accepter.',
                    ]),
                ],
            ])
            ->add('content', TextareaType::class, [
                'attr' => ['class' => 'honeypot'],
                'constraints' => [new Blank()], // Honeypot
                'required' => false,
                'mapped' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'latest_privacy' => null,
            'latest_cla' => null,
        ]);
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Form;

use App\Entity\CaseEntity;
use App\Entity\SituationLocation;
use App\Entity\SituationPending;
use App\Entity\SituationType;
use App\Form\Type\MarkdownType;
use ReflectionMethod;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SituationType.
 */
class SituationFormType extends AbstractSubmittableEntityFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('number', IntegerType::class, [
            'label' => 'situations_number',
            'required' => true,
            'trim' => true,
            'attr' => ['maxlength' => 1, 'min' => 1, 'max' => 9],
        ])
            ->add('title', TextType::class, [
                'label' => 'Titre (si plusieurs situations)',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('case', EntityType::class, [
                'label' => 'situations_case',
                'class' => CaseEntity::class,
                'choices' => $options['entity_manager']->getRepository(CaseEntity::class)->findAll(),
                'choice_value' => 'id',
                'choice_label' => 'displayName',
                'required' => true,
            ])
            ->add('types', EntityType::class, [
                'label' => 'situations_types',
                'class' => SituationType::class,
                'choices' => $options['entity_manager']->getRepository(SituationType::class)->findBy([], ['order' => 'ASC']),
                'multiple' => true,
                'required' => false,
                'choice_value' => 'id',
                'choice_label' => 'name',
                'by_reference' => false,
            ])
            ->add('causeOfDeath', TextType::class, [
                'label' => 'situations_causeOfDeath',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('location', EntityType::class, [
                'label' => 'situations_location',
                'class' => SituationLocation::class,
                'choices' => $options['entity_manager']->getRepository(SituationLocation::class)->findBy([], ['order' => 'ASC']),
                'choice_value' => 'id',
                'choice_label' => 'name',
                'required' => false,
            ])
            ->add('scene', TextType::class, [
                'label' => 'situations_scene',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('suspects', CollectionType::class, [
                'label' => 'situations_suspects',
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('victims', CollectionType::class, [
                'label' => 'situations_victims',
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('culprits', CollectionType::class, [
                'label' => 'situations_culprits',
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('summary', MarkdownType::class, [
                'label' => 'situations_summary',
                'required' => false,
                'trim' => true,
            ])
            ->add('resolutionExplanations', MarkdownType::class, [
                'label' => 'situations_resolutionExplanations',
                'required' => false,
                'trim' => true,
            ])
            ->add('resolutionEvidences', MarkdownType::class, [
                'label' => 'situations_resolutionEvidences',
                'required' => false,
                'trim' => true,
            ])
            ->add('resolutionMotive', MarkdownType::class, [
                'label' => 'situations_resolutionMotive',
                'required' => false,
                'trim' => true,
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function ($event) use ($options) {
                $data = $event->getData();
                $form = $event->getForm();

                if (null === $data) {
                    return;
                }

                $reflectionMethod = new ReflectionMethod($options['data_class'], 'getCurrent');
                $repository = $reflectionMethod->getReturnType()->getName();
                $existingSituation = $options['entity_manager']->getRepository($repository)->findOneBy(['case' => intval($data->getCase()->getId()), 'number' => intval($data->getNumber())]);
                if (('add' == $options['page_action'] && !is_null($existingSituation)) || ('edit' == $options['page_action'] && !is_null($existingSituation) && $existingSituation->getId() != $options['page_id'])) {
                    $form->get('case')->addError(new FormError('Il existe déjà une situation avec ce numéro dans cette affaire.'));
                    $form->get('number')->addError(new FormError('Il existe déjà une situation avec ce numéro dans cette affaire.'));
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', SituationPending::class);
    }
}

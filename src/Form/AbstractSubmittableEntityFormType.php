<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AbstractSubmittableEntityFormType extended by all FormType of submittable entities.
 */
abstract class AbstractSubmittableEntityFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('entity_manager');
        $resolver->setDefaults([
            'page_action' => null,
            'page_id' => null,
        ]);
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Form;

use App\Entity\CaseEntity;
use App\Entity\Character;
use App\Entity\CharacterCategory;
use App\Entity\CharacterPending;
use App\Form\Type\MarkdownType;
use ReflectionMethod;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CharacterType.
 */
class CharacterFormType extends AbstractSubmittableEntityFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('firstname', TextType::class, [
            'label' => 'characters_firstname',
            'required' => false,
            'trim' => true,
            'attr' => ['maxlength' => 20],
        ])
            ->add('lastname', TextType::class, [
                'label' => 'characters_lastname',
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 20],
            ])
            ->add('shortname', TextType::class, [
                'label' => 'characters_shortname',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Nom utilisé pour accéder à la page dans l‘URL. Exemple : Shin’ichi Kudō devient Shinichi_Kudo.'],
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 40],
            ])
            ->add('aliases', TextType::class, [
                'label' => 'characters_aliases',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Séparés par des virgules'],
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('gender', ChoiceType::class, [
                'label' => 'characters_gender',
                'choices' => array_flip($options['entity_manager']->getRepository(Character::class)->getGenders()),
                'required' => true,
            ])
            ->add('age', TextType::class, [
                'label' => 'characters_age',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 10],
            ])
            ->add('height', IntegerType::class, [
                'label' => 'characters_height',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 3, 'min' => 1, 'max' => 250],
            ])
            ->add('weight', IntegerType::class, [
                'label' => 'characters_weight',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 3, 'min' => 1, 'max' => 500],
            ])
            ->add('occupation', TextType::class, [
                'label' => 'characters_occupation',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('character_category', EntityType::class, [
                'label' => 'characters_character_category',
                'class' => CharacterCategory::class,
                'choice_value' => 'id',
                'choice_label' => 'formName',
                'choices' => $options['entity_manager']->getRepository(CharacterCategory::class)->findAll(),
                'required' => true,
            ])
            ->add('first_appearance', EntityType::class, [
                'label' => 'characters_first_appearance',
                'class' => CaseEntity::class,
                'placeholder' => '—',
                'choices' => $options['entity_manager']->getRepository(CaseEntity::class)->findAll(),
                'choice_value' => 'id',
                'choice_label' => 'displayName',
                'required' => false,
            ])
            ->add('voice_jp', TextType::class, [
                'label' => 'characters_voice_jp',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('voice_fr', TextType::class, [
                'label' => 'characters_voice_fr',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('shortdesc', MarkdownType::class, [
                'label' => 'characters_shortdesc',
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 255],])
            ->add('desc', MarkdownType::class, [
                'label' => 'characters_desc',
                'required' => false,
                'trim' => true,
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function ($event) use ($options) {
                $data = $event->getData();
                $form = $event->getForm();

                if (null === $data) {
                    return;
                }

                $reflectionMethod = new ReflectionMethod($options['data_class'], 'getCurrent');
                $repository = $reflectionMethod->getReturnType()->getName();
                if ('add' == $options['page_action']) {
                    if (isset($data->firstname)) {
                        $oEntity = $options['entity_manager']->getRepository($repository)->findOneBy(['firstname' => $data->getFirstname(), 'lastname' => $data->getLastname()]);
                        if (!is_null($oEntity)) {
                            $form->get('firstname')->addError(new FormError('Il existe déjà un personnage avec ce prénom et ce nom.'));
                            $form->get('lastname')->addError(new FormError('Il existe déjà un personnage avec ce prénom et ce nom.'));
                        }
                    } else {
                        $oEntityNull = $options['entity_manager']->getRepository($repository)->findOneBy(['firstname' => null, 'lastname' => $data->getLastname()]);
                        $oEntityEmpty = $options['entity_manager']->getRepository($repository)->findOneBy(['firstname' => '', 'lastname' => $data->getLastname()]);
                        if (!is_null($oEntityNull) || !is_null($oEntityEmpty)) {
                            $form->get('lastname')->addError(new FormError('Il existe déjà un personnage avec ce nom.'));
                        }
                    }
                }

                $oEntity = $options['entity_manager']->getRepository($repository)->findOneBy(['shortname' => $data->getShortname()]);
                if (('add' == $options['page_action'] && !is_null($oEntity)) || ('edit' == $options['page_action'] && !is_null($oEntity) && $oEntity->getId() != $options['page_id'])) {
                    $form->get('shortname')->addError(new FormError('Il existe déjà un personnage avec ce nom épuré.'));
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', CharacterPending::class);
    }
}

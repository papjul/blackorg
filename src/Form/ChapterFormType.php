<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Form;

use App\Entity\CaseEntity;
use App\Entity\ChapterPending;
use App\Entity\Volume;
use App\Entity\VolumeAbstract;
use ReflectionMethod;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ChapterType.
 */
class ChapterFormType extends AbstractSubmittableEntityFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('number', IntegerType::class, [
            'label' => 'chapters_number',
            'required' => true,
            'trim' => true,
            'attr' => ['maxlength' => 4, 'min' => 1, 'max' => 9999],
        ])
            ->add('volume', EntityType::class, [
                'label' => 'chapters_volume',
                'class' => Volume::class,
                'choices' => $options['entity_manager']->getRepository(Volume::class)->findBy([], ['type' => 'ASC', 'number' => 'ASC']),
                'placeholder' => 'Chapitre pré-publié',
                'choice_value' => 'id',
                'choice_label' => 'displayNumber',
                'required' => false,
            ])
            ->add('volume_number', ChoiceType::class, [
                'label' => 'chapters_volume_number',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Ordre du chapitre dans le tome. Obligatoire si un tome est sélectionné.'],
                'choices' => $options['entity_manager']->getRepository(Volume::class)->getVolumeNumbers(),
                'required' => false,
            ])
            ->add('case', EntityType::class, [
                'label' => 'chapters_case',
                'class' => CaseEntity::class,
                'placeholder' => '—',
                'choices' => $options['entity_manager']->getRepository(CaseEntity::class)->findAll(),
                'choice_value' => 'id',
                'choice_label' => 'displayName',
                'required' => false,
            ])
            ->add('title_fr', TextType::class, [
                'label' => 'chapters_title_fr',
                'required' => true,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('title_jp_romaji', TextType::class, [
                'label' => 'chapters_title_jp_romaji',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Conformément à la politique du site, les titres doivent respecter la méthode Hepburn, avec utilisation de macrons pour les lettres accentuées.'],
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('title_jp_kanji', TextType::class, [
                'label' => 'chapters_title_jp_kanji',
                'label_attr' => ['data-bs-toggle' => 'tooltip', 'title' => 'Merci d’ajouter des furigana si possible.'],
                'required' => false,
                'trim' => true,
                'attr' => ['maxlength' => 100],
            ])
            ->add('date', DateType::class, [
                'label' => 'chapters_date',
                'widget' => 'single_text',
                'required' => false,
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function ($event) use ($options) {
                $data = $event->getData();
                $form = $event->getForm();

                if (null === $data) {
                    return;
                }

                if ('add' == $options['page_action'] || !is_null($data->getVolume())) {
                    $reflectionMethod = new ReflectionMethod($options['data_class'], 'getCurrent');
                    $repository = $reflectionMethod->getReturnType()->getName();
                    if ('add' == $options['page_action']) {
                        $oEntity = $options['entity_manager']->getRepository($repository)->findOneBy(['number' => intval($data->getNumber())]);
                        if (!is_null($oEntity)
                            && (
                                (
                                    ($oEntity->getVolume() === null || $oEntity->getVolume()->getType() == VolumeAbstract::TYPE_MANGA)
                                    && ($data->getVolume() === null || $data->getVolume()->getType() == VolumeAbstract::TYPE_MANGA)
                                )
                                || (
                                    $oEntity->getVolume() !== null
                                    && $data->getVolume() !== null
                                    && $oEntity->getVolume()->getType() === $data->getVolume()->getType()
                                )
                            )
                        ) {
                            $form->get('number')->addError(new FormError('Il existe déjà un chapitre avec ce numéro.'));
                        }
                    }
                    if (!is_null($data->getVolume())) {
                        if (0 == intval($data->getVolumeNumber())) {
                            $form->get('volume_number')->addError(new FormError('L’ordre du chapitre dans le tome est manquant.'));
                        } else {
                            $oEntity = $options['entity_manager']->getRepository($repository)->findOneBy(['volume' => intval($data->getVolume()->getId()), 'volume_number' => intval($data->getVolumeNumber())]);
                            if (('add' == $options['page_action'] && !is_null($oEntity)) || ('edit' == $options['page_action'] && !is_null($oEntity) && $oEntity->getId() != $options['page_id'])) {
                                $form->get('volume')->addError(new FormError('Il existe déjà un chapitre avec cet ordre de numéro dans ce tome.'));
                                $form->get('volume_number')->addError(new FormError('Il existe déjà un chapitre avec cet ordre de numéro dans ce tome.'));
                            }
                        }
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', ChapterPending::class);
    }
}

<?php

declare(strict_types=1);

namespace App\Form;

use App\Form\Type\MarkdownType;
use App\Validator\Constraints\NoHtmlLinks;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Class SubmissionType.
 */
class SubmissionFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('embedded_form', $options['form_type'])
            ->add('comm', MarkdownType::class, [
                'label' => 'Remarques éventuelles concernant ta suggestion (explications, sources)',
                'constraints' => [new NoHtmlLinks(), new Length(['max' => 255])],
                'required' => false,
                'mapped' => false,
                'trim' => true,])
            ->add('message', TextareaType::class, [
                'attr' => ['class' => 'honeypot'],
                'constraints' => [new Blank()], // Honeypot
                'required' => false,
                'mapped' => false,
            ]);

        if ('valid' == $options['page_action']) {
            $builder->add('reason', MarkdownType::class, [
                'label' => 'Commentaire du validateur (raison du refus par exemple)',
                'constraints' => [new Length(['max' => 255])],
                'attr' => ['class' => 'success'],
                'required' => false,
                'mapped' => false,
                'trim' => true,])
                ->add('submit', SubmitType::class, ['label' => 'Valider la suggestion', 'attr' => ['class' => 'success']])
                ->add('update', SubmitType::class, ['label' => 'Enregistrer les modifications'])
                ->add('reject', SubmitType::class, ['label' => 'Rejeter la suggestion', 'attr' => ['class' => 'alert']]);
        } else {
            $builder->add('submit', SubmitType::class, ['label' => 'Soumettre à validation']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('page_action');
        $resolver->setRequired('form_type');
        $resolver->setDefaults([
            'page_action' => null,
            'page_id' => null,
        ]);
    }
}

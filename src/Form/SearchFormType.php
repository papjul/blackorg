<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\CaseEntity;
use App\Entity\Character;
use App\Entity\Gadget;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchFormType.
 */
class SearchFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->setMethod('GET')
            ->add('type', ChoiceType::class, [
                'label' => 'cases_type',
                'choices' => array_flip($options['entity_manager']->getRepository(CaseEntity::class)->getCaseTypes()),
                'multiple' => true,
                'required' => false,])
            ->add('arc', ChoiceType::class, [
                'label' => 'cases_arc',
                'choices' => array_flip($options['entity_manager']->getRepository(CaseEntity::class)->getArcs()),
                'multiple' => true,
                'required' => false,])
            ->add('plot', ChoiceType::class, [
                'label' => 'Plot',
                'choices' => [
                    //'Première apparition d’un ou plusieurs personnages' => 'firstappearance',
                    'Développement d’un ou plusieurs personnages' => 'chardev',
                    'Développement d’une ou plusieurs relations amoureuses' => 'romance',
                    'Développement de l’intrigue relative à l’Organisation' => 'org',
                ],
                'multiple' => true,
                'required' => false,])
            ->add('gadgets', ChoiceType::class, [
                'label' => 'cases_gadgets',
                'choices' => $options['entity_manager']->getRepository(Gadget::class)->findAll(),
                //'expanded' => true, // TODO: Make this an option
                'multiple' => true,
                'required' => false,
                'choice_value' => 'id',
                'choice_label' => 'htmlOptionLabel',
                'group_by' => 'createdBy.displayName',])
            ->add('characters', ChoiceType::class, [
                'label' => 'cases_characters',
                'choices' => $options['entity_manager']->getRepository(Character::class)->findAll(),
                //'expanded' => true, // TODO: Make this an option
                'multiple' => true,
                'required' => false,
                'choice_value' => 'id',
                'choice_label' => 'htmlOptionLabel',
                'group_by' => 'characterCategory.displayName',]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('entity_manager');
    }
}

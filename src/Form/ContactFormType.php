<?php

declare(strict_types=1);

namespace App\Form;

use App\Form\Type\MarkdownType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * Class ContactFormType.
 */
class ContactFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('identity', TextType::class, [
                'label' => 'Identité',
                'required' => true,
                'attr' => ['placeholder' => 'Nom sous lequel apparaître', 'maxlength' => 50],])
            ->add('email', EmailType::class, [
                'label' => 'Adresse',
                'required' => false,
                'attr' => ['placeholder' => 'Courriel pour te recontacter', 'maxlength' => 50],])
            ->add('subject', TextType::class, [
                'label' => 'Objet',
                'required' => true,
                'attr' => ['placeholder' => 'Objet de ton message', 'maxlength' => 50],])
            ->add('message', MarkdownType::class, [
                'label' => 'Message',
                'required' => false,
                'attr' => ['placeholder' => 'Ton message'],
                'trim' => true,])
            ->add('content', TextareaType::class, [
                'attr' => ['class' => 'honeypot'],
                'constraints' => [new Blank()], // Honeypot
                'required' => false,
                'mapped' => false,
            ])
            ->add('agreePrivacyPolicy', CheckboxType::class, [
                'label' => 'J’accepte la <a href="/privacy">politique de confidentialité</a> de Black Org.',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Pour des raisons légales, tu dois accepter.',
                    ]),
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}

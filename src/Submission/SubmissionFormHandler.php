<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Submission;

use App\Entity\PendingInterface;
use App\Entity\Submission;
use App\Entity\User;
use App\Form\Type\MarkdownType;
use App\Validator\Constraints\NoHtmlLinks;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class SubmissionFormHandler.
 */
class SubmissionFormHandler
{
    protected array $sectionToEntity = [
        'albums' => 'Album',
        'cases' => 'CaseEntity',
        'chapters' => 'Chapter',
        'characters' => 'Character',
        'dvds' => 'Dvd',
        'episodes' => 'Episode',
        'gadgets' => 'Gadget',
        'situations' => 'Situation',
        'tracks' => 'Track',
        'volumes' => 'Volume',
    ];

    /**
     * SubmissionFormHandler constructor.
     */
    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly FormFactoryInterface $formFactory, private readonly TranslatorInterface $translator)
    {
    }

    /**
     * @param $section
     */
    public function getEntity($section): ?string
    {
        return array_key_exists($section, $this->sectionToEntity) ? $this->sectionToEntity[$section] : null;
    }

    /**
     * @param $entityName
     */
    public function getPendingEntityFQCN($entityName): string
    {
        return ('CaseEntity' == $entityName) ? \App\Entity\CasePending::class : 'App\Entity\\' . $entityName . 'Pending';
    }

    /**
     * Convert a list of entity Objects to a list of IDs.
     */
    public function entitiesToIds(?iterable $entities): array
    {
        $ids = [];
        if ($entities === null ? 0 : count($entities)) {
            foreach ($entities as $entity) {
                $ids[] = $entity->getId();
            }
        }
        sort($ids);

        return $ids;
    }

    /**
     *
     * @return FormInterface
     */
    public function getForm(string $entityFQCN, ?PendingInterface $pendingEntity, string $action, ?int $id, ?UserInterface $user)
    {
        $formType = $this->entityManager->getRepository($entityFQCN)->getFormClass();
        $formParent = $this->formFactory->createBuilder($formType, $pendingEntity, ['entity_manager' => $this->entityManager, 'page_action' => $action, 'page_id' => $id])
            ->add('comm', MarkdownType::class, [
                'label' => 'Remarques éventuelles concernant ta suggestion (explications, sources)',
                'constraints' => [new NoHtmlLinks(), new Length(['max' => 255])],
                'required' => false,
                'mapped' => false,
                'trim' => true,
            ])
            ->add('message', TextareaType::class, [
                'attr' => ['class' => 'honeypot'],
                'constraints' => [new Blank()], // Honeypot
                'required' => false,
                'mapped' => false,
            ]);

        if (!$user) {
            $formParent->add('agreeClaAndPrivacyPolicy', CheckboxType::class, [
                'label' => 'J’accepte l’<a href="/cla">accord de licence de contributeur</a>, et la <a href="/privacy">politique de confidentialité</a>.',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Pour des raisons légales, tu dois accepter.',
                    ]),
                ],
            ]);
        }

        if ('valid' == $action) {
            $formParent->add('reason', MarkdownType::class, [
                'label' => 'Commentaire du validateur (raison du refus par exemple)',
                'constraints' => [new Length(['max' => 255])],
                'attr' => ['class' => 'success'],
                'required' => false,
                'mapped' => false,
                'trim' => true,])
                ->add('submit', SubmitType::class, ['label' => 'Valider la suggestion', 'attr' => ['class' => 'btn-success']])
                ->add('update', SubmitType::class, ['label' => 'Enregistrer les modifications'])
                ->add('reject', SubmitType::class, ['label' => 'Rejeter la suggestion', 'attr' => ['class' => 'btn-danger']]);
        } else {
            $formParent->add('submit', SubmitType::class, ['label' => 'Soumettre à validation']);
        }

        return $formParent->getForm();
    }

    /**
     * @return string
     *
     */
    public function handleValidFormSubmit(FormInterface $form, Submission $submission, User $user)
    {
        $submission->setReason($form->get('reason')->getData());

        if ($form->get('reject')->isClicked()) {
            $this->entityManager->getRepository(Submission::class)->deleteSubmission($submission, $user);

            return 'Cette suggestion a bien été refusée.';
        } elseif ($form->get('submit')->isClicked()) {
            $entityFQCN = $this->getEntityFQCN($submission->getType());
            if (null == $submission->getPending()->getCurrent()) {
                $this->entityManager->getRepository($entityFQCN)->createEntity($submission->getPending());
            } else {
                $this->entityManager->getRepository($entityFQCN)->updateEntity($submission->getPending());
            }
            $this->entityManager->getRepository(Submission::class)->validateSubmission($submission, $user);

            return 'Cette suggestion a bien été validée.';
        } else {
            $this->entityManager->getRepository(Submission::class)->updateSubmission($submission);

            return 'Les modifications ont bien été enregistrées.';
        }
    }

    /**
     * @param $entityName
     */
    public function getEntityFQCN($entityName): string
    {
        return 'App\Entity\\' . $entityName;
    }
}

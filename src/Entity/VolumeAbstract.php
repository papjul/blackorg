<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Abstract class for real entity and pending entities.
 */
class VolumeAbstract implements \Stringable
{
    public const TYPE_MANGA = 'original';
    public const TYPE_SPECIAL = 'tokubetsu';
    public const TYPE_MOVIE = 'movie';
    public const TYPE_WPS = 'wps';

    /**
     * @var int|null
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    protected ?int $id = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 9, nullable: false)]
    #[Assert\NotBlank]
    protected ?string $type = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 3)]
    #[Assert\Range(min: 1, max: 999)]
    protected ?int $number = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'date', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Type('\DateTime')]
    protected ?DateTime $date_jp = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'date', nullable: true)]
    #[Assert\Type('\DateTime')]
    protected ?DateTime $date_fr = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $summary = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return VolumeAbstract
     * @return VolumeAbstract
     */
    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getDisplayNumber();
    }

    /**
     * @return string|null
     */
    public function getDisplayNumber(): ?string
    {
        return ((Volume::TYPE_WPS == $this->type) ? 'WPS ' : '') . ((Volume::TYPE_MOVIE == $this->type) ? 'Film ' : '') . ((Volume::TYPE_SPECIAL == $this->type) ? 'Spécial ' : '') . $this->number;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @return VolumeAbstract
     * @return VolumeAbstract
     */
    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateJp(): ?DateTime
    {
        return $this->date_jp;
    }

    /**
     * @return VolumeAbstract
     * @return VolumeAbstract
     */
    public function setDateJp(?DateTime $date_jp): self
    {
        $this->date_jp = $date_jp;

        return $this;
    }

    public function getDateFr(): ?DateTime
    {
        return $this->date_fr;
    }

    public function setDateFr(?DateTime $date_fr): self
    {
        $this->date_fr = $date_fr;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }
}

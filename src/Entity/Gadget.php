<?php

declare (strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Metadata\ApiFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
#[ApiResource(operations: [new Get(), new GetCollection()])]
#[ApiFilter(filterClass: PropertyFilter::class)]
#[ApiResource(uriTemplate: '/cases/{id}/gadgets.{_format}', uriVariables: ['id' => new Link(fromClass: \App\Entity\CaseEntity::class, identifiers: ['id'], fromProperty: 'gadgets')], status: 200, filters: ['annotated_app_entity_gadget_api_platform_serializer_filter_property_filter'], operations: [new GetCollection()])]
#[ApiResource(uriTemplate: '/characters/{id}/gadgets.{_format}', uriVariables: ['id' => new Link(fromClass: \App\Entity\Character::class, identifiers: ['id'], fromProperty: 'gadgets')], status: 200, filters: ['annotated_app_entity_gadget_api_platform_serializer_filter_property_filter'], operations: [new GetCollection()])]
#[ORM\Table(name: 'gadgets', options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity(repositoryClass: \App\Repository\GadgetRepository::class)]
class Gadget extends GadgetAbstract implements SubmittableEntityInterface
{
    use SubmittableEntityTrait;
    /**
     * @var CaseEntity|null
     */
    #[ORM\ManyToOne(targetEntity: 'CaseEntity', inversedBy: 'first_uses')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    protected ?CaseEntity $first_use = null;
    /**
     * @var Character|null
     */
    #[ORM\ManyToOne(targetEntity: 'Character', inversedBy: 'gadgets_invented')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Character $created_by = null;
    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: 'Character', mappedBy: 'gadgets')]
    protected $used_by;
    /**
     * @var \Collection
     */
    #[ORM\ManyToMany(targetEntity: 'CaseEntity', mappedBy: 'gadgets')]
    protected $cases;
    /**
     * Gadget constructor.
     */
    public function __construct()
    {
        $this->used_by = new ArrayCollection();
        $this->cases = new ArrayCollection();
    }
    public function getFirstUse() : ?CaseEntity
    {
        return $this->first_use;
    }
    public function setFirstUse(?CaseEntity $first_use) : self
    {
        $this->first_use = $first_use;
        return $this;
    }
    public function getCreatedBy() : ?Character
    {
        return $this->created_by;
    }
    public function setCreatedBy(?Character $created_by) : self
    {
        $this->created_by = $created_by;
        return $this;
    }
    public function getUsedBy() : Collection
    {
        return $this->used_by;
    }
    public function addUsedBy(Character $character) : self
    {
        $this->used_by->add($character);
        $character->addGadget($this);
        return $this;
    }
    public function removeUsedBy(Character $character) : self
    {
        $this->used_by->removeElement($character);
        $character->removeGadget($this);
        return $this;
    }
    public function getCases() : Collection
    {
        return $this->cases;
    }
    public function addCase(CaseEntity $case) : self
    {
        $this->cases[] = $case;
        return $this;
    }
    public function removeCase(CaseEntity $case) : self
    {
        $this->cases->removeElement($case);
        return $this;
    }
}

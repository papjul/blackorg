<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table(options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity]
class SituationSolvedBy
{
    /**
     * @var Situation|null
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: 'Situation', inversedBy: 'solvedBy')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: false)]
    #[Assert\NotBlank]
    protected ?Situation $situation = null;

    /**
     * @var Character|null
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: 'Character')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: false)]
    #[Assert\NotBlank]
    protected ?Character $character = null;

    /**
     * @var Character|null
     */
    #[ORM\ManyToOne(targetEntity: 'Character')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    #[Assert\NotBlank]
    protected ?Character $characterVia = null;

    /**
     * @var int|null
     */
    #[ORM\Column(name: '`order`', type: 'integer', nullable: true)]
    protected ?int $order = null;

    public function getSituation(): ?Situation
    {
        return $this->situation;
    }

    public function setSituation(?Situation $situation): self
    {
        $this->situation = $situation;

        return $this;
    }

    public function getCharacter(): ?Character
    {
        return $this->character;
    }

    public function setCharacter(?Character $character): self
    {
        $this->character = $character;

        return $this;
    }

    public function getCharacterVia(): ?Character
    {
        return $this->characterVia;
    }

    public function setCharacterVia(?Character $characterVia): self
    {
        $this->characterVia = $characterVia;

        return $this;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(?int $order): self
    {
        $this->order = $order;

        return $this;
    }
}

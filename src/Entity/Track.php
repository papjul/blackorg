<?php

declare (strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Metadata\ApiFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
#[ApiResource(operations: [new Get(), new GetCollection()])]
#[ApiFilter(filterClass: PropertyFilter::class)]
#[ApiResource(uriTemplate: '/albums/{id}/tracks.{_format}', uriVariables: ['id' => new Link(fromClass: \App\Entity\Album::class, identifiers: ['id'], toProperty: 'album')], status: 200, filters: ['annotated_app_entity_track_api_platform_serializer_filter_property_filter'], operations: [new GetCollection()])]
#[ORM\Table(name: 'tracks', options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity(repositoryClass: \App\Repository\TrackRepository::class)]
class Track extends TrackAbstract implements SubmittableEntityInterface
{
    use SubmittableEntityTrait;
    /**
     * @var Album|null
     */
    #[ORM\ManyToOne(targetEntity: 'Album', inversedBy: 'tracks')]
    #[ORM\JoinColumn(name: 'album_id', referencedColumnName: 'id', nullable: false)]
    #[Assert\NotBlank]
    protected ?Album $album = null;
    /**
     * @var Collection
     */
    #[ORM\OneToMany(targetEntity: 'Episode', mappedBy: 'opening')]
    #[ORM\OrderBy(['number' => 'ASC'])]
    protected $episodesOp;
    /**
     * @var Collection
     */
    #[ORM\OneToMany(targetEntity: 'Episode', mappedBy: 'ending')]
    #[ORM\OrderBy(['number' => 'ASC'])]
    protected $episodesEd;
    /**
     * Track constructor.
     */
    public function __construct()
    {
        $this->episodesOp = new ArrayCollection();
        $this->episodesEd = new ArrayCollection();
    }
    public function getAlbum() : ?Album
    {
        return $this->album;
    }
    public function setAlbum(?Album $album) : self
    {
        $this->album = $album;
        return $this;
    }
    public function getEpisodesOp() : Collection
    {
        return $this->episodesOp;
    }
    public function getEpisodesEd() : Collection
    {
        return $this->episodesEd;
    }
}

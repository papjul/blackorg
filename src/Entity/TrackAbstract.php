<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Abstract class for real entity and pending entities.
 */
class TrackAbstract implements \Stringable
{
    /**
     * @var int|null
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    protected ?int $id = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 3)]
    #[Assert\Range(min: 1, max: 999)]
    protected ?int $number = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    protected ?string $artist = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(max: 100)]
    protected ?string $title_fr = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(max: 100)]
    protected ?string $title_en = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    protected ?string $title_jp = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'date', nullable: true)]
    #[Assert\Type('\DateTime')]
    protected ?DateTime $date = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'date', nullable: true)]
    #[Assert\Type('\DateTime')]
    protected ?DateTime $date_first_use = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 5, nullable: true)]
    #[Assert\Length(max: 5)]
    protected ?string $time = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 11, nullable: true)]
    #[Assert\Length(max: 11)]
    protected ?string $youtube_id = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('%02s', $this->number) . ' — ' . $this->title_jp;
    }

    /**
     * @return string|null
     */
    public function getDisplayName(): ?string
    {
        return $this->title_jp;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @return TrackAbstract
     * @return TrackAbstract
     */
    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getArtist(): ?string
    {
        return $this->artist;
    }

    /**
     * @return TrackAbstract
     * @return TrackAbstract
     */
    public function setArtist(?string $artist): self
    {
        $this->artist = $artist;

        return $this;
    }

    public function getTitleFr(): ?string
    {
        return $this->title_fr;
    }

    public function setTitleFr(?string $title_fr): self
    {
        $this->title_fr = $title_fr;

        return $this;
    }

    public function getTitleEn(): ?string
    {
        return $this->title_en;
    }

    public function setTitleEn(?string $title_en): self
    {
        $this->title_en = $title_en;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitleJp(): ?string
    {
        return $this->title_jp;
    }

    /**
     * @return TrackAbstract
     * @return TrackAbstract
     */
    public function setTitleJp(?string $title_jp): self
    {
        $this->title_jp = $title_jp;

        return $this;
    }

    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    public function setDate(?DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return DateTime|null date
     */
    public function getDateFirstUse(): ?DateTime
    {
        return $this->date_first_use;
    }

    public function setDateFirstUse(?DateTime $date_first_use): self
    {
        $this->date_first_use = $date_first_use;

        return $this;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(?string $time): self
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getYoutubeId(): ?string
    {
        return $this->youtube_id;
    }

    /**
     * @return TrackAbstract
     * @return TrackAbstract
     */
    public function setYoutubeId(?string $youtube_id): self
    {
        $this->youtube_id = $youtube_id;

        return $this;
    }
}

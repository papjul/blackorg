<?php
declare (strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Metadata\ApiFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
#[ApiResource(operations: [new Get(), new GetCollection()])]
#[ApiFilter(filterClass: PropertyFilter::class)]
#[ApiResource(uriTemplate: '/cases/{id}/episodes.{_format}', uriVariables: ['id' => new Link(fromClass: \App\Entity\CaseEntity::class, identifiers: ['id'], toProperty: 'cases')], status: 200, filters: ['annotated_app_entity_episode_api_platform_serializer_filter_property_filter'], operations: [new GetCollection()])]
#[ApiResource(uriTemplate: '/dvds/{id}/episodes.{_format}', uriVariables: ['id' => new Link(fromClass: \App\Entity\Dvd::class, identifiers: ['id'], toProperty: 'dvd')], status: 200, filters: ['annotated_app_entity_episode_api_platform_serializer_filter_property_filter'], operations: [new GetCollection()])]
#[ORM\Table(name: 'episodes', options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity(repositoryClass: \App\Repository\EpisodeRepository::class)]
class Episode extends EpisodeAbstract implements SubmittableEntityInterface
{
    use SubmittableEntityTrait;
    /**
     * @var Collection
     */
    #[ORM\JoinTable(name: 'episodes_cases')]
    #[ORM\ManyToMany(targetEntity: 'CaseEntity', inversedBy: 'episodes')]
    protected $cases;
    /**
     * @var Track|null
     */
    #[ORM\ManyToOne(targetEntity: 'Track', inversedBy: 'episodesOp')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Track $opening = null;
    /**
     * @var Track|null
     */
    #[ORM\ManyToOne(targetEntity: 'Track', inversedBy: 'episodesEd')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Track $ending = null;
    /**
     * @var Dvd|null
     */
    #[ORM\ManyToOne(targetEntity: 'Dvd', inversedBy: 'episodes')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Dvd $dvd = null;
    /**
     * Episode constructor.
     */
    public function __construct()
    {
        $this->cases = new ArrayCollection();
    }
    public function getCases() : Collection
    {
        return $this->cases;
    }
    public function addCase(CaseEntity $case) : self
    {
        $this->cases->add($case);
        $case->addEpisode($this);
        return $this;
    }
    public function removeCase(CaseEntity $case) : self
    {
        $this->cases->removeElement($case);
        $case->removeEpisode($this);
        return $this;
    }
    public function getOpening() : ?Track
    {
        return $this->opening;
    }
    public function setOpening(?Track $opening) : self
    {
        $this->opening = $opening;
        return $this;
    }
    public function getEnding() : ?Track
    {
        return $this->ending;
    }
    public function setEnding(?Track $ending) : self
    {
        $this->ending = $ending;
        return $this;
    }
    public function getDvd() : ?Dvd
    {
        return $this->dvd;
    }
    public function setDvd(?Dvd $dvd) : self
    {
        $this->dvd = $dvd;
        return $this;
    }
}

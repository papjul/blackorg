<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table(options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity]
class GadgetPending extends GadgetAbstract implements PendingInterface
{
    /**
     * @var CaseEntity|null
     */
    #[ORM\ManyToOne(targetEntity: 'CaseEntity')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    protected ?CaseEntity $first_use = null;

    /**
     * @var Character|null
     */
    #[ORM\ManyToOne(targetEntity: 'Character')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Character $created_by = null;

    /**
     * @var Collection
     */
    #[ORM\JoinTable(name: 'gadget_used_by_pending')]
    #[ORM\ManyToMany(targetEntity: 'Character')]
    protected $used_by;

    /**
     * Points to the current visible entity.
     * NULL if new entity.
     *
     * @var Gadget|null
     */
    #[ORM\ManyToOne(targetEntity: 'Gadget')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Gadget $current = null;

    /**
     * @var Submission|null
     */
    #[ORM\OneToOne(targetEntity: 'Submission', mappedBy: 'gadgetPending')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Submission $submission = null;

    /**
     * Gadget constructor.
     */
    public function __construct()
    {
        $this->used_by = new ArrayCollection();
    }

    public function getFirstUse(): ?CaseEntity
    {
        return $this->first_use;
    }

    public function setFirstUse(?CaseEntity $first_use): self
    {
        $this->first_use = $first_use;

        return $this;
    }

    public function getCreatedBy(): ?Character
    {
        return $this->created_by;
    }

    public function setCreatedBy(?Character $created_by): self
    {
        $this->created_by = $created_by;

        return $this;
    }

    public function getUsedBy(): Collection
    {
        return $this->used_by;
    }

    public function addUsedBy(Character $character): self
    {
        $this->used_by->add($character);

        return $this;
    }

    public function removeUsedBy(Character $character): self
    {
        $this->used_by->removeElement($character);

        return $this;
    }

    public function getCurrent(): ?Gadget
    {
        return $this->current;
    }

    /**
     * @param Gadget $current
     */
    public function setCurrent(/* @var Gadget */ $current): self
    {
        $this->current = $current;

        return $this;
    }

    public function getSubmission(): ?Submission
    {
        return $this->submission;
    }

    public function setSubmission(?Submission $submission): self
    {
        $this->submission = $submission;

        return $this;
    }
}

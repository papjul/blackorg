<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table(options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity]
class CharacterPending extends CharacterAbstract implements PendingInterface
{
    /**
     * @var CharacterCategory|null
     */
    #[ORM\ManyToOne(targetEntity: 'CharacterCategory')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: false)]
    #[Assert\NotBlank]
    protected ?CharacterCategory $character_category = null;

    /**
     * @var CaseEntity|null
     */
    #[ORM\ManyToOne(targetEntity: 'CaseEntity')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?CaseEntity $first_appearance = null;

    /**
     * Points to the current visible entity.
     * NULL if new entity.
     *
     * @var Character|null
     */
    #[ORM\ManyToOne(targetEntity: 'Character')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Character $current = null;

    /**
     * @var Submission|null
     */
    #[ORM\OneToOne(targetEntity: 'Submission', mappedBy: 'characterPending')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Submission $submission = null;

    public function getFirstAppearance(): ?CaseEntity
    {
        return $this->first_appearance;
    }

    public function setFirstAppearance(?CaseEntity $first_appearance): self
    {
        $this->first_appearance = $first_appearance;

        return $this;
    }

    public function getCharacterCategory(): ?CharacterCategory
    {
        return $this->character_category;
    }

    public function setCharacterCategory(CharacterCategory $character_category): self
    {
        $this->character_category = $character_category;

        return $this;
    }

    public function getCurrent(): ?Character
    {
        return $this->current;
    }

    /**
     * @param Character $current
     */
    public function setCurrent(/* @var Character */ $current): self
    {
        $this->current = $current;

        return $this;
    }

    public function getSubmission(): ?Submission
    {
        return $this->submission;
    }

    public function setSubmission(?Submission $submission): self
    {
        $this->submission = $submission;

        return $this;
    }
}

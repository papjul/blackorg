<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Abstract class for real entity and pending entities.
 */
class CaseAbstract implements \Stringable
{
    /**
     * @var int|null
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    protected ?int $id = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 4)]
    #[Assert\Range(min: 1, max: 9999)]
    protected ?int $number = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false)]
    #[Assert\NotBlank]
    protected ?int $arc = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    protected ?string $title = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 13, nullable: false)]
    #[Assert\NotBlank]
    protected ?string $type = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'date', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Type('\DateTime')]
    protected ?DateTime $date = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $summary = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $plot_chardev = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $plot_romance = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $plot_org = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $plot_essential = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $plot_recommended = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $plot_minor = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) self::getDisplayName();
    }

    /**
     * @param string $format
     *                       'none' is for only the number formatted (shortcut for displayNumber())
     *                       'short' is for image names
     *                       'medium' is for short columns
     *                       'long' is the most common display
     *                       'full' is 'long' with title
     */
    public function getDisplayName(string $format = 'full'): ?string
    {
        if ('none' === $format) {
            return $this->getDisplayNumber();
        }

        $prefix = '';
        if ('long' === $format || 'full' === $format) {
            $prefix = match ($this->type) {
                'manga', 'anime' => 'Affaire n°',
                'movie' => 'Film',
                'movie_special' => 'Film spécial',
                'ova' => 'OVA',
                'magic_file' => 'Magic File',
                'dc_special' => 'Détective Conan spécial',
                'mk_special' => 'Magic Kaito spécial',
                'mk1412' => 'Magic Kaito 1412 épisode',
                'tanpenshu' => 'Recueil d’histoires courtes de Gōshō Aoyama',
                'drama_special' => 'Drama live',
                'drama_episode' => 'Drama épisode',
                'wps' => 'Wild Police Story partie',
                default => '',
            };
            $prefix .= ' ';
        } elseif ('medium' === $format) {
            $prefix = match ($this->type) {
                'movie' => 'Film',
                'movie_special' => 'Film sp.',
                'ova' => 'OVA',
                'magic_file' => 'MF',
                'dc_special' => 'Sp.',
                'mk_special' => 'MKS',
                'mk1412' => 'MK',
                'tanpenshu' => 'Tanpenshū',
                'drama_special' => 'Live',
                'drama_episode' => 'Drama ép.',
                'wps' => 'WPS',
                default => '',
            };
            $prefix .= ' ';
        } elseif ('short' === $format) {
            $prefix = match ($this->type) {
                'movie', 'movie_special' => 'movie',
                'ova' => 'ova',
                'magic_file' => 'mf',
                'dc_special' => 'dcs',
                'mk_special' => 'mks',
                'mk1412' => 'mk',
                'tanpenshu' => 'tanpenshu',
                'drama_special' => 'live',
                'drama_episode' => 'drama',
                'wps' => 'wps',
                default => '',
            };
        }

        $suffix = '';
        if ('long' === $format || 'full' === $format) {
            $suffix = ' ';
            if ('anime' === $this->type) {
                $suffix .= 'HS';
            }
        } elseif ('medium' === $format) {
            $suffix = ' ';
            if ('anime' === $this->type) {
                $suffix .= 'HS';
            }
        } elseif ('short' === $format) {
            $suffix = match ($this->type) {
                'anime' => 'hs',
                'movie_special' => 'sp',
                default => '',
            };
        }

        return $prefix . self::getDisplayNumber() . $suffix . (('full' === $format) ? ' — ' . self::getTitle() : '');
    }

    public function getDisplayNumber(): string
    {
        if ('manga' === $this->type || 'anime' === $this->type) {
            return sprintf('%03s', $this->number);
        } else {
            return strval($this->number);
        }
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getArc(): ?int
    {
        return $this->arc;
    }

    public function setArc(?int $arc): self
    {
        $this->arc = $arc;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    public function setDate(?DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getPlotChardev(): ?string
    {
        return $this->plot_chardev;
    }

    public function setPlotChardev(?string $plot_chardev): self
    {
        $this->plot_chardev = $plot_chardev;

        return $this;
    }

    public function getPlotRomance(): ?string
    {
        return $this->plot_romance;
    }

    public function setPlotRomance(?string $plot_romance): self
    {
        $this->plot_romance = $plot_romance;

        return $this;
    }

    public function getPlotOrg(): ?string
    {
        return $this->plot_org;
    }

    public function setPlotOrg(?string $plot_org): self
    {
        $this->plot_org = $plot_org;

        return $this;
    }

    public function getPlotEssential(): ?string
    {
        return $this->plot_essential;
    }

    public function setPlotEssential(?string $plot_essential): self
    {
        $this->plot_essential = $plot_essential;

        return $this;
    }

    public function getPlotRecommended(): ?string
    {
        return $this->plot_recommended;
    }

    public function setPlotRecommended(?string $plot_recommended): self
    {
        $this->plot_recommended = $plot_recommended;

        return $this;
    }

    public function getPlotMinor(): ?string
    {
        return $this->plot_minor;
    }

    public function setPlotMinor(?string $plot_minor): self
    {
        $this->plot_minor = $plot_minor;

        return $this;
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity]
class ChapterPending extends ChapterAbstract implements PendingInterface
{
    /**
     * @var Volume|null
     */
    #[ORM\ManyToOne(targetEntity: 'Volume')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Volume $volume = null;

    /**
     * @var CaseEntity|null
     */
    #[ORM\ManyToOne(targetEntity: 'CaseEntity')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?CaseEntity $case = null;

    /**
     * Points to the current visible entity.
     * NULL if new entity.
     *
     * @var Chapter|null
     */
    #[ORM\ManyToOne(targetEntity: 'Chapter')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Chapter $current = null;

    /**
     * @var Submission|null
     */
    #[ORM\OneToOne(targetEntity: 'Submission', mappedBy: 'chapterPending')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Submission $submission = null;

    public function getVolume(): ?Volume
    {
        return $this->volume;
    }

    public function setVolume(?Volume $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    public function getCase(): ?CaseEntity
    {
        return $this->case;
    }

    public function setCase(?CaseEntity $case): self
    {
        $this->case = $case;

        return $this;
    }

    public function getCurrent(): ?Chapter
    {
        return $this->current;
    }

    /**
     * @param Chapter $current
     */
    public function setCurrent(/* @var Chapter */ $current): self
    {
        $this->current = $current;

        return $this;
    }

    public function getSubmission(): ?Submission
    {
        return $this->submission;
    }

    public function setSubmission(?Submission $submission): self
    {
        $this->submission = $submission;

        return $this;
    }
}

<?php

declare (strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Metadata\ApiFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
#[ApiResource(operations: [new Get(), new GetCollection()])]
#[ApiFilter(filterClass: PropertyFilter::class)]
#[ApiResource(uriTemplate: '/cases/{id}/characters.{_format}', uriVariables: ['id' => new Link(fromClass: \App\Entity\CaseEntity::class, identifiers: ['id'], fromProperty: 'characters')], status: 200, filters: ['annotated_app_entity_character_api_platform_serializer_filter_property_filter'], operations: [new GetCollection()])]
#[ApiResource(uriTemplate: '/character_categories/{id}/characters.{_format}', uriVariables: ['id' => new Link(fromClass: \App\Entity\CharacterCategory::class, identifiers: ['id'], toProperty: 'character_category')], status: 200, filters: ['annotated_app_entity_character_api_platform_serializer_filter_property_filter'], operations: [new GetCollection()])]
#[ORM\Table(name: 'characters', options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity(repositoryClass: \App\Repository\CharacterRepository::class)]
class Character extends CharacterAbstract implements SubmittableEntityInterface
{
    use SubmittableEntityTrait;
    /**
     * @var CharacterCategory|null
     */
    #[ORM\ManyToOne(targetEntity: 'CharacterCategory', inversedBy: 'characters')]
    #[ORM\JoinColumn(name: 'charactercategory_id', referencedColumnName: 'id', nullable: false)]
    #[Assert\NotBlank]
    protected ?CharacterCategory $character_category = null;
    /**
     * @var CaseEntity|null
     */
    #[ORM\ManyToOne(targetEntity: 'CaseEntity', inversedBy: 'first_appearances')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?CaseEntity $first_appearance = null;
    /**
     * @var \Collection
     */
    #[ORM\ManyToMany(targetEntity: 'CaseEntity', mappedBy: 'characters')]
    protected $cases;
    /**
     * @var \Collection
     */
    #[ORM\JoinTable(name: 'characters_gadgets')]
    #[ORM\ManyToMany(targetEntity: 'Gadget', inversedBy: 'used_by')]
    protected $gadgets;
    /**
     * @var Collection
     */
    #[ORM\OneToMany(targetEntity: 'Gadget', mappedBy: 'created_by')]
    protected $gadgets_invented;
    /**
     * Character constructor.
     */
    public function __construct()
    {
        $this->cases = new ArrayCollection();
        $this->gadgets = new ArrayCollection();
        $this->gadgets_invented = new ArrayCollection();
    }
    public function getFirstAppearance() : ?CaseEntity
    {
        return $this->first_appearance;
    }
    public function setFirstAppearance(?CaseEntity $first_appearance) : self
    {
        $this->first_appearance = $first_appearance;
        return $this;
    }
    public function getCases() : Collection
    {
        return $this->cases;
    }
    public function getGadgetsInvented() : Collection
    {
        return $this->gadgets_invented;
    }
    public function addCase(CaseEntity $case) : self
    {
        $this->cases[] = $case;
        return $this;
    }
    public function removeCase(CaseEntity $case) : self
    {
        $this->cases->removeElement($case);
        return $this;
    }
    public function getGadgets() : Collection
    {
        return $this->gadgets;
    }
    public function addGadget(Gadget $gadget) : self
    {
        $this->gadgets[] = $gadget;
        return $this;
    }
    public function removeGadget(Gadget $gadget) : self
    {
        $this->gadgets->removeElement($gadget);
        return $this;
    }
    public function getCharacterCategory() : ?CharacterCategory
    {
        return $this->character_category;
    }
    public function setCharacterCategory(CharacterCategory $character_category) : self
    {
        $this->character_category = $character_category;
        return $this;
    }
}

<?php
declare (strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use Doctrine\ORM\Mapping as ORM;
#[ApiResource(operations: [new Get(), new GetCollection()])]
#[ApiResource(uriTemplate: '/cases/{id}/chapters.{_format}', uriVariables: ['id' => new Link(fromClass: \App\Entity\CaseEntity::class, identifiers: ['id'], toProperty: 'case')], status: 200, operations: [new GetCollection()])]
#[ApiResource(uriTemplate: '/volumes/{id}/chapters.{_format}', uriVariables: ['id' => new Link(fromClass: \App\Entity\Volume::class, identifiers: ['id'], toProperty: 'volume')], status: 200, operations: [new GetCollection()])]
#[ORM\Table(name: 'chapters', options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity(repositoryClass: \App\Repository\ChapterRepository::class)]
class Chapter extends ChapterAbstract implements SubmittableEntityInterface
{
    use SubmittableEntityTrait;
    /**
     * @var Volume|null
     */
    #[ORM\ManyToOne(targetEntity: 'Volume', inversedBy: 'chapters')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Volume $volume = null;
    /**
     * @var CaseEntity|null
     */
    #[ORM\ManyToOne(targetEntity: 'CaseEntity', inversedBy: 'chapters')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?CaseEntity $case = null;
    public function getVolume() : ?Volume
    {
        return $this->volume;
    }
    public function setVolume(?Volume $volume) : self
    {
        $this->volume = $volume;
        return $this;
    }
    public function getCase() : ?CaseEntity
    {
        return $this->case;
    }
    public function setCase(?CaseEntity $case) : self
    {
        $this->case = $case;
        return $this;
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Abstract class for real entity and pending entities.
 */
class DvdAbstract implements \Stringable
{
    /**
     * @var int|null
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    protected ?int $id = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 2)]
    #[Assert\Range(min: 1, max: 99)]
    protected ?int $part = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 2)]
    #[Assert\Range(min: 1, max: 99)]
    protected ?int $number = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 5)]
    #[Assert\Range(min: 1, max: 99999)]
    protected ?int $price = null;

    /**
     * @var bool|null
     */
    #[ORM\Column(type: 'boolean', nullable: false)]
    protected ?bool $ttc = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 15, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 15)]
    protected ?string $ref = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'date', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Type('\DateTime')]
    protected ?DateTime $date = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getPart(): ?int
    {
        return $this->part;
    }

    /**
     * @return Dvd
     */
    public function setPart(?int $part): self
    {
        $this->part = $part;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getDisplayName();
    }

    /**
     * @return string|null
     */
    public function getDisplayName(): ?string
    {
        return 'Part ' . $this->part . ' — Vol ' . $this->number;
    }

    /**
     * @return string|null
     */
    public function getDisplayNumber(): ?string
    {
        return $this->part . '-' . $this->number;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @return DvdAbstract
     * @return DvdAbstract
     */
    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @return DvdAbstract
     * @return DvdAbstract
     */
    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isTTC(): ?bool
    {
        return $this->ttc;
    }

    /**
     * @param bool $ttc
     * @return DvdAbstract
     * @return DvdAbstract
     */
    public function setTTC(?bool $ttc): self
    {
        $this->ttc = $ttc;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @return DvdAbstract
     * @return DvdAbstract
     */
    public function setRef(?string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @return DvdAbstract
     * @return DvdAbstract
     */
    public function setDate(?DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }
}

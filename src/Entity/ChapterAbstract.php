<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Abstract class for real entity and pending entities.
 */
class ChapterAbstract implements \Stringable
{
    /**
     * @var int|null
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    protected ?int $id = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 4)]
    #[Assert\Range(min: 1, max: 9999)]
    protected ?int $number = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $volume_number = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    protected ?string $title_fr = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(max: 100)]
    protected ?string $title_jp_romaji = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(max: 100)]
    protected ?string $title_jp_kanji = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'date', nullable: true)]
    protected ?DateTime $date = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getDisplayNumber() . ' — ' . $this->getTitleFr();
    }

    /**
     * @return string|null
     */
    public function getDisplayNumber(): ?string
    {
        return sprintf('%03s', $this->number);
    }

    /**
     * @return string|null
     */
    public function getTitleFr(): ?string
    {
        return $this->title_fr;
    }

    /**
     * @return Chapter
     */
    public function setTitleFr(?string $title_fr): self
    {
        $this->title_fr = $title_fr;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @return Chapter
     */
    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getVolumeNumber(): ?int
    {
        return $this->volume_number;
    }

    /**
     * @return Chapter
     */
    public function setVolumeNumber(?int $volume_number): self
    {
        $this->volume_number = $volume_number;

        return $this;
    }

    public function getTitleJpRomaji(): ?string
    {
        return $this->title_jp_romaji;
    }

    /**
     * @return Chapter
     */
    public function setTitleJpRomaji(?string $title_jp_romaji): self
    {
        $this->title_jp_romaji = $title_jp_romaji;

        return $this;
    }

    public function getTitleJpKanji(): ?string
    {
        return $this->title_jp_kanji;
    }

    /**
     * @return Chapter
     */
    public function setTitleJpKanji(?string $title_jp_kanji): self
    {
        $this->title_jp_kanji = $title_jp_kanji;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @return Chapter
     */
    public function setDate(?DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class EpisodeAbstract
 * @package App\Entity
 */
class EpisodeAbstract implements \Stringable
{
    public const NUM_ITEMS = 100;

    /**
     * @var int|null
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    protected ?int $id = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 4)]
    #[Assert\Range(min: 1, max: 9999)]
    protected ?int $number = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false)]
    #[Assert\NotBlank]
    protected ?int $season = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Assert\Range(min: 0, max: 10)]
    protected ?int $part = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Assert\Length(max: 50)]
    protected ?string $part_title = null;

    /**
     * @var bool|null
     */
    #[ORM\Column(type: 'boolean', nullable: false)]
    protected ?bool $special = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    protected ?string $title_fr = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    protected ?string $title_jp_romaji = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(max: 100)]
    protected ?string $title_jp_kanji = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Assert\Length(max: 50)]
    protected ?string $next_hint = null;

    /**
     * @var float|null
     */
    #[ORM\Column(type: 'decimal', precision: 5, scale: 2, nullable: true)]
    #[Assert\Length(max: 5)]
    protected ?float $rating = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'date', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Type('\DateTime')]
    protected ?DateTime $date = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'date', nullable: true)]
    #[Assert\Type('\DateTime')]
    protected ?DateTime $date_remastered = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Assert\Length(max: 5)]
    #[Assert\Range(min: 1, max: 99999)]
    protected ?int $adn_id = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $summary = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getDisplayNumber() . ' — ' . $this->getTitleFr();
    }

    /**
     * @return string|null
     */
    public function getDisplayNumber(): ?string
    {
        return sprintf('%03s', $this->number);
    }

    /**
     * @return string|null
     */
    public function getTitleFr(): ?string
    {
        return $this->title_fr;
    }

    /**
     * @return Episode
     */
    public function setTitleFr(?string $title_fr): self
    {
        $this->title_fr = $title_fr;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @return Episode
     */
    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSeason(): ?int
    {
        return $this->season;
    }

    /**
     * @return Episode
     */
    public function setSeason(?int $season): self
    {
        $this->season = $season;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPart(): ?int
    {
        return $this->part;
    }

    /**
     * @return Episode
     */
    public function setPart(?int $part): self
    {
        $this->part = $part;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPartTitle(): ?string
    {
        return $this->part_title;
    }

    /**
     * @return Episode
     */
    public function setPartTitle(?string $part_title): self
    {
        $this->part_title = $part_title;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isSpecial(): ?bool
    {
        return $this->special;
    }

    /**
     * @param bool $special
     * @return Episode
     */
    public function setSpecial(?bool $special): self
    {
        $this->special = $special;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitleJpRomaji(): ?string
    {
        return $this->title_jp_romaji;
    }

    /**
     * @return Episode
     */
    public function setTitleJpRomaji(?string $title_jp_romaji): self
    {
        $this->title_jp_romaji = $title_jp_romaji;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitleJpKanji(): ?string
    {
        return $this->title_jp_kanji;
    }

    /**
     * @return Episode
     */
    public function setTitleJpKanji(?string $title_jp_kanji): self
    {
        $this->title_jp_kanji = $title_jp_kanji;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNextHint(): ?string
    {
        return $this->next_hint;
    }

    /**
     * @return Episode
     */
    public function setNextHint(?string $next_hint): self
    {
        $this->next_hint = $next_hint;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getRating(): ?float
    {
        return !is_null($this->rating) ? floatval($this->rating) : null;
    }

    /**
     * @return Episode
     */
    public function setRating(?float $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getADNId(): ?int
    {
        return $this->adn_id;
    }

    /**
     * @return Episode
     */
    public function setADNId(?int $adn_id): self
    {
        $this->adn_id = $adn_id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSummary(): ?string
    {
        return !is_null($this->summary) ? trim($this->summary) : null;
    }

    /**
     * @return Episode
     */
    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @return Episode
     */
    public function setDate(?DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateRemastered(): ?DateTime
    {
        return $this->date_remastered;
    }

    /**
     * @return Episode
     */
    public function setDateRemastered(?DateTime $date_remastered): self
    {
        $this->date_remastered = $date_remastered;

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * Interface for pending entities.
 */
interface PendingInterface
{

    public function getCurrent();

    /**
     * @param $current
     */
    public function setCurrent($current);

    /**
     * @return Submission|null
     */
    public function getSubmission(): ?Submission;

    /**
     * @param Submission|null $submission
     */
    public function setSubmission(?Submission $submission);
}

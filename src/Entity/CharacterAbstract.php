<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Abstract class for real entity and pending entities.
 */
class CharacterAbstract implements \Stringable
{
    public const GENDER_FEMALE = 'female';
    public const GENDER_MALE = 'male';
    public const GENDER_OTHER = 'other';

    /**
     * @var int|null
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    protected ?int $id = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 40, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 40)]
    #[Assert\Regex(pattern: '/^[a-z_]+$/i')]
    protected ?string $shortname = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    #[Assert\Length(max: 20)]
    protected ?string $firstname = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 20, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 20)]
    protected ?string $lastname = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(max: 100)]
    protected ?string $aliases = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 6, nullable: false)]
    #[Assert\NotBlank]
    protected ?string $gender = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 10, nullable: true)]
    #[Assert\Length(max: 10)]
    protected ?string $age = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Assert\Length(max: 3)]
    #[Assert\Range(min: 1, max: 250)]
    protected ?int $height = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Assert\Length(max: 3)]
    #[Assert\Range(min: 1, max: 500)]
    protected ?int $weight = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(max: 100)]
    protected ?string $occupation = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(max: 100)]
    protected ?string $voice_fr = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(max: 100)]
    protected ?string $voice_jp = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    protected ?string $shortdesc = null;

    /**
     * @var string|null
     */
    #[ORM\Column(name: '`desc`', type: 'text', nullable: true)]
    protected ?string $desc = null;

    /**
     * @var int|null
     */
    #[ORM\Column(name: '`order`', type: 'integer', nullable: true)]
    protected ?int $order = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): self
    {
        $this->shortname = $shortname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @return CharacterAbstract
     */
    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFormName(): ?string
    {
        return '<img src="/img/character/' . $this->shortname . '_60px.jpg" width="20" height="20" alt="" /> ' . (!empty($this->firstname) ? $this->firstname . ' ' : '') . $this->lastname;
    }

    /**
     * @return string|null
     */
    public function getHtmlOptionLabel(): ?string
    {
        return '/img/character/' . $this->shortname . '_60px.jpg|' . (!empty($this->firstname) ? $this->firstname . ' ' : '') . $this->lastname;
    }

    /**
     * @deprecated
     */
    public function getFullname(): ?string
    {
        return $this->getDisplayName();
    }

    /**
     * @return string|null
     */
    public function getDisplayName(): ?string
    {
        return $this->__toString();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (!empty($this->firstname) ? $this->firstname . ' ' : '') . $this->lastname;
    }

    public function getAliases(): ?string
    {
        return $this->aliases;
    }

    public function setAliases(?string $aliases): self
    {
        $this->aliases = $aliases;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        if (!in_array($gender, [self::GENDER_MALE, self::GENDER_FEMALE, self::GENDER_OTHER])) {
            throw new InvalidArgumentException('Invalid gender');
        }
        $this->gender = $gender;

        return $this;
    }

    public function getAge(): ?string
    {
        return $this->age;
    }

    public function setAge(?string $age): self
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(?int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(?int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getOccupation(): ?string
    {
        return $this->occupation;
    }

    public function setOccupation(?string $occupation): self
    {
        $this->occupation = $occupation;

        return $this;
    }

    public function getVoiceFr(): ?string
    {
        return $this->voice_fr;
    }

    public function setVoiceFr(?string $voice_fr): self
    {
        $this->voice_fr = $voice_fr;

        return $this;
    }

    public function getVoiceJp(): ?string
    {
        return $this->voice_jp;
    }

    public function setVoiceJp(?string $voice_jp): self
    {
        $this->voice_jp = $voice_jp;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShortdesc(): ?string
    {
        return $this->shortdesc;
    }

    /**
     * @return CharacterAbstract
     */
    public function setShortdesc(?string $shortdesc): self
    {
        $this->shortdesc = $shortdesc;

        return $this;
    }

    public function getDesc(): ?string
    {
        return $this->desc;
    }

    public function setDesc(?string $desc): self
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(?int $order): self
    {
        $this->order = $order;

        return $this;
    }
}

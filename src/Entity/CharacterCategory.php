<?php

declare (strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Metadata\ApiFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [new Get(), new GetCollection()])]
#[ApiFilter(filterClass: PropertyFilter::class)]
#[ORM\Table(name: 'characters_categories', options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity(repositoryClass: \App\Repository\CharacterCategoryRepository::class)]
class CharacterCategory implements \Stringable
{
    /**
     * @var int|null
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    protected ?int $id = null;
    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    protected ?string $name = null;
    /**
     * @var \Collection
     */
    #[ORM\OneToMany(targetEntity: 'Character', mappedBy: 'character_category')]
    #[ORM\OrderBy(['order' => 'ASC'])]
    protected $characters;
    /**
     * @var Collection
     */
    #[ORM\OneToMany(targetEntity: 'CharacterCategory', mappedBy: 'parent')]
    private $children;
    /**
     * @var CharacterCategory|null
     */
    #[ORM\ManyToOne(targetEntity: 'CharacterCategory', inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id', nullable: true)]
    private ?CharacterCategory $parent = null;
    /**
     * CharacterCategory constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }
    public function getId() : ?int
    {
        return $this->id;
    }
    /**
     * @return string
     */
    public function __toString() : string
    {
        return (string) $this->getDisplayName();
    }
    public function getDisplayName() : ?string
    {
        return (!is_null($this->parent) ? $this->parent->getName() . ' — ' : '') . $this->name;
    }
    public function getName() : ?string
    {
        return $this->name;
    }
    public function setName(?string $name) : CharacterCategory
    {
        $this->name = $name;
        return $this;
    }
    public function getFormName() : ?string
    {
        $arrows = '>';
        if (!is_null($this->getParent())) {
            $arrows = '>>';
            if (!is_null($this->getParent()->getParent())) {
                $arrows = '>>>';
            }
        }
        return $arrows . ' ' . $this->name;
    }
    public function getParent() : ?CharacterCategory
    {
        return $this->parent;
    }
    public function setParent(?CharacterCategory $parent) : CharacterCategory
    {
        $this->parent = $parent;
        return $this;
    }
    public function getChildren() : Collection
    {
        return $this->children;
    }
    public function getCharacters() : Collection
    {
        return $this->characters;
    }
}

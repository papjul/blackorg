<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Symfony\Component\PropertyAccess\Exception\UnexpectedTypeException;

/**
 * OneToOne relationship table with all pending entities.
 */
#[ORM\Table(name: 'submissions', options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity(repositoryClass: \App\Repository\SubmissionRepository::class)]
class Submission
{
    /**
     * @var int|null
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    protected ?int $id = null;

    /**
     * @var User|null
     */
    #[ORM\ManyToOne(targetEntity: 'User')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?User $user = null;

    /**
     * @var User|null
     */
    #[ORM\ManyToOne(targetEntity: 'User')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?User $userValid = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $comm = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'datetime', nullable: false)]
    protected ?DateTime $dateSubmitted = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    protected ?DateTime $dateValid = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    protected ?string $ip = null;

    /**
     * @var bool|null
     */
    #[ORM\Column(type: 'boolean', nullable: false)]
    protected ?bool $validated = null;

    /**
     * @var bool|null
     */
    #[ORM\Column(type: 'boolean', nullable: false)]
    protected ?bool $deleted = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $reason = null;

    /**
     * @var array
     */
    #[ORM\Column(type: 'json', nullable: false)]
    protected array $changedFields = [];

    /**
     * @var AlbumPending|null
     */
    #[OneToOne(targetEntity: 'AlbumPending', inversedBy: 'submission')]
    #[JoinColumn(referencedColumnName: 'id')]
    protected ?AlbumPending $albumPending = null;

    /**
     * @var CasePending|null
     */
    #[OneToOne(targetEntity: 'CasePending', inversedBy: 'submission')]
    #[JoinColumn(referencedColumnName: 'id')]
    protected ?CasePending $casePending = null;

    /**
     * @var ChapterPending|null
     */
    #[OneToOne(targetEntity: 'ChapterPending', inversedBy: 'submission')]
    #[JoinColumn(referencedColumnName: 'id')]
    protected ?ChapterPending $chapterPending = null;

    /**
     * @var CharacterPending|null
     */
    #[OneToOne(targetEntity: 'CharacterPending', inversedBy: 'submission')]
    #[JoinColumn(referencedColumnName: 'id')]
    protected ?CharacterPending $characterPending = null;

    /**
     * @var DvdPending|null
     */
    #[OneToOne(targetEntity: 'DvdPending', inversedBy: 'submission')]
    #[JoinColumn(referencedColumnName: 'id')]
    protected ?DvdPending $dvdPending = null;

    /**
     * @var EpisodePending|null
     */
    #[OneToOne(targetEntity: 'EpisodePending', inversedBy: 'submission')]
    #[JoinColumn(referencedColumnName: 'id')]
    protected ?EpisodePending $episodePending = null;

    /**
     * @var GadgetPending|null
     */
    #[OneToOne(targetEntity: 'GadgetPending', inversedBy: 'submission')]
    #[JoinColumn(referencedColumnName: 'id')]
    protected ?GadgetPending $gadgetPending = null;

    /**
     * @var SituationPending|null
     */
    #[OneToOne(targetEntity: 'SituationPending', inversedBy: 'submission')]
    #[JoinColumn(referencedColumnName: 'id')]
    protected ?SituationPending $situationPending = null;

    /**
     * @var TrackPending|null
     */
    #[OneToOne(targetEntity: 'TrackPending', inversedBy: 'submission')]
    #[JoinColumn(referencedColumnName: 'id')]
    protected ?TrackPending $trackPending = null;

    /**
     * @var VolumePending|null
     */
    #[OneToOne(targetEntity: 'VolumePending', inversedBy: 'submission')]
    #[JoinColumn(referencedColumnName: 'id')]
    protected ?VolumePending $volumePending = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserValid(): ?User
    {
        return $this->userValid;
    }

    /**
     * @return $this
     */
    public function setUserValid(?User $userValid): self
    {
        $this->userValid = $userValid;

        return $this;
    }

    public function getComm(): ?string
    {
        return $this->comm;
    }

    /**
     * @return $this
     */
    public function setComm(?string $comm): self
    {
        $this->comm = $comm;

        return $this;
    }

    public function getDateSubmitted(): ?DateTime
    {
        return $this->dateSubmitted;
    }

    public function setDateSubmitted(?DateTime $dateSubmitted): self
    {
        $this->dateSubmitted = $dateSubmitted;

        return $this;
    }

    public function getDateValid(): ?DateTime
    {
        return $this->dateValid;
    }

    /**
     * @return $this
     */
    public function setDateValid(?DateTime $dateValid): self
    {
        $this->dateValid = $dateValid;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        // We only record IP if user is not set (anonymous contribution)
        if (null === $this->getUser()) {
            $this->ip = $ip;
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function isValidated(): ?bool
    {
        return $this->validated;
    }

    /**
     * @param bool $validated
     */
    public function setValidated(?bool $validated): self
    {
        $this->validated = $validated;

        return $this;
    }

    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(?string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }

    public function getChangedFields(): array
    {
        return $this->changedFields;
    }

    public function setChangedFields(array $changedFields): self
    {
        $this->changedFields = $changedFields;

        return $this;
    }

    public function getType(): ?string
    {
        if (null !== $this->albumPending) {
            return 'Album';
        } elseif (null !== $this->casePending) {
            return 'CaseEntity';
        } elseif (null !== $this->chapterPending) {
            return 'Chapter';
        } elseif (null !== $this->characterPending) {
            return 'Character';
        } elseif (null !== $this->dvdPending) {
            return 'Dvd';
        } elseif (null !== $this->episodePending) {
            return 'Episode';
        } elseif (null !== $this->gadgetPending) {
            return 'Gadget';
        } elseif (null !== $this->situationPending) {
            return 'Situation';
        } elseif (null !== $this->trackPending) {
            return 'Track';
        } elseif (null !== $this->volumePending) {
            return 'Volume';
        }

        return null;
    }

    public function getPending(): ?PendingInterface
    {
        if (null !== $this->albumPending) {
            return $this->albumPending;
        } elseif (null !== $this->casePending) {
            return $this->casePending;
        } elseif (null !== $this->chapterPending) {
            return $this->chapterPending;
        } elseif (null !== $this->characterPending) {
            return $this->characterPending;
        } elseif (null !== $this->dvdPending) {
            return $this->dvdPending;
        } elseif (null !== $this->episodePending) {
            return $this->episodePending;
        } elseif (null !== $this->gadgetPending) {
            return $this->gadgetPending;
        } elseif (null !== $this->situationPending) {
            return $this->situationPending;
        } elseif (null !== $this->trackPending) {
            return $this->trackPending;
        } elseif (null !== $this->volumePending) {
            return $this->volumePending;
        }

        return null;
    }

    public function setPending(PendingInterface $pending): self
    {
        if (is_a($pending, AlbumPending::class)) {
            $this->setAlbumPending($pending);
        } elseif (is_a($pending, CasePending::class)) {
            $this->setCasePending($pending);
        } elseif (is_a($pending, ChapterPending::class)) {
            $this->setChapterPending($pending);
        } elseif (is_a($pending, CharacterPending::class)) {
            $this->setCharacterPending($pending);
        } elseif (is_a($pending, DvdPending::class)) {
            $this->setDvdPending($pending);
        } elseif (is_a($pending, EpisodePending::class)) {
            $this->setEpisodePending($pending);
        } elseif (is_a($pending, GadgetPending::class)) {
            $this->setGadgetPending($pending);
        } elseif (is_a($pending, SituationPending::class)) {
            $this->setSituationPending($pending);
        } elseif (is_a($pending, TrackPending::class)) {
            $this->setTrackPending($pending);
        } elseif (is_a($pending, VolumePending::class)) {
            $this->setVolumePending($pending);
        } else {
            throw new UnexpectedTypeException($pending);
        }

        return $this;
    }

    public function getAlbumPending(): ?AlbumPending
    {
        return $this->albumPending;
    }

    public function setAlbumPending(?AlbumPending $albumPending): self
    {
        $this->albumPending = $albumPending;

        return $this;
    }

    public function getCasePending(): ?CasePending
    {
        return $this->casePending;
    }

    public function setCasePending(?CasePending $casePending): self
    {
        $this->casePending = $casePending;

        return $this;
    }

    public function getChapterPending(): ?ChapterPending
    {
        return $this->chapterPending;
    }

    public function setChapterPending(?ChapterPending $chapterPending): self
    {
        $this->chapterPending = $chapterPending;

        return $this;
    }

    public function getCharacterPending(): ?CharacterPending
    {
        return $this->characterPending;
    }

    public function setCharacterPending(?CharacterPending $characterPending): self
    {
        $this->characterPending = $characterPending;

        return $this;
    }

    public function getDvdPending(): ?DvdPending
    {
        return $this->dvdPending;
    }

    public function setDvdPending(?DvdPending $dvdPending): self
    {
        $this->dvdPending = $dvdPending;

        return $this;
    }

    public function getEpisodePending(): ?EpisodePending
    {
        return $this->episodePending;
    }

    public function setEpisodePending(?EpisodePending $episodePending): self
    {
        $this->episodePending = $episodePending;

        return $this;
    }

    public function getGadgetPending(): ?GadgetPending
    {
        return $this->gadgetPending;
    }

    public function setGadgetPending(?GadgetPending $gadgetPending): self
    {
        $this->gadgetPending = $gadgetPending;

        return $this;
    }

    public function getSituationPending(): ?SituationPending
    {
        return $this->situationPending;
    }

    public function setSituationPending(?SituationPending $situationPending): self
    {
        $this->situationPending = $situationPending;

        return $this;
    }

    public function getTrackPending(): ?TrackPending
    {
        return $this->trackPending;
    }

    public function setTrackPending(?TrackPending $trackPending): self
    {
        $this->trackPending = $trackPending;

        return $this;
    }

    public function getVolumePending(): ?VolumePending
    {
        return $this->volumePending;
    }

    public function setVolumePending(?VolumePending $volumePending): self
    {
        $this->volumePending = $volumePending;

        return $this;
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity]
class CasePending extends CaseAbstract implements PendingInterface
{
    /**
     * @var Collection
     */
    #[ORM\JoinTable(name: 'case_character_pending')]
    #[ORM\ManyToMany(targetEntity: 'Character')]
    protected $characters;

    /**
     * @var Collection
     */
    #[ORM\JoinTable(name: 'case_gadget_pending')]
    #[ORM\ManyToMany(targetEntity: 'Gadget')]
    protected $gadgets;

    /**
     * Points to the current visible entity.
     * NULL if new entity.
     *
     * @var CaseEntity|null
     */
    #[ORM\ManyToOne(targetEntity: 'CaseEntity')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?CaseEntity $current = null;

    /**
     * @var Submission|null
     */
    #[ORM\OneToOne(targetEntity: 'Submission', mappedBy: 'casePending')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Submission $submission = null;

    /**
     * CaseEntity constructor.
     */
    public function __construct()
    {
        $this->characters = new ArrayCollection();
        $this->gadgets = new ArrayCollection();
    }

    public function getCharacters(): Collection
    {
        return $this->characters;
    }

    public function addCharacter(Character $character): self
    {
        $this->characters->add($character);

        return $this;
    }

    public function removeCharacter(Character $character): self
    {
        $this->characters->removeElement($character);

        return $this;
    }

    public function getGadgets(): Collection
    {
        return $this->gadgets;
    }

    public function addGadget(Gadget $gadget): self
    {
        $this->gadgets->add($gadget);

        return $this;
    }

    public function removeGadget(Gadget $gadget): self
    {
        $this->gadgets->removeElement($gadget);

        return $this;
    }


    public function getCurrent(): ?CaseEntity
    {
        return $this->current;
    }

    /**
     * @param CaseEntity $current
     */
    public function setCurrent(/* @var CaseEntity */ $current): self
    {
        $this->current = $current;

        return $this;
    }

    public function getSubmission(): ?Submission
    {
        return $this->submission;
    }

    public function setSubmission(?Submission $submission): self
    {
        $this->submission = $submission;

        return $this;
    }
}

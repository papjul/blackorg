<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Abstract class for real entity and pending entities.
 */
class SituationAbstract implements \Stringable
{
    /**
     * @var int|null
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    protected ?int $id = null;

    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 1)]
    #[Assert\Range(min: 1, max: 9)]
    protected ?int $number = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(max: 100)]
    protected ?string $title = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    protected ?string $scene = null;

    #[ORM\Column(type: 'json')]
    protected array $suspects = [];

    #[ORM\Column(type: 'json')]
    protected array $victims = [];

    #[ORM\Column(type: 'json')]
    protected array $culprits = [];

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $summary = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    protected ?string $causeOfDeath = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $resolutionExplanations = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $resolutionEvidences = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $resolutionMotive = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getDisplayNumber();
    }

    /**
     * @return string|null
     */
    public function getDisplayNumber(): ?string
    {
        return $this->case->getDisplayNumber() . '-' . $this->number;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @return SituationAbstract
     */
    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return SituationAbstract
     * @return SituationAbstract
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getScene(): ?string
    {
        return $this->scene;
    }

    public function setScene(?string $scene): self
    {
        $this->scene = $scene;

        return $this;
    }

    public function getSuspects(): array
    {
        return $this->suspects;
    }

    public function setSuspects(array $suspects): self
    {
        $this->suspects = array_unique($suspects);

        return $this;
    }

    public function getVictims(): array
    {
        return $this->victims;
    }

    public function setVictims(array $victims): self
    {
        $this->victims = array_unique($victims);

        return $this;
    }

    public function getCulprits(): array
    {
        return $this->culprits;
    }

    public function setCulprits(array $culprits): self
    {
        $this->culprits = array_unique($culprits);

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getCauseOfDeath(): ?string
    {
        return $this->causeOfDeath;
    }

    public function setCauseOfDeath(?string $causeOfDeath): self
    {
        $this->causeOfDeath = $causeOfDeath;

        return $this;
    }

    public function getResolutionExplanations(): ?string
    {
        return $this->resolutionExplanations;
    }

    public function setResolutionExplanations(?string $resolutionExplanations): self
    {
        $this->resolutionExplanations = $resolutionExplanations;

        return $this;
    }

    public function getResolutionEvidences(): ?string
    {
        return $this->resolutionEvidences;
    }

    public function setResolutionEvidences(?string $resolutionEvidences): self
    {
        $this->resolutionEvidences = $resolutionEvidences;

        return $this;
    }

    public function getResolutionMotive(): ?string
    {
        return $this->resolutionMotive;
    }

    public function setResolutionMotive(?string $resolutionMotive): self
    {
        $this->resolutionMotive = $resolutionMotive;

        return $this;
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity]
class SituationPending extends SituationAbstract implements PendingInterface
{
    /**
     * @var CaseEntity|null
     */
    #[ORM\ManyToOne(targetEntity: 'CaseEntity')]
    #[ORM\JoinColumn(nullable: false)]
    protected ?CaseEntity $case = null;

    /**
     * @var Collection
     */
    #[ORM\JoinTable(name: 'situation_situation_type_pending')]
    #[ORM\ManyToMany(targetEntity: 'SituationType')]
    protected $types;

    /**
     * @var SituationLocation|null
     */
    #[ORM\ManyToOne(targetEntity: 'SituationLocation')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?SituationLocation $location = null;

    /**
     * Points to the current visible entity.
     * NULL if new entity.
     *
     * @var Situation|null
     */
    #[ORM\ManyToOne(targetEntity: 'Situation')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Situation $current = null;

    /**
     * @var Submission|null
     */
    #[ORM\OneToOne(targetEntity: 'Submission', mappedBy: 'situationPending')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Submission $submission = null;

    /**
     * SituationPending constructor.
     */
    public function __construct()
    {
        $this->types = new ArrayCollection();
    }

    public function getCase(): ?CaseEntity
    {
        return $this->case;
    }

    public function setCase(?CaseEntity $case): self
    {
        $this->case = $case;

        return $this;
    }

    public function getLocation(): ?SituationLocation
    {
        return $this->location;
    }

    public function setLocation(?SituationLocation $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function addType(SituationType $situation_type): self
    {
        $this->types->add($situation_type);

        return $this;
    }

    public function removeType(SituationType $situation_type): self
    {
        $this->types->removeElement($situation_type);

        return $this;
    }

    public function getCurrent(): ?Situation
    {
        return $this->current;
    }

    /**
     * @param Situation $current
     */
    public function setCurrent(/* @var Situation */ $current): self
    {
        $this->current = $current;

        return $this;
    }

    public function getSubmission(): ?Submission
    {
        return $this->submission;
    }

    public function setSubmission(?Submission $submission): self
    {
        $this->submission = $submission;

        return $this;
    }
}

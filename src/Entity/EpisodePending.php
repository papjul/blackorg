<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity]
class EpisodePending extends EpisodeAbstract implements PendingInterface
{
    /**
     * @var Collection
     */
    #[ORM\JoinTable(name: 'episode_case_pending')]
    #[ORM\ManyToMany(targetEntity: 'CaseEntity')]
    protected $cases;

    /**
     * @var Track|null
     */
    #[ORM\ManyToOne(targetEntity: 'Track')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Track $opening = null;

    /**
     * @var Track|null
     */
    #[ORM\ManyToOne(targetEntity: 'Track')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Track $ending = null;

    /**
     * @var Dvd|null
     */
    #[ORM\ManyToOne(targetEntity: 'Dvd')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?Dvd $dvd = null;

    /**
     * Points to the current visible entity.
     * NULL if new entity.
     *
     * @var Episode|null
     */
    #[ORM\ManyToOne(targetEntity: 'Episode')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Episode $current = null;

    /**
     * @var Submission|null
     */
    #[ORM\OneToOne(targetEntity: 'Submission', mappedBy: 'episodePending')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Submission $submission = null;

    /**
     * Episode Pending constructor.
     */
    public function __construct()
    {
        $this->cases = new ArrayCollection();
    }

    public function getCases(): Collection
    {
        return $this->cases;
    }

    public function addCase(CaseEntity $case): self
    {
        $this->cases->add($case);

        return $this;
    }

    public function removeCase(CaseEntity $case): self
    {
        $this->cases->removeElement($case);

        return $this;
    }

    public function getOpening(): ?Track
    {
        return $this->opening;
    }

    public function setOpening(?Track $opening): self
    {
        $this->opening = $opening;

        return $this;
    }

    public function getEnding(): ?Track
    {
        return $this->ending;
    }

    public function setEnding(?Track $ending): self
    {
        $this->ending = $ending;

        return $this;
    }

    public function getDvd(): ?Dvd
    {
        return $this->dvd;
    }

    public function setDvd(?Dvd $dvd): self
    {
        $this->dvd = $dvd;

        return $this;
    }

    public function getCurrent(): ?Episode
    {
        return $this->current;
    }

    /**
     * @param Episode $current
     */
    public function setCurrent(/* @var Episode */ $current): self
    {
        $this->current = $current;

        return $this;
    }

    public function getSubmission(): ?Submission
    {
        return $this->submission;
    }

    public function setSubmission(?Submission $submission): self
    {
        $this->submission = $submission;

        return $this;
    }
}

<?php

declare (strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Metadata\ApiFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
#[ApiResource(operations: [new Get(), new GetCollection()])]
#[ApiFilter(filterClass: PropertyFilter::class)]
#[ORM\Table(name: 'volumes', options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity(repositoryClass: \App\Repository\VolumeRepository::class)]
class Volume extends VolumeAbstract implements SubmittableEntityInterface
{
    use SubmittableEntityTrait;
    /**
     * @var \Collection
     */
    #[ORM\OneToMany(targetEntity: 'Chapter', mappedBy: 'volume')]
    #[ORM\OrderBy(['number' => 'ASC'])]
    protected $chapters;
    /**
     * Volume constructor.
     */
    public function __construct()
    {
        $this->chapters = new ArrayCollection();
    }
    public function getChapters() : Collection
    {
        return $this->chapters;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * Interface for entities submittables.
 * For both pending and real entities.
 */
interface SubmittableEntityInterface
{

}

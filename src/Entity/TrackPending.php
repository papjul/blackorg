<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table(options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity]
class TrackPending extends TrackAbstract implements PendingInterface
{
    /**
     * @var Album|null
     */
    #[ORM\ManyToOne(targetEntity: 'Album')]
    #[ORM\JoinColumn(name: 'album_id', referencedColumnName: 'id', nullable: false)]
    #[Assert\NotBlank]
    protected ?Album $album = null;

    /**
     * Points to the current visible entity.
     * NULL if new entity.
     *
     * @var Track|null
     */
    #[ORM\ManyToOne(targetEntity: 'Track')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Track $current = null;

    /**
     * @var Submission|null
     */
    #[ORM\OneToOne(targetEntity: 'Submission', mappedBy: 'trackPending')]
    #[ORM\JoinColumn(referencedColumnName: 'id', nullable: true)]
    protected ?Submission $submission = null;

    public function getAlbum(): ?Album
    {
        return $this->album;
    }

    public function setAlbum(?Album $album): self
    {
        $this->album = $album;

        return $this;
    }

    public function getCurrent(): ?Track
    {
        return $this->current;
    }

    /**
     * @param Track|null $current
     */
    public function setCurrent(/* @var Track */ $current): self
    {
        $this->current = $current;

        return $this;
    }

    public function getSubmission(): ?Submission
    {
        return $this->submission;
    }

    public function setSubmission(?Submission $submission): self
    {
        $this->submission = $submission;

        return $this;
    }
}

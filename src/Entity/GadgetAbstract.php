<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Abstract class for real entity and pending entities.
 */
class GadgetAbstract implements SubmittableEntityInterface, \Stringable
{
    /**
     * @var int|null
     */
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    protected ?int $id = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    protected ?string $complete_name_fr = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    protected ?string $name_fr = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[Assert\Length(max: 100)]
    protected ?string $name_jp = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    protected ?string $shortdesc = null;

    /**
     * @var string|null
     */
    #[ORM\Column(name: '`desc`', type: 'text', nullable: true)]
    protected ?string $desc = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getDisplayName(): ?string
    {
        return $this->__toString();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->complete_name_fr;
    }

    /**
     * @return string|null
     */
    public function getFormName(): ?string
    {
        return '<img src="/img/gadget/' . $this->id . '_60px.jpg" width="20" height="20" alt="" /> ' . $this->name_fr;
    }

    /**
     * @return string|null
     */
    public function getHtmlOptionLabel(): ?string
    {
        return '/img/gadget/' . $this->id . '_60px.jpg|' . $this->name_fr;
    }

    /**
     * @return string|null
     */
    public function getCompleteNameFr(): ?string
    {
        return $this->complete_name_fr;
    }

    /**
     * @return GadgetAbstract
     * @return GadgetAbstract
     */
    public function setCompleteNameFr(?string $complete_name_fr): self
    {
        $this->complete_name_fr = $complete_name_fr;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNameFr(): ?string
    {
        return $this->name_fr;
    }

    /**
     * @return GadgetAbstract
     * @return GadgetAbstract
     */
    public function setNameFr(?string $name_fr): self
    {
        $this->name_fr = $name_fr;

        return $this;
    }

    public function getNameJp(): ?string
    {
        return $this->name_jp;
    }

    public function setNameJp(?string $name_jp): self
    {
        $this->name_jp = $name_jp;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShortdesc(): ?string
    {
        return $this->shortdesc;
    }

    /**
     * @return GadgetAbstract
     * @return GadgetAbstract
     */
    public function setShortdesc(?string $shortdesc): self
    {
        $this->shortdesc = $shortdesc;

        return $this;
    }

    public function getDesc(): ?string
    {
        return $this->desc;
    }

    public function setDesc(?string $desc): self
    {
        $this->desc = $desc;

        return $this;
    }
}

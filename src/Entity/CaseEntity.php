<?php

declare (strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Metadata\ApiFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
#[ApiResource(operations: [new Get(), new GetCollection()], shortName: 'Case')]
#[ApiFilter(filterClass: PropertyFilter::class)]
#[ApiResource(uriTemplate: '/characters/{id}/cases.{_format}', uriVariables: ['id' => new Link(fromClass: \App\Entity\Character::class, identifiers: ['id'], toProperty: 'characters')], status: 200, filters: ['annotated_app_entity_case_entity_api_platform_serializer_filter_property_filter'], operations: [new GetCollection()])]
#[ApiResource(uriTemplate: '/gadgets/{id}/cases.{_format}', uriVariables: ['id' => new Link(fromClass: \App\Entity\Gadget::class, identifiers: ['id'], toProperty: 'gadgets')], status: 200, filters: ['annotated_app_entity_case_entity_api_platform_serializer_filter_property_filter'], operations: [new GetCollection()])]
#[ORM\Table(name: 'cases', options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity(repositoryClass: \App\Repository\CaseRepository::class)]
class CaseEntity extends CaseAbstract implements SubmittableEntityInterface
{
    use SubmittableEntityTrait;
    /**
     * @var \Collection
     */
    #[ORM\OneToMany(targetEntity: 'Chapter', mappedBy: 'case')]
    #[ORM\OrderBy(['number' => 'ASC'])]
    protected $chapters;
    /**
     * @var \Collection
     */
    #[ORM\ManyToMany(targetEntity: 'Episode', mappedBy: 'cases')]
    #[ORM\OrderBy(['number' => 'ASC'])]
    protected $episodes;
    /**
     * @var \Collection
     */
    #[ORM\JoinTable(name: 'cases_characters')]
    #[ORM\ManyToMany(targetEntity: 'Character', inversedBy: 'cases')]
    protected $characters;
    /**
     * @var \Collection
     */
    #[ORM\JoinTable(name: 'cases_gadgets')]
    #[ORM\ManyToMany(targetEntity: 'Gadget', inversedBy: 'cases')]
    protected $gadgets;
    /**
     * @var Collection
     */
    #[ORM\OneToMany(targetEntity: 'Situation', mappedBy: 'case')]
    #[ORM\OrderBy(['number' => 'ASC'])]
    protected $situations;
    /**
     * @var Collection
     */
    #[ORM\OneToMany(targetEntity: 'Character', mappedBy: 'first_appearance')]
    protected $first_appearances;
    /**
     * @var Collection
     */
    #[ORM\OneToMany(targetEntity: 'Gadget', mappedBy: 'first_use')]
    protected $first_uses;
    /**
     * CaseEntity constructor.
     */
    public function __construct()
    {
        $this->chapters = new ArrayCollection();
        $this->situations = new ArrayCollection();
        $this->first_appearances = new ArrayCollection();
        $this->first_uses = new ArrayCollection();
        $this->characters = new ArrayCollection();
        $this->episodes = new ArrayCollection();
        $this->gadgets = new ArrayCollection();
    }
    /**
     * Returns whether this case has at least one element of plot.
     */
    public function hasPlot() : bool
    {
        return count($this->first_appearances) || count($this->first_uses) || !empty($this->plot_chardev) || !empty($this->plot_romance) || !empty($this->plot_org);
    }
    public function getChapters() : Collection
    {
        return $this->chapters;
    }
    public function getEpisodes() : Collection
    {
        return $this->episodes;
    }
    public function getCharacters() : Collection
    {
        return $this->characters;
    }
    public function getGadgets() : Collection
    {
        return $this->gadgets;
    }
    public function getSituations() : Collection
    {
        return $this->situations;
    }
    public function getFirstAppearances() : Collection
    {
        return $this->first_appearances;
    }
    public function getFirstUses() : Collection
    {
        return $this->first_uses;
    }
    public function addEpisode(Episode $episode) : CaseEntity
    {
        $this->episodes[] = $episode;
        return $this;
    }
    public function removeEpisode(Episode $episode) : CaseEntity
    {
        $this->episodes->removeElement($episode);
        return $this;
    }
    public function addCharacter(Character $character) : CaseEntity
    {
        $this->characters->add($character);
        $character->addCase($this);
        return $this;
    }
    public function removeCharacter(Character $character) : CaseEntity
    {
        $this->characters->removeElement($character);
        $character->removeCase($this);
        return $this;
    }
    public function addGadget(Gadget $gadget) : CaseEntity
    {
        $this->gadgets->add($gadget);
        $gadget->addCase($this);
        return $this;
    }
    public function removeGadget(Gadget $gadget) : CaseEntity
    {
        $this->gadgets->removeElement($gadget);
        $gadget->removeCase($this);
        return $this;
    }
}

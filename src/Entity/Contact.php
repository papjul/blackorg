<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contact.
 */
#[ORM\Table]
#[ORM\Entity(repositoryClass: \App\Repository\ContactRepository::class)]
class Contact
{
    /**
     * @var int|null
     */
    #[ORM\Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected ?int $id = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'datetime', nullable: false)]
    protected ?DateTime $creationDate = null;

    /**
     * @var string|null
     */
    #[Assert\NotBlank]
    #[Assert\Length(min: 2, max: 50)]
    #[ORM\Column(type: 'text', length: 50, nullable: false)]
    protected ?string $identity = null;

    #[Assert\Email]
    #[Assert\Length(min: 2, max: 50)]
    #[ORM\Column(type: 'text', length: 50, nullable: true)]
    protected ?string $email = null;

    #[Assert\NotBlank]
    #[Assert\Length(min: 2, max: 50)]
    #[ORM\Column(type: 'text', length: 50, nullable: false)]
    protected ?string $subject = null;

    #[Assert\NotBlank]
    #[Assert\Length(min: 2, max: 2000)]
    #[ORM\Column(type: 'text', nullable: false)]
    protected ?string $message = null;

    /**
     * @var bool
     */
    #[ORM\Column(type: 'boolean')]
    protected bool $read = false;

    public function getIdentity(): ?string
    {
        return $this->identity;
    }

    public function setIdentity(?string $identity): void
    {
        $this->identity = $identity;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): void
    {
        $this->subject = $subject;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

    public function getCreationDate(): ?DateTime
    {
        return $this->creationDate;
    }

    public function setCreationDate(?DateTime $creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isRead(): ?bool
    {
        return $this->read;
    }

    public function setRead(?bool $read): void
    {
        $this->read = $read;
    }
}

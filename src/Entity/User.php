<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table(name: '`user`')]
#[ORM\Entity(repositoryClass: \App\Repository\UserRepository::class)]
#[UniqueEntity(fields: ['username'], message: 'Il existe déjà un utilisateur avec ce nom')]
#[UniqueEntity(fields: ['email'], message: 'Il existe déjà un utilisateur avec cette adresse de courriel')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private readonly int $id;

    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private string $username;

    #[ORM\Column(type: 'json')]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column(type: 'string')]
    private string $password;

    /**
     * @var string|null
     */
    #[Assert\Email]
    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $email = null;

    /**
     * @var string|null
     */
    #[Assert\Email]
    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $pendingEmail = null;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 180, nullable: true)]
    private ?string $confirmationToken = null;

    /**
     * @var bool
     */
    #[ORM\Column(type: 'boolean', nullable: false)]
    private bool $enabled = false;

    /**
     * @var bool
     */
    #[ORM\Column(type: 'boolean', nullable: false)]
    private bool $locked = false;

    /**
     * @var bool
     */
    #[ORM\Column(type: 'boolean', nullable: false)]
    private bool $emailConfirmed = false;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'datetime', nullable: false)]
    private ?DateTime $registrationDate;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?DateTime $lastLogin = null;

    /**
     * @var int
     */
    #[ORM\Column(type: 'integer')]
    private int $accepted_cla;

    /**
     * @var int
     */
    #[ORM\Column(type: 'integer')]
    private int $accepted_privacy;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', length: 5, nullable: true)]
    private ?string $theme = 'light';

    public function __construct()
    {
        $this->registrationDate = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPendingEmail(): ?string
    {
        return $this->pendingEmail;
    }

    public function setPendingEmail(?string $pendingEmail): User
    {
        $this->pendingEmail = $pendingEmail;

        return $this;
    }

    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): User
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function isLocked(): ?bool
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     */
    public function setLocked(?bool $locked): User
    {
        $this->locked = $locked;

        return $this;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): User
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function isEmailConfirmed(): ?bool
    {
        return $this->emailConfirmed;
    }

    public function setEmailConfirmed(?bool $emailConfirmed): User
    {
        $this->emailConfirmed = $emailConfirmed;

        return $this;
    }

    public function getRegistrationDate(): ?DateTime
    {
        return $this->registrationDate;
    }

    public function setRegistrationDate(?DateTime $registrationDate): User
    {
        $this->registrationDate = $registrationDate;

        return $this;
    }

    public function getLastLogin(): ?DateTime
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?DateTime $lastLogin): User
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    public function getAcceptedCla(): ?int
    {
        return $this->accepted_cla;
    }

    public function setAcceptedCla(?int $accepted_cla): User
    {
        $this->accepted_cla = $accepted_cla;

        return $this;
    }

    public function getAcceptedPrivacy(): ?int
    {
        return $this->accepted_privacy;
    }

    public function setAcceptedPrivacy(?int $accepted_privacy): User
    {
        $this->accepted_privacy = $accepted_privacy;

        return $this;
    }

    public function getTheme(): ?string
    {
        return $this->theme;
    }

    public function setTheme(?string $theme): User
    {
        $this->theme = $theme;

        return $this;
    }
}

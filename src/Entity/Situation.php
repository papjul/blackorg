<?php

declare (strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Serializer\Filter\PropertyFilter;
use ApiPlatform\Metadata\ApiFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
#[ApiResource(operations: [new Get(), new GetCollection()])]
#[ApiFilter(filterClass: PropertyFilter::class)]
#[ORM\Table(options: ['collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'])]
#[ORM\Entity(repositoryClass: \App\Repository\SituationRepository::class)]
class Situation extends SituationAbstract implements SubmittableEntityInterface
{
    use SubmittableEntityTrait;
    /**
     * @var CaseEntity|null
     */
    #[ORM\ManyToOne(targetEntity: 'CaseEntity', inversedBy: 'situations')]
    #[ORM\JoinColumn(nullable: false)]
    protected ?CaseEntity $case = null;
    /**
     * @var Collection
     */
    #[ORM\JoinTable(name: 'situation_situation_type')]
    #[ORM\ManyToMany(targetEntity: 'SituationType', inversedBy: 'situations')]
    protected $types;
    /**
     * @var SituationLocation|null
     */
    #[ORM\ManyToOne(targetEntity: 'SituationLocation', inversedBy: 'situations')]
    #[ORM\JoinColumn(nullable: true)]
    protected ?SituationLocation $location = null;
    /**
     * @var Collection
     */
    #[ORM\OneToMany(targetEntity: 'SituationSolvedBy', mappedBy: 'situation')]
    #[ORM\OrderBy(['order' => 'ASC'])]
    protected $solvedBy;
    /**
     * Situation constructor.
     */
    public function __construct()
    {
        $this->types = new ArrayCollection();
        $this->solvedBy = new ArrayCollection();
    }
    public function getCase() : ?CaseEntity
    {
        return $this->case;
    }
    public function setCase(?CaseEntity $case) : self
    {
        $this->case = $case;
        return $this;
    }
    public function getLocation() : ?SituationLocation
    {
        return $this->location;
    }
    public function setLocation(?SituationLocation $location) : self
    {
        $this->location = $location;
        return $this;
    }
    public function getTypes() : Collection
    {
        return $this->types;
    }
    public function addType(SituationType $situation_type) : self
    {
        $this->types->add($situation_type);
        $situation_type->addSituation($this);
        return $this;
    }
    public function removeType(SituationType $situation_type) : self
    {
        $this->types->removeElement($situation_type);
        $situation_type->removeSituation($this);
        return $this;
    }
    public function getSolvedBy() : Collection
    {
        return $this->solvedBy;
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Command;

use App\Repository\SubmissionRepository;
use App\Repository\UserRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

/**
 * A console command that send all pending submissions to all users with ROLE_VALIDATOR.
 * Useful as a cron task.
 */
#[AsCommand(name: 'app:mail-pending-submissions')]
class MailPendingSubmissionsCommand extends Command
{
    /**
     * MailPendingSubmissionsCommand constructor.
     * @param MailerInterface $mailer
     * @param UserRepository $users
     * @param SubmissionRepository $submissions
     */
    public function __construct(private readonly MailerInterface $mailer, private readonly UserRepository $users, private readonly SubmissionRepository $submissions)
    {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Send all pending submissions to all users with ROLE_VALIDATOR')
            ->setHelp(
                <<<'HELP'
The <info>%command.name%</info> command send all pending submissions to all users with ROLE_VALIDATOR:
  <info>php %command.full_name%</info>
HELP
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $submissions = $this->submissions->getSubmissions();
        if (count($submissions)) {
            $validatorUsers = $this->users->findValidatorUsers();
            $output->writeln(count($submissions) . ' submissions found, ' . (is_countable($validatorUsers) ? count($validatorUsers) : 0) . ' validator users will receive them by email.');

            foreach ($validatorUsers as $validatorUser) {
                if (null !== $validatorUser->getEmail()) {
                    $email = (new TemplatedEmail())
                        ->from(new Address('noreply@black-org.fr', 'Black Org'))
                        ->to(new Address($validatorUser->getEmail(), $validatorUser->getUsername()))
                        ->subject('[Black Org] Suggestions en attente')
                        ->textTemplate('email/submissions.txt.twig')
                        ->htmlTemplate('email/submissions.html.twig')
                        ->context([
                            'user' => $validatorUser,
                            'submissions' => $submissions,
                        ]);
                    $this->mailer->send($email);
                    $output->writeln('Email sent to ' . $validatorUser->getUsername());
                }
            }
        } else {
            $output->writeln('Found no pending submissions. No mail will be send.');
        }

        return 0;
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Command;

use App\Repository\ContactRepository;
use App\Repository\UserRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

/**
 * A console command that send all contact messages to all users with ROLE_SUPER_ADMIN.
 * Useful as a cron task.
 */
#[AsCommand(name: 'app:mail-contact-messages')]
class MailContactMessagesCommand extends Command
{
    /**
     * MailContactMessagesCommand constructor.
     * @param MailerInterface $mailer
     * @param UserRepository $users
     * @param ContactRepository $contact
     */
    public function __construct(private readonly MailerInterface $mailer, private readonly UserRepository $users, private readonly ContactRepository $contact)
    {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Send all contact messages to all users with ROLE_SUPER_ADMIN.')
            ->setHelp(
                <<<'HELP'
The <info>%command.name%</info> command send all contact messages to all users with ROLE_SUPER_ADMIN:
  <info>php %command.full_name%</info>
HELP
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $messages = $this->contact->findBy(['read' => false]);
        if (count($messages)) {
            $superAdminUsers = $this->users->findSuperAdminUsers();
            $output->writeln(count($messages) . ' contact messages found, ' . (is_countable($superAdminUsers) ? count($superAdminUsers) : 0) . ' super admins will receive them by email.');

            foreach ($superAdminUsers as $superAdminUser) {
                if (null !== $superAdminUser->getEmail()) {
                    $email = (new TemplatedEmail())
                        ->from(new Address('noreply@black-org.fr', 'Black Org'))
                        ->to(new Address($superAdminUser->getEmail(), $superAdminUser->getUsername()))
                        ->subject('[Black Org] Messages de contact')
                        ->textTemplate('email/contact.txt.twig')
                        ->htmlTemplate('email/contact.html.twig')
                        ->context([
                            'user' => $superAdminUser,
                            'messages' => $messages,
                        ]);
                    $this->mailer->send($email);
                    $output->writeln('Email sent to ' . $superAdminUser->getUsername());
                }
            }
        } else {
            $output->writeln('Found no contact messages. No mail will be send.');
        }

        return 0;
    }
}

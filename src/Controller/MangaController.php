<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\Chapter;
use App\Entity\Volume;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class MangaController.
 */
class MangaController extends AbstractController
{
    /**
     * Chapters page controller.
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    #[Route("/chapters", name: "chapters", methods: ["GET"])]
    public function chaptersAction(EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $volumes = $entityManager->getRepository(Volume::class)->getVolumes('original');

        if (!(is_countable($volumes) ? count($volumes) : 0)) {
            throw new HttpException(404, 'Aucun tome n’a été trouvé. ' . $translator->trans('TempAbortMsg'));
        }

        return $this->render('manga/chapters.html.twig', [
            'breadcrumb' => [$this->generateUrl('volumes') => 'Tomes'],
            'volumes' => $volumes,
            'firstVolume' => $volumes[0],
            'latestJpVolume' => $volumes[(is_countable($volumes) ? count($volumes) : 0) - 1],
            'latestFrVolume' => $entityManager->getRepository(Volume::class)->getLatestFrVolume(),
            'firstChapter' => $entityManager->getRepository(Chapter::class)->getFirstChapter(),
            'maxChapter' => $entityManager->getRepository(Chapter::class)->getMaxChapter(),
            'prepublishedChapters' => $entityManager->getRepository(Chapter::class)->getPrepublishedChapters(),
        ]);
    }

    /**
     * Tokubestu page controller.
     */
    #[Route("/chapters/tokubetsu", name: "chapters_tokubetsu", methods: ["GET"])]
    public function tokubetsuAction(EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $volumes = $entityManager->getRepository(Volume::class)->getVolumes('tokubetsu');

        if (!(is_countable($volumes) ? count($volumes) : 0)) {
            throw new HttpException(404, 'Aucun tome n’a été trouvé. ' . $translator->trans('TempAbortMsg'));
        }

        return $this->render('manga/chapters-tokubetsu.html.twig', [
            'volumes' => $volumes,
            'latestVolume' => $volumes[(is_countable($volumes) ? count($volumes) : 0) - 1],
        ]);
    }

    /**
     * Movie page controller.
     */
    #[Route("/chapters/movie", name: "chapters_movie", methods: ["GET"])]
    public function movieAction(EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $volumes = $entityManager->getRepository(Volume::class)->getVolumes('movie');

        if (!(is_countable($volumes) ? count($volumes) : 0)) {
            throw new HttpException(404, 'Aucun tome n’a été trouvé. ' . $translator->trans('TempAbortMsg'));
        }

        return $this->render('manga/chapters-movie.html.twig', [
            'volumes' => $volumes,
            'latestVolume' => $volumes[(is_countable($volumes) ? count($volumes) : 0) - 1],
        ]);
    }

    /**
     * Wps page controller.
     */
    #[Route("/chapters/wps", name: "chapters_wps", methods: ["GET"])]
    public function wpsAction(EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $volumes = $entityManager->getRepository(Volume::class)->getVolumes('wps');

        if (!(is_countable($volumes) ? count($volumes) : 0)) {
            throw new HttpException(404, 'Aucun tome n’a été trouvé. ' . $translator->trans('TempAbortMsg'));
        }

        return $this->render('manga/chapters-wps.html.twig', [
            'volumes' => $volumes,
            'latestVolume' => $volumes[(is_countable($volumes) ? count($volumes) : 0) - 1],
        ]);
    }

    /**
     * Volumes page controller.
     */
    #[Route("/volumes", name: "volumes", methods: ["GET"])]
    #[Route("/volumes/{type<(tokubetsu|movie|wps)>}", name: "volumes_type", methods: ["GET"])]
    public function volumesAction(EntityManagerInterface $entityManager, TranslatorInterface $translator, string $type = 'original'): Response
    {
        $volumes = $entityManager->getRepository(Volume::class)->findBy(['type' => $type]);

        if (!count($volumes)) {
            throw new HttpException(404, 'Aucun tome n’a été trouvé. ' . $translator->trans('TempAbortMsg'));
        }

        return $this->render('manga/volumes.html.twig', [
            'volumes' => $volumes,
            'type' => $type,
        ]);
    }

    /**
     * Original volume view page controller.
     */
    #[Route("/volumes/{number<[1-9]\d*>}", name: "volume_original_view", methods: ["GET"])]
    public function originalVolumeViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findOriginal(number)")] Volume $originalVolume): Response
    {
        return $this->volumeViewAction($entityManager, $originalVolume);
    }

    private function volumeViewAction(EntityManagerInterface $entityManager, Volume $volume): Response
    {
        $volumeBefore = $entityManager->getRepository(Volume::class)->findOneBy(['number' => $volume->getNumber() - 1, 'type' => $volume->getType()]);
        $volumeAfter = $entityManager->getRepository(Volume::class)->findOneBy(['number' => $volume->getNumber() + 1, 'type' => $volume->getType()]);

        if ('tokubetsu' == $volume->getType()) {
            $breadcrumb = [$this->generateUrl('chapters_tokubetsu') => 'Tomes spéciaux'];
        } else if ('movie' == $volume->getType()) {
            $breadcrumb = [$this->generateUrl('chapters_movie') => 'Tomes du film'];
        } else if ('wps' == $volume->getType()) {
            $breadcrumb = [$this->generateUrl('chapters_wps') => 'Tomes Wild Police Story'];
        } else {
            $breadcrumb = [$this->generateUrl('volumes') => 'Tomes'];
        }

        return $this->render('manga/volume-view.html.twig', [
            'breadcrumb' => $breadcrumb,
            'volume' => $volume,
            'volumeBefore' => $volumeBefore,
            'volumeAfter' => $volumeAfter,
        ]);
    }

    /**
     * Tokubetsu volume view page controller.
     */
    #[Route("/volumes/tokubetsu/{number<[1-9]\d*>}", name: "volume_tokubetsu_view", methods: ["GET"])]
    public function tokubetsuVolumeViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findTokubetsu(number)")] Volume $tokubetsuVolume): Response
    {
        return $this->volumeViewAction($entityManager, $tokubetsuVolume);
    }

    /**
     * Movie volume view page controller.
     */
    #[Route("/volumes/movie/{number<[1-9]\d*>}", name: "volume_movie_view", methods: ["GET"])]
    public function movieVolumeViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findMovie(number)")] Volume $movieVolume): Response
    {
        return $this->volumeViewAction($entityManager, $movieVolume);
    }

    /**
     * Movie volume view page controller.
     */
    #[Route("/volumes/wps/{number<[1-9]\d*>}", name: "volume_wps_view", methods: ["GET"])]
    public function wpsVolumeViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findWps(number)")] Volume $wpsVolume): Response
    {
        return $this->volumeViewAction($entityManager, $wpsVolume);
    }
}

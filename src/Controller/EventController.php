<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\Event;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EventController.
 */
class EventController extends AbstractController
{
    /**
     * News controller.
     *
     * @param int|null $year
     * @param int|null $month
     * @throws Exception
     */
    #[Route("/news", name: "news", methods: ["GET"])]
    #[Route("/news/{year<199[4-9]|20[0-9][0-9]>}/{month<[1-9]|1[0-2]>}", name: "news_parameters", methods: ["GET"])]
    public function indexAction(EntityManagerInterface $entityManager, int $year = null, int $month = null): Response
    {
        // $aNumToType = [0 => 'Anime', 1 => 'Manga', 2 => 'Musique', 3 => 'Divers'];

        $minYear = $entityManager->getRepository(Event::class)::MIN_YEAR;
        $maxYear = intval(date('Y')) + 1;

        if (null === $month || null === $year) {
            $month = intval(date('m'));
            $year = intval(date('Y'));
        } elseif ($year < $minYear || $year > $maxYear) {
            throw new NotFoundHttpException('L’année que tu as demandée est invalide');
        }

        $monthDateTime = new DateTime($year . '-' . $month . '-1');
        $monthBeforeDateTime = (new DateTime($year . '-' . $month . '-1'))->modify('first day of previous month');
        $monthAfterDateTime = (new DateTime($year . '-' . $month . '-1'))->modify('first day of next month');

        $events = $entityManager->getRepository(Event::class)->getEventsDate($year, sprintf('%02s', $month));

        $today = 0;
        if ($monthDateTime->format('m') == date('m') && $monthDateTime->format('Y') == date('Y')) {
            $today = date('d');
        }

        return $this->render('news.html.twig', [
            'minYear' => $minYear,
            'maxYear' => $maxYear,
            'monthBeforeDateTime' => $monthBeforeDateTime,
            'monthDateTime' => $monthDateTime,
            'monthAfterDateTime' => $monthAfterDateTime,
            'month' => $month,
            'year' => $year,
            'today' => $today,
            'events' => $events,
        ]);
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\CaseEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class CaseController.
 */
class CaseController extends AbstractController
{
    /**
     * Cases page controller.
     */
    #[Route("/cases", name: "cases", methods: ["GET"])]
    #[Route("/cases/light", name: "cases_light", methods: ["GET"])]
    public function indexAction(Request $request, EntityManagerInterface $entityManager): Response
    {
        return $this->render('cases/index.html.twig', [
            'arcs' => $entityManager->getRepository(CaseEntity::class)->getArcs(),
            'light' => ('cases_light' == $request->attributes->get('_route')),
        ]);
    }

    /**
     * Cases arc page controller.
     */
    #[Route("/cases/arc/{arc<[1-7]>}", name: "cases_arc", methods: ["GET"])]
    #[Route("/cases/arc/{arc<[1-7]>}/light", name: "cases_arc_light", methods: ["GET"])]
    public function caseDisplayArcAction(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator, int $arc): Response
    {
        $cases = $entityManager->getRepository(CaseEntity::class)->getArcCases($arc);
        if (!(is_countable($cases) ? count($cases) : 0)) {
            throw new HttpException(404, 'Aucune affaire n’a été trouvée. ' . $translator->trans('TempAbortMsg'));
        }

        return $this->render('cases/arc.html.twig', [
            'breadcrumb' => [$this->generateUrl('cases') => 'Affaires'],
            'cases' => $cases,
            'arcs' => $entityManager->getRepository(CaseEntity::class)->getArcs(),
            'arc' => $arc,
            'light' => ('cases_arc_light' == $request->attributes->get('_route')),
        ]);
    }

    /**
     * Cases all arcs page controller.
     */
    #[Route("/cases/arc/all", name: "cases_all_arcs", methods: ["GET"])]
    #[Route("/cases/arc/all/light", name: "cases_all_arcs_light", methods: ["GET"])]
    public function caseDisplayAllAction(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $cases = $entityManager->getRepository(CaseEntity::class)->getCases();
        if (!(is_countable($cases) ? count($cases) : 0)) {
            throw new HttpException(404, 'Aucune affaire n’a été trouvée. ' . $translator->trans('TempAbortMsg'));
        }

        return $this->render('cases/arc-all.html.twig', [
            'breadcrumb' => [$this->generateUrl('cases') => 'L’intrigue'],
            'cases' => $cases,
            'arcs' => $entityManager->getRepository(CaseEntity::class)->getArcs(),
            'light' => ('cases_all_arcs_light' == $request->attributes->get('_route')),
        ]);
    }

    /**
     * Manga case view page controller.
     */
    #[Route("/cases/{number<[1-9]\d*>}", name: "manga_case_view", methods: ["GET"])]
    public function mangaCaseViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findManga(number)")] CaseEntity $mangaCase): Response
    {
        return $this->caseViewAction($entityManager, $mangaCase);
    }

    /**
     * WPS case view page controller.
     */
    #[Route("/wps/{number<[1-9]\d*>}", name: "wps_case_view", methods: ["GET"])]
    public function wpsCaseViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findWps(number)")] CaseEntity $wpsCase): Response
    {
        return $this->caseViewAction($entityManager, $wpsCase);
    }

    private function caseViewAction(EntityManagerInterface $entityManager, CaseEntity $case): Response
    {
        $caseBefore = $entityManager->getRepository(CaseEntity::class)->findOneBy(['number' => $case->getNumber() - 1, 'type' => $case->getType()]);
        $caseAfter = $entityManager->getRepository(CaseEntity::class)->findOneBy(['number' => $case->getNumber() + 1, 'type' => $case->getType()]);

        $arcs = $entityManager->getRepository(CaseEntity::class)->getArcs();
        $breadcrumb = [
            $this->generateUrl('cases') => 'Affaires',
            $this->generateUrl('cases_arc', ['arc' => $case->getArc()]) => 'Arc ' . $arcs[$case->getArc()],
        ];

        return $this->render('cases/view.html.twig', [
            'breadcrumb' => $breadcrumb,
            'case' => $case,
            'arcs' => $arcs,
            'caseBefore' => $caseBefore,
            'caseAfter' => $caseAfter,
        ]);
    }

    /**
     * Anime case view page controller.
     */
    #[Route("/cases/{number<[1-9]\d*>}hs", name: "anime_case_view", methods: ["GET"])]
    public function animeCaseViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findAnime(number)")] CaseEntity $animeCase): Response
    {
        return $this->caseViewAction($entityManager, $animeCase);
    }

    /**
     * Plot index page controller.
     */
    #[Route("/plot", name: "plot", methods: ["GET"])]
    public function plotIndexAction(EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $cases = $entityManager->getRepository(CaseEntity::class)->findAllWithPlot();
        if (!(is_countable($cases) ? count($cases) : 0)) {
            throw new HttpException(404, 'Aucune affaire n’a été trouvée. ' . $translator->trans('TempAbortMsg'));
        }

        return $this->render('cases/plot.html.twig', [
            'arcs' => $entityManager->getRepository(CaseEntity::class)->getArcs(),
            'cases' => $cases,
        ]);
    }
}

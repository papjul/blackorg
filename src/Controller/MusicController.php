<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\Album;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MusicController.
 */
class MusicController extends AbstractController
{
    /**
     * Index page controller.
     *
     * @throws Exception
     */
    #[Route("/music", name: "music", methods: ["GET"])]
    public function indexAction(EntityManagerInterface $entityManager): Response
    {
        return $this->render('music/index.html.twig', [
            'breadcrumb' => [$this->generateUrl('anime') => 'Anime'],
            'openingAlbum' => $entityManager->getRepository(Album::class)->getOpeningAlbum(),
            'endingAlbum' => $entityManager->getRepository(Album::class)->getEndingAlbum(),
            'movieThemeAlbum' => $entityManager->getRepository(Album::class)->getMovieAlbum(),
            'tvOST' => $entityManager->getRepository(Album::class)->getAlbums('tv_ost'),
            'movieOST' => $entityManager->getRepository(Album::class)->getAlbums('movie_ost'),
            'other' => $entityManager->getRepository(Album::class)->getAlbums('other'),
        ]);
    }

    /**
     * Album view page controller.
     */
    #[Route("/music/{id<[1-9]\d*>}", name: "album", methods: ["GET"])]
    public function albumAction(EntityManagerInterface $entityManager, Album $album): Response
    {
        if (!in_array($album->getType(), ['movie_ost', 'tv_ost', 'other'])) {
            throw new HttpException(404);
        }

        $albumTypes = $entityManager->getRepository(Album::class)->getAlbumTypes();
        $breadcrumb = [
            $this->generateUrl('anime') => 'Anime',
            $this->generateUrl('music') => 'Musiques',
            $this->generateUrl('music') . '#' . $album->getType() => $albumTypes[$album->getType()],
        ];

        return $this->render('music/album-view.html.twig', [
            'breadcrumb' => $breadcrumb,
            'album' => $album,
        ]);
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\CaseEntity;
use App\Entity\Dvd;
use App\Entity\Episode;
use App\Entity\Track;
use App\Twig\AppExtension;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AnimeController.
 */
class AnimeController extends AbstractController
{
    /**
     * Episodes page controller.
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    #[Route("/episodes", name: "episodes", methods: ["GET"])]
    public function indexAction(EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $episodes = $entityManager->getRepository(Episode::class)->getLatestEpisodes(15);

        if (!(is_countable($episodes) ? count($episodes) : 0)) {
            throw new HttpException(404, 'Aucun épisode n’a été trouvé. ' . $translator->trans('TempAbortMsg'));
        }

        $latestEpisode = $entityManager->getRepository(Episode::class)->getLatestEpisode();

        return $this->render('anime/episodes.html.twig', [
            'breadcrumb' => [$this->generateUrl('anime') => 'Anime'],
            'episodes' => $episodes,
            'nbEpisodes' => $latestEpisode->getNumber(),
        ]);
    }

    /**
     * Episodes page controller.
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws Exception
     */
    #[Route("/episodes/page/{page<[1-9]\d*>}", name: "episodes_paginated", methods: ["GET"])]
    public function episodesPageAction(EntityManagerInterface $entityManager, int $page): Response
    {
        $episodes = $entityManager->getRepository(Episode::class)->getEpisodes($page);

        if (!(is_countable($episodes['results']) ? count($episodes['results']) : 0)) {
            throw new HttpException(404, 'Cette page n’existe pas. Revenir à la <a href="' . $this->generateUrl('episodes') . '">liste des épisodes</a>.');
        }
        $latestEpisode = $entityManager->getRepository(Episode::class)->getLatestEpisode();

        return $this->render('anime/episodes-page.html.twig', [
            'breadcrumb' => [
                $this->generateUrl('anime') => 'Anime',
                $this->generateUrl('episodes') => 'Épisodes',
            ],
            'episodes' => $episodes,
            'nbEpisodes' => $latestEpisode->getNumber(),
        ]);
    }

    /**
     * Season view page controller.
     *
     * @deprecated
     */
    #[Route("/episodes/season/{season<[1-9]\d*>}", name: "episodes_season", methods: ["GET"])]
    public function seasonViewAction(EntityManagerInterface $entityManager, int $season): Response
    {
        $ep = $entityManager->getRepository(Episode::class)->findOneBy(['season' => $season], ['number' => 'ASC']);
        if (null !== $ep) {
            return $this->redirectToRoute('episodes_paginated', ['page' => ceil($ep->getNumber() / Episode::NUM_ITEMS)], 301);
        } else {
            throw new HttpException(404, 'Cette page n’existe pas. Revenir à la <a href="' . $this->generateUrl('episodes') . '">liste des épisodes</a>.');
        }
    }

    /**
     * Episode view page controller.
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    #[Route("/episodes/{number<[1-9]\d*>}", name: "episode_view", methods: ["GET"])]
    public function episodeViewAction(EntityManagerInterface $entityManager, AppExtension $appExtension, Episode $episode): Response
    {
        $epBefore = $entityManager->getRepository(Episode::class)->findOneBy(['number' => $episode->getNumber() - 1]);
        $epAfter = $entityManager->getRepository(Episode::class)->findOneBy(['number' => $episode->getNumber() + 1]);

        $latestEpisode = $entityManager->getRepository(Episode::class)->getLatestEpisode();
        $page = ceil($episode->getNumber() / Episode::NUM_ITEMS);
        $firstEpisodeNumber = $appExtension->addLeadingZero(((($page - 1) * Episode::NUM_ITEMS) + 1), 3);
        $lastEpisodeNumber = $appExtension->addLeadingZero((($page * Episode::NUM_ITEMS > $latestEpisode->getNumber()) ? $latestEpisode->getNumber() : $page * Episode::NUM_ITEMS), 3);

        $breadcrumb = [
            $this->generateUrl('anime') => 'Anime',
            $this->generateUrl('episodes') => 'Épisodes',
            $this->generateUrl('episodes_paginated', ['page' => $page]) => 'Épisodes ' . $firstEpisodeNumber . ' - ' . $lastEpisodeNumber,
        ];

        return $this->render('anime/episode-view.html.twig', [
            'shortTitle' => 'Épisode ' . $episode->getDisplayNumber(),
            'breadcrumb' => $breadcrumb,
            'episode' => $episode,
            'epBefore' => $epBefore,
            'epAfter' => $epAfter,
        ]);
    }

    /**
     * Movie page controller.
     */
    #[Route("/movies", name: "movies", methods: ["GET"])]
    public function moviesAction(EntityManagerInterface $entityManager): Response
    {
        $movies = $entityManager->getRepository(CaseEntity::class)->findBy(['type' => 'movie'], ['date' => 'ASC']);
        $special_movies = $entityManager->getRepository(CaseEntity::class)->findBy(['type' => 'movie_special'], ['date' => 'ASC']);

        return $this->render('anime/movies.html.twig', [
            'breadcrumb' => [$this->generateUrl('anime') => 'Anime'],
            'movies' => $movies,
            'special_movies' => $special_movies,
        ]);
    }

    /**
     * Featured movie view page controller.
     */
    #[Route("/movies/{number<[1-9]\d*>}", name: "movie_view", methods: ["GET"])]
    public function featuredMovieViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findMovie(number)")] CaseEntity $movie): Response
    {
        return $this->movieViewAction($entityManager, $movie);
    }

    /**
     * Movie view page controller.
     */
    private function movieViewAction(EntityManagerInterface $entityManager, CaseEntity $movie): Response
    {
        $movieBefore = $entityManager->getRepository(CaseEntity::class)->findOneBy(['number' => $movie->getNumber() - 1, 'type' => $movie->getType()]);
        $movieAfter = $entityManager->getRepository(CaseEntity::class)->findOneBy(['number' => $movie->getNumber() + 1, 'type' => $movie->getType()]);
        if ('movie' == $movie->getType()) {
            $ending = $entityManager->getRepository(Track::class)->findMovieEnding($movie->getNumber());
        } else {
            $ending = null;
        }

        $breadcrumb = [$this->generateUrl('anime') => 'Anime'];
        if ('movie' === $movie->getType()) {
            $breadcrumb[$this->generateUrl('movies') . '#featuredmovies'] = 'Films';
        } elseif ('movie_special' === $movie->getType()) {
            $breadcrumb[$this->generateUrl('movies') . '#specialmovies'] = 'Films spéciaux';
        }

        return $this->render('anime/movie-view.html.twig', [
            'breadcrumb' => $breadcrumb,
            'movie' => $movie,
            'movieBefore' => $movieBefore,
            'movieAfter' => $movieAfter,
            'ending' => $ending,
        ]);
    }

    /**
     * Special movie view page controller.
     */
    #[Route("/movies/{number<[1-9]\d*>}sp", name: "movie_special_view", methods: ["GET"])]
    public function specialMovieViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findSpecialMovie(number)")] CaseEntity $special_movie): Response
    {
        return $this->movieViewAction($entityManager, $special_movie);
    }

    /**
     * Special page controller.
     */
    #[Route("/specials", name: "specials", methods: ["GET"])]
    public function specialsAction(EntityManagerInterface $entityManager): Response
    {
        $ovas = $entityManager->getRepository(CaseEntity::class)->findBy(['type' => 'ova'], ['date' => 'ASC']);
        $magic_files = $entityManager->getRepository(CaseEntity::class)->findBy(['type' => 'magic_file'], ['date' => 'ASC']);
        $dc_specials = $entityManager->getRepository(CaseEntity::class)->findBy(['type' => 'dc_special'], ['date' => 'ASC']);
        $mk_specials = $entityManager->getRepository(CaseEntity::class)->findBy(['type' => 'mk_special'], ['date' => 'ASC']);
        $mk1412_episodes = $entityManager->getRepository(CaseEntity::class)->findBy(['type' => 'mk1412'], ['date' => 'ASC']);
        $tanpenshus = $entityManager->getRepository(CaseEntity::class)->findBy(['type' => 'tanpenshu'], ['date' => 'ASC']);

        return $this->render('anime/specials.html.twig', [
            'breadcrumb' => [$this->generateUrl('anime') => 'Anime'],
            'ovas' => $ovas,
            'magic_files' => $magic_files,
            'dc_specials' => $dc_specials,
            'mk_specials' => $mk_specials,
            'mk1412_episodes' => $mk1412_episodes,
            'tanpenshus' => $tanpenshus,
        ]);
    }

    /**
     * OVA view page controller.
     */
    #[Route("/specials/ova{number<[1-9]\d*>}", name: "ova_view", methods: ["GET"])]
    public function ovaViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findOva(number)")] CaseEntity $ova): Response
    {
        return $this->specialViewAction($entityManager, $ova);
    }

    /**
     * Special view page controller.
     */
    private function specialViewAction(EntityManagerInterface $entityManager, CaseEntity $special): Response
    {
        $specialBefore = $entityManager->getRepository(CaseEntity::class)->findOneBy(['number' => $special->getNumber() - 1, 'type' => $special->getType()]);
        $specialAfter = $entityManager->getRepository(CaseEntity::class)->findOneBy(['number' => $special->getNumber() + 1, 'type' => $special->getType()]);

        $breadcrumb = [$this->generateUrl('anime') => 'Anime'];
        if ('ova' === $special->getType()) {
            $breadcrumb[$this->generateUrl('specials') . '#ova'] = 'OVA';
        } elseif ('magic_file' === $special->getType()) {
            $breadcrumb[$this->generateUrl('specials') . '#magicfiles'] = 'Magic Files';
        }

        return $this->render('anime/special-view.html.twig', [
            'breadcrumb' => $breadcrumb,
            'special' => $special,
            'specialBefore' => $specialBefore,
            'specialAfter' => $specialAfter,
        ]);
    }

    /**
     * Magic File view page controller.
     */
    #[Route("/specials/mf{number<[1-9]\d*>}", name: "magic_file_view", methods: ["GET"])]
    public function magicFileViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findMagicFile(number)")] CaseEntity $magicFile): Response
    {
        return $this->specialViewAction($entityManager, $magicFile);
    }

    /**
     * Detective Conan Special view page controller.
     */
    #[Route("/specials/sp{number<[1-9]\d*>}", name: "dc_special_view", methods: ["GET"])]
    public function dcSpecialViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findDcSpecial(number)")] CaseEntity $dcSpecial): Response
    {
        return $this->specialViewAction($entityManager, $dcSpecial);
    }

    /**
     * Magic Kaito Special view page controller.
     */
    #[Route("/specials/mks{number<[1-9]\d*>}", name: "mk_special_view", methods: ["GET"])]
    public function mkSpecialViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findMkSpecial(number)")] CaseEntity $mkSpecial): Response
    {
        return $this->specialViewAction($entityManager, $mkSpecial);
    }

    /**
     * Magic Kaito 1412 view page controller.
     */
    #[Route("/specials/mk{number<[1-9]\d*>}", name: "mk1412_view", methods: ["GET"])]
    public function mk1412ViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findMk1412(number)")] CaseEntity $mk1412): Response
    {
        return $this->specialViewAction($entityManager, $mk1412);
    }

    /**
     * Tanpenshū view page controller.
     */
    #[Route("/specials/tanpenshu{number<[1-9]\d*>}", name: "tanpenshu_view", methods: ["GET"])]
    public function tanpenshuSpecialViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findTanpenshu(number)")] CaseEntity $tanpenshu): Response
    {
        return $this->specialViewAction($entityManager, $tanpenshu);
    }

    /**
     * DVD page controller.
     */
    #[Route("/dvds", name: "dvds", methods: ["GET"])]
    public function dvdsAction(EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $dvds = $entityManager->getRepository(Dvd::class)->getDvds();
        if (!(is_countable($dvds) ? count($dvds) : 0)) {
            throw new HttpException(404, 'Aucun DVD n’a été trouvé. ' . $translator->trans('TempAbortMsg'));
        }

        return $this->render('anime/dvds.html.twig', [
            'breadcrumb' => [$this->generateUrl('anime') => 'Anime'],
            'dvds' => $dvds,
        ]);
    }

    /**
     * DVD part view page controller.
     *
     * @return Response
     */
    #[Route("/dvds/{part<[1-9]\d*>}", name: "dvd_view", methods: ["GET"])]
    public function dvdViewAction(EntityManagerInterface $entityManager, int $part)
    {
        $dvds = $entityManager->getRepository(Dvd::class)->getDvd($part);
        if (!(is_countable($dvds) ? count($dvds) : 0)) {
            throw new HttpException(404);
        }

        $breadcrumb = [
            $this->generateUrl('anime') => 'Anime',
            $this->generateUrl('dvds') => 'DVD',
        ];

        return $this->render('anime/dvd-view.html.twig', [
            'breadcrumb' => $breadcrumb,
            'shortTitle' => 'Part ' . $part,
            'part' => $part,
            'dvds' => $dvds,
        ]);
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxController.
 */
#[Route("/ajax")]
class AjaxController extends AbstractController
{
    /**
     * Setting theme controller.
     */
    #[Route("/theme/{theme<(dark|light)>}", name: "ajax_set_theme", methods: ["POST"])]
    public function setThemeAction(Request $request, EntityManagerInterface $entityManager, string $theme): Response
    {
        if ($this->getUser()) {
            $this->getUser()->setTheme($theme);
            $entityManager->flush();
        } else {
            $request->getSession()->set('theme', $theme);
        }

        return new Response('{}');
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Submission;
use App\Entity\User;
use App\Form\EmailChangeFormType;
use App\Form\PasswordChangeFormType;
use App\Form\RegistrationFormType;
use App\Util\TokenGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class UserController.
 */
#[Route("/user")]
class UserController extends AbstractController
{
    #[Route("/login", name: "user_login", defaults: ["_maintenance_access" => true], methods: ["GET", "POST"])]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('user/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @throws Exception
     */
    #[Route("/logout", name: "user_logout", methods: ["GET"])]
    public function logout(): never
    {
        // controller can be blank: it will never be executed!
        throw new Exception('Logout not activated in user.yaml');
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route("/register", name: "user_register", methods: ["GET", "POST"])]
    public function register(Request $request, EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher, TokenGenerator $tokenGenerator, MailerInterface $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user, [
            'latest_privacy' => $this->getParameter('latest_privacy'),
            'latest_cla' => $this->getParameter('latest_cla'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            if ($form['acceptedPrivacy']->getData()) {
                $user->setAcceptedPrivacy($this->getParameter('latest_privacy'));
            }
            if ($form['acceptedCla']->getData()) {
                $user->setAcceptedCla($this->getParameter('latest_cla'));
            }

            $user->setEmailConfirmed(false);
            if ($this->getParameter('confirm_email')) {
                $user->setEnabled(false);
                $user->setConfirmationToken($tokenGenerator->generateToken());
            } else {
                $user->setEnabled(true);
            }

            $entityManager->persist($user);
            $entityManager->flush();
            $email = (new TemplatedEmail())
                ->from(new Address('noreply@black-org.fr', 'Black Org'))
                ->to(new Address($user->getEmail(), $user->getUsername()))
                ->subject('Bienvenue sur Black Org')
                ->textTemplate('email/registration.txt.twig')
                ->htmlTemplate('email/registration.html.twig')
                ->context([
                    'user' => $user,
                ]);
            $mailer->send($email);

            if ($this->getParameter('confirm_email')) {
                $confirmMessage = 'Ton compte a été créé. Tu devras cliquer sur le lien de confirmation envoyé à ton adresse de courriel avant de pouvoir te connecter.';
            } else {
                $confirmMessage = 'Ton compte a été créé. Tu peux dès à présent te connecter.';
            }
            $request->getSession()->getFlashBag()->add('success', $confirmMessage);

            return $this->redirectToRoute('homepage');
        }

        return $this->render('user/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route("/register/confirm/{id}/{confirmationToken}", name: "user_register_confirm", methods: ["GET"])]
    public function confirmRegister(Request $request, EntityManagerInterface $entityManager, User $user, string $confirmationToken): Response
    {
        if ($user->getConfirmationToken() === $confirmationToken) {
            $user->setEmailConfirmed(true);
            $user->setConfirmationToken(null);
            $confirmMessage = 'Merci d’avoir confirmé ton adresse de courriel.';
            if (!$user->isEnabled()) {
                $confirmMessage .= ' Tu peux dès à présent te connecter.';
                $user->setEnabled(true);
            }
            $entityManager->flush();
            $request->getSession()->getFlashBag()->add('success', $confirmMessage);
        } else {
            $request->getSession()->getFlashBag()->add('danger', 'Ce lien n’est pas ou plus valide. En cas de problème, merci d’utiliser le formulaire de contact.');
        }

        return $this->redirectToRoute('homepage');
    }

    #[Route("/register/disprove/{id}/{confirmationToken}", name: "user_register_disprove", methods: ["GET"])]
    public function disproveRegister(Request $request, EntityManagerInterface $entityManager, User $user, string $confirmationToken): Response
    {
        if ($user->getConfirmationToken() === $confirmationToken) {
            $user->setEmailConfirmed(false);
            $user->setEmail(null);
            $user->setConfirmationToken(null);
            $user->setEnabled(false);
            $entityManager->flush();
            $request->getSession()->getFlashBag()->add('success', 'Merci de nous avoir signalé l’erreur dans l’adresse de courriel, elle a bien été retirée et le compte créé a été verrouillé.');
        } else {
            $request->getSession()->getFlashBag()->add('danger', 'Ce lien n’est pas ou plus valide. En cas de problème, merci d’utiliser le formulaire de contact.');
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    #[Route("/account", name: "user_account", methods: ["GET", "POST"])]
    public function account(Request $request, EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher, TokenGenerator $tokenGenerator, MailerInterface $mailer): Response
    {
        $emailForm = $this->createForm(EmailChangeFormType::class, $this->getUser());
        $passwordForm = $this->createForm(PasswordChangeFormType::class);

        $emailForm->handleRequest($request);
        $passwordForm->handleRequest($request);

        if ($emailForm->isSubmitted() && $emailForm->isValid()) {
            $this->getUser()->setConfirmationToken($tokenGenerator->generateToken());

            $entityManager->flush();
            $email = (new TemplatedEmail())
                ->from(new Address('noreply@black-org.fr', 'Black Org'))
                ->to(new Address($this->getUser()->getPendingEmail(), $this->getUser()->getUsername()))
                ->subject('[Black Org] Confirmation de changement d’adresse de courriel')
                ->textTemplate('email/confirmEmail.txt.twig')
                ->htmlTemplate('email/confirmEmail.html.twig')
                ->context([
                    'user' => $this->getUser(),
                ]);
            $mailer->send($email);

            $request->getSession()->getFlashBag()->add('success', 'Pour valider le changement d’adresse, merci de cliquer sur le lien de confirmation dans le courriel qui vient de t’être envoyé.');

            return $this->redirectToRoute('user_account');
        }

        if ($passwordForm->isSubmitted() && $passwordForm->isValid()) {
            // Current password check is already done by the security assert constraint
            // encode the plain password
            $this->getUser()->setPassword(
                $passwordHasher->hashPassword(
                    $this->getUser(),
                    $passwordForm->get('plainPassword')->getData()
                )
            );
            $entityManager->flush();
            $request->getSession()->getFlashBag()->add('success', 'Ton mot de passe a bien été modifié.');

            return $this->redirectToRoute('user_account');
        }

        return $this->render('user/account.html.twig', [
            'passwordForm' => $passwordForm->createView(),
            'emailForm' => $emailForm->createView(),
        ]);
    }

    #[Route("/email/confirm/{id}/{confirmationToken}", name: "user_email_confirm", methods: ["GET"])]
    public function confirmEmail(Request $request, EntityManagerInterface $entityManager, User $user, string $confirmationToken): Response
    {
        if ($user->isEnabled() && $user->getConfirmationToken() === $confirmationToken && null !== $user->getPendingEmail()) {
            $user->setEmail($user->getPendingEmail());
            $user->setPendingEmail(null);
            $user->setEmailConfirmed(true);
            $user->setConfirmationToken(null);
            $confirmMessage = 'Merci d’avoir confirmé ta nouvelle adresse de courriel.';
            $entityManager->flush();
            $request->getSession()->getFlashBag()->add('success', $confirmMessage);
        } else {
            $request->getSession()->getFlashBag()->add('danger', 'Ce lien n’est pas ou plus valide. En cas de problème, merci d’utiliser le formulaire de contact.');
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @return Response
     */
    #[Route("/email/disprove/{id}/{confirmationToken}", name: "user_email_disprove", methods: ["GET"])]
    public function disproveEmail(Request $request, EntityManagerInterface $entityManager, User $user, string $confirmationToken)
    {
        if ($user->isEnabled() && $user->getConfirmationToken() === $confirmationToken && null !== $user->getPendingEmail()) {
            // We keep old email, we just remove the "pending email" field
            $user->setPendingEmail(null);
            $user->setConfirmationToken(null);
            $entityManager->flush();
            $request->getSession()->getFlashBag()->add('success', 'Merci de nous avoir signalé l’erreur dans l’adresse de courriel.');
        } else {
            $request->getSession()->getFlashBag()->add('danger', 'Ce lien n’est pas ou plus valide. En cas de problème, merci d’utiliser le formulaire de contact.');
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * Section page controller.
     */
    #[Route("/submissions", name: "user_submissions", methods: ["GET"])]
    public function userSubmissionsAction(EntityManagerInterface $entityManager): Response
    {
        $submissions = $entityManager->getRepository(Submission::class)->getSubmissions(null, false, 'ASC', null, $this->getUser());
        $validatedSubmissions = $entityManager->getRepository(Submission::class)->getSubmissions(null, true, 'DESC', 50, $this->getUser());

        return $this->render('user/submissions.html.twig', [
            'submissions' => $submissions,
            'validatedSubmissions' => $validatedSubmissions,
        ]);
    }
}

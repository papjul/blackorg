<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\CaseEntity;
use App\Entity\Changelog;
use App\Entity\Chapter;
use App\Entity\Contact;
use App\Entity\Submission;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PageController.
 */
class PageController extends AbstractController
{
    /**
     * Anime page controller.
     */
    #[Route("/anime", name: "anime", methods: ["GET"])]
    public function animeAction(): Response
    {
        return $this->render('page/anime.html.twig', ['shortTitle' => 'Anime']);
    }

    /**
     * APTX page controller.
     */
    #[Route("/aptx", name: "aptx", methods: ["GET"])]
    public function aptxAction(): Response
    {
        $breadcrumb = [
            $this->generateUrl('characters') => 'Personnages',
            $this->generateUrl('org') => 'L’Organisation Noire',
        ];

        return $this->render('page/aptx.html.twig', ['breadcrumb' => $breadcrumb]);
    }

    /**
     * Author page controller.
     */
    #[Route("/author", name: "author", methods: ["GET"])]
    public function authorAction(): Response
    {
        return $this->render('page/author.html.twig', ['shortTitle' => 'Auteur']);
    }

    /**
     * Changelog page controller.
     */
    #[Route("/changelog", name: "changelog", methods: ["GET"])]
    public function changelogAction(EntityManagerInterface $entityManager): Response
    {
        $changes = $entityManager->getRepository(Changelog::class)->findAllGroupedByDate();

        return $this->render('page/changelog.html.twig', ['changes' => $changes]);
    }

    /**
     * Contributor License Agreement page controller.
     */
    #[Route("/cla", name: "cla", methods: ["GET"])]
    public function claAction(): Response
    {
        return $this->render('page/cla.html.twig');
    }

    /**
     * Contributing page controller.
     */
    #[Route("/contributing", name: "contributing", methods: ["GET"])]
    public function contributingAction(EntityManagerInterface $entityManager): Response
    {
        $cases = $entityManager->getRepository(CaseEntity::class)->findCasesWithMissingFields();
        $chapters = $entityManager->getRepository(Chapter::class)->findChaptersWithMissingFields();
        return $this->render('page/contributing.html.twig', [
            'cases' => $cases,
            'chapters' => $chapters,
        ]);
    }

    /**
     * FAQ page controller.
     */
    #[Route("/faq", name: "faq", methods: ["GET"])]
    public function faqAction(): Response
    {
        return $this->render('page/faq.html.twig');
    }

    /**
     * Games page controller.
     */
    #[Route("/games", name: "games", methods: ["GET"])]
    public function gamesAction(): Response
    {
        return $this->render('page/games.html.twig');
    }

    /**
     * Legal page controller.
     */
    #[Route("/legal", name: "legal", methods: ["GET"])]
    public function legalAction(): Response
    {
        return $this->render('page/legal.html.twig');
    }

    /**
     * Links page controller.
     */
    #[Route("/links", name: "links", methods: ["GET"])]
    public function linksAction(): Response
    {
        return $this->render('page/links.html.twig');
    }

    /**
     * Privacy policy page controller.
     *
     * @throws NonUniqueResultException
     */
    #[Route("/privacy", name: "privacy", methods: ["GET"])]
    public function privacyAction(EntityManagerInterface $entityManager): Response
    {
        $oldestSubmissionWithIp = $entityManager->getRepository(Submission::class)->getOldestSubmissionWithIp();
        $oldestContactMessageWithIp = $entityManager->getRepository(Contact::class)->getOldestContactMessageWithIp();
        $oldestContactMessageWithEmail = $entityManager->getRepository(Contact::class)->getOldestContactMessageWithEmail();

        $last_update = filemtime($this->getParameter('twig.default_path') . '/page/privacy.html.twig');

        return $this->render('page/privacy.html.twig', [
            'last_update' => $last_update,
            'oldestSubmissionWithIp' => $oldestSubmissionWithIp,
            'oldestContactMessageWithIp' => $oldestContactMessageWithIp,
            'oldestContactMessageWithEmail' => $oldestContactMessageWithEmail,
        ]);
    }

    /**
     * Progress page controller.
     */
    #[Route("/progress", name: "progress", methods: ["GET"])]
    public function progressAction(): Response
    {
        $last_update = filemtime($this->getParameter('twig.default_path') . '/page/progress.html.twig');

        return $this->render('page/progress.html.twig', ['last_update' => $last_update]);
    }

    /**
     * Sitemap page controller.
     */
    #[Route("/sitemap", name: "sitemap", methods: ["GET"])]
    public function sitemapAction(): Response
    {
        return $this->render('page/sitemap.html.twig');
    }

    /**
     * Timeline page controller.
     */
    #[Route("/timeline", name: "timeline", methods: ["GET"])]
    public function timelineAction(): Response
    {
        return $this->render('page/timeline.html.twig');
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\Album;
use App\Entity\Chapter;
use App\Entity\Episode;
use App\Entity\Volume;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

/**
 * Class StatsController.
 */
class StatsController extends AbstractController
{
    /**
     * Stats page controller.
     *
     * @throws Exception
     */
    #[Route("/stats", name: "stats", methods: ["GET"])]
    public function statsAction(EntityManagerInterface $entityManager, ChartBuilderInterface $chartBuilder): Response
    {
        $chapters = $entityManager->getRepository(Chapter::class)->getChaptersByDate();
        $episodes = $entityManager->getRepository(Episode::class)->getEpisodesForStats();

        // Volumes
        $chartVolumes = $chartBuilder->createChart(Chart::TYPE_LINE);
        $dataVolumes = [
            'datasets' => [
                [
                    'label' => 'France',
                    'backgroundColor' => 'rgba(255, 255, 255, 0.5)',
                    //'fill' => 'origin',
                    'data' => [],
                ],
                [
                    'label' => 'Japon',
                    'backgroundColor' => 'rgba(255, 153, 0, 0.5)',
                    //'fill' => 'origin',
                    'data' => [],
                ],
            ],
        ];
        $volumes = $entityManager->getRepository(Volume::class)->findBy(['type' => 'original']);
        foreach ($volumes as $volume) {
            if ($volume->getDateFr() != null) {
                $dataVolumes['datasets'][0]['data'][] = [
                    'x' => $volume->getDateFr()->getTimestamp(),
                    'y' => $volume->getNumber(),
                ];
            }
            if ($volume->getDateJp() != null) {
                $dataVolumes['datasets'][1]['data'][] = [
                    'x' => $volume->getDateJp()->getTimestamp(),
                    'y' => $volume->getNumber(),
                ];
            }
        }
        $chartVolumes->setData($dataVolumes);
        /*$chartVolumes->setOptions([
            'scales' => [
                'xAxes' => [
                    [
                        'type' => "time",
                        'time' => [
                            'unit' => "year",
                            'parser' => "dd/MM/yyyy",
                        ],
                        'scaleLabel' => [
                            'display' => true,
                            'labelString' => "Date"
                        ]
                    ]
                ],
            ]
        ]);*/

        // Types
        $chartTypes = $chartBuilder->createChart(Chart::TYPE_BAR);
        $dataTypes = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Manga',
                    'backgroundColor' => '#FFEFB5',
                ],
                [
                    'label' => 'Hors série',
                    'backgroundColor' => '#FFB5B5',
                ],
            ],
        ];
        $casesTypes = [];
        foreach ($episodes as $ep) {
            if (!array_key_exists($ep->getDate()->format('Y'), $casesTypes)) {
                $casesTypes[$ep->getDate()->format('Y')] = ['manga' => 0, 'anime' => 0];
            }
            if (null !== $ep->getCases() && (is_countable($ep->getCases()) ? count($ep->getCases()) : 0)) {
                if ('manga' == $ep->getCases()[0]->getType()) {
                    ++$casesTypes[$ep->getDate()->format('Y')]['manga'];
                } elseif ('anime' == $ep->getCases()[0]->getType()) {
                    ++$casesTypes[$ep->getDate()->format('Y')]['anime'];
                }
            }
        }
        foreach ($casesTypes as $year => $caseType) {
            $dataTypes['labels'][] = $year;
            $dataTypes['datasets'][0]['data'][] = $caseType['manga'];
            $dataTypes['datasets'][1]['data'][] = $caseType['anime'];
        }
        $chartTypes->setData($dataTypes);

        // Rating
        $chartRating = $chartBuilder->createChart(Chart::TYPE_SCATTER);
        $dataRating = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Audience',
                    'backgroundColor' => '#007ACC',
                    'data' => [],
                ],
            ],
        ];
        foreach ($episodes as $episode) {
            $dataRating['labels'][] = $episode->getNumber();
            $dataRating['datasets'][0]['data'][] = [
                'x' => $episode->getNumber(),
                'y' => $episode->getRating(),
            ];
        }
        $chartRating->setData($dataRating);

        // Opening
        $chartOpening = $chartBuilder->createChart(Chart::TYPE_BAR);
        $dataOpening = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Nombre d’épisodes',
                    'backgroundColor' => '#e268ef',
                    'data' => [],
                ],
            ],
        ];
        $openingAlbum = $entityManager->getRepository(Album::class)->getOpeningAlbum();
        foreach ($openingAlbum->getTracks() as $openingTrack) {
            $dataOpening['labels'][] = $openingTrack->getNumber();
            $dataOpening['datasets'][0]['data'][] = [
                'x' => $openingTrack->getNumber(),
                'y' => is_countable($openingTrack->getEpisodesOp()) ? count($openingTrack->getEpisodesOp()) : 0,
            ];
        }
        $chartOpening->setData($dataOpening);

        // Ending
        $chartEnding = $chartBuilder->createChart(Chart::TYPE_BAR);
        $dataEnding = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Nombre d’épisodes',
                    'backgroundColor' => '#e268ef',
                    'data' => [],
                ],
            ],
        ];
        $endingAlbum = $entityManager->getRepository(Album::class)->getEndingAlbum();
        foreach ($endingAlbum->getTracks() as $endingTrack) {
            $dataEnding['labels'][] = $endingTrack->getNumber();
            $dataEnding['datasets'][0]['data'][] = [
                'x' => $endingTrack->getNumber(),
                'y' => is_countable($endingTrack->getEpisodesEd()) ? count($endingTrack->getEpisodesEd()) : 0,
            ];
        }
        $chartEnding->setData($dataEnding);

        return $this->render('page/stats.html.twig', [
            'chapters' => $chapters,
            'chart_volumes' => $chartVolumes,
            'chart_types' => $chartTypes,
            'chart_rating' => $chartRating,
            'chart_opening' => $chartOpening,
            'chart_ending' => $chartEnding,
        ]);
    }
}

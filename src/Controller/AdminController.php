<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Submission;
use App\Entity\User;
use App\Submission\SubmissionFormHandler;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionException;
use ReflectionMethod;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AdminController.
 */
#[Route("/admin")]
class AdminController extends AbstractController
{
    /**
     * Admin page controller.
     */
    #[Route('', name: "admin", methods: ["GET"])]
    public function indexAction(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    /**
     * Section page controller.
     */
    #[Route("/contact", name: "admin_contact", methods: ["GET"])]
    #[IsGranted("ROLE_SUPER_ADMIN")]
    public function contactAction(EntityManagerInterface $entityManager): Response
    {
        $messages = $entityManager->getRepository(Contact::class)->findAll();

        return $this->render('admin/contact.html.twig', [
            'breadcrumb' => [$this->generateUrl('admin') => 'Administration'],
            'noindex' => true,
            'messages' => $messages,
        ]);
    }

    /**
     * Section page controller.
     */
    #[IsGranted("ROLE_ADMIN")]
    #[Route("/users", name: "admin_users", methods: ["GET"])]
    public function usersAction(EntityManagerInterface $entityManager): Response
    {
        $users = $entityManager->getRepository(User::class)->findAll();

        return $this->render('admin/users.html.twig', [
            'breadcrumb' => [$this->generateUrl('admin') => 'Administration'],
            'shortTitle' => 'Utilisateurs',
            'noindex' => true,
            'users' => $users,
        ]);
    }

    /**
     * Section page controller.
     */
    #[Route("/{section<(albums|cases|chapters|characters|dvds|episodes|gadgets|situations|tracks|volumes)>}", name: "admin_section", methods: ["GET"])]
    public function sectionAction(EntityManagerInterface $entityManager, SubmissionFormHandler $submissionFormHandler, TranslatorInterface $translator, string $section): Response
    {
        $submissions = $entityManager->getRepository(Submission::class)->getSubmissions($section);
        $validatedSubmissions = $entityManager->getRepository(Submission::class)->getSubmissions($section, true, 'DESC', 10);
        $entity = $submissionFormHandler->getEntity($section);

        return $this->render('admin/section.html.twig', [
            'breadcrumb' => [$this->generateUrl('admin') => 'Administration'],
            'noindex' => true,
            'shortTitle' => $translator->trans($entity) . 's',
            'section' => $section,
            'entity' => $entity,
            'submissions' => $submissions,
            'validatedSubmissions' => $validatedSubmissions,
        ]);
    }

    /**
     * Section page controller.
     */
    #[Route("/submissions", name: "admin_submissions", methods: ["GET"])]
    public function submissionsAction(EntityManagerInterface $entityManager): Response
    {
        $submissions = $entityManager->getRepository(Submission::class)->getSubmissions();
        $validatedSubmissions = $entityManager->getRepository(Submission::class)->getSubmissions(null, true, 'DESC', 50);

        return $this->render('admin/submissions.html.twig', [
            'breadcrumb' => [$this->generateUrl('admin') => 'Administration'],
            'noindex' => true,
            'submissions' => $submissions,
            'validatedSubmissions' => $validatedSubmissions,
        ]);
    }

    /**
     * View page controller.
     *
     * @throws ReflectionException
     */
    #[Route("/submissions/view/{id<[1-9]\d*>}", name: "admin_submission_view", methods: ["GET"])]
    public function viewAction(EntityManagerInterface $entityManager, TranslatorInterface $translator, SubmissionFormHandler $submissionFormHandler, Submission $submission): Response
    {
        if (null === $submission->getPending()) {
            throw new HttpException(404, 'La suggestion a été trouvée mais l’entité associée est introuvable. Merci de contacter l’administrateur avec la référence #' . $submission->getId() . '.');
        }

        $reflectionMethod = new ReflectionMethod($submission->getPending()::class, 'getCurrent');
        $section = $entityManager->getRepository($reflectionMethod->getReturnType()->getName())->getRouteSectionName();

        $breadcrumb = [
            $this->generateUrl('admin') => 'Administration',
            $this->generateUrl('admin_submissions') => 'Suggestions',
            $this->generateUrl('admin_section', ['section' => $section]) => $translator->trans($submissionFormHandler->getEntity($section)) . 's',
        ];

        return $this->render('admin/view.html.twig', [
            'breadcrumb' => $breadcrumb,
            'noindex' => true,
            'submission' => $submission,
            'section' => $section,
        ]);
    }

    /**
     * Add a submission.
     *
     * @param null $parameter Optional parameter
     *
     */
    #[Route("/{section<(albums|cases|chapters|characters|dvds|episodes|gadgets|situations|tracks|volumes)>}/add", name: "admin_submission_add", methods: ["GET", "POST"])]
    #[Route("/{section<(cases)>}/add/{parameter<(manga|anime|movie|movie_special|ova|magic_file|dc_special|mk_special|mk1412|tanpenshu|drama_special|drama_episode|wps)>}", name: "admin_submission_add_case", methods: ["GET", "POST"])]
    #[Route("/{section<(chapters)>}/add/{parameter<(original|tokubetsu|movie|wps)>}", name: "admin_submission_add_chapter", methods: ["GET", "POST"])]
    #[Route("/{section<(situations)>}/add/{parameter<[1-9]\d*>}", name: "admin_submission_add_situation", methods: ["GET", "POST"])]
    #[Route("/{section<(tracks)>}/add/{parameter<[1-9]\d*>}", name: "admin_submission_add_track", methods: ["GET", "POST"])]
    #[Route("/{section<(volumes)>}/add/{parameter<(original|tokubetsu|movie|wps)>}", name: "admin_submission_add_volume", methods: ["GET", "POST"])]
    public function submissionAdd(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator, SubmissionFormHandler $submissionFormHandler, string $section, $parameter = null): Response
    {
        $this->denyAccessUnlessGranted($this->getParameter('allowed_roles_submission'));

        return $this->formAction($request, $entityManager, $translator, $submissionFormHandler, 'add', $section, null, $parameter);
    }

    /**
     * Form page service.
     *
     * @param string|null $section
     * @param int|null $id
     * @param null $parameter
     *
     */
    private function formAction(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator, SubmissionFormHandler $submissionFormHandler, string $action, string $section = null, int $id = null, $parameter = null): Response
    {
        $pendingEntity = null;
        $originalEntity = null;
        $entityName = $submissionFormHandler->getEntity($section);
        $entityFQCN = $submissionFormHandler->getEntityFQCN($entityName);
        $pendingEntityFQCN = $submissionFormHandler->getPendingEntityFQCN($entityName);
        $entityRepository = $entityManager->getRepository($entityFQCN);

        if ('add' == $action) {
            $originalEntity = null;
            $pendingEntity = $entityRepository->getDefaultEntity($parameter);
            $similarPendings = $entityRepository->findSimilarPendings(null);
        } elseif ('edit' == $action) {
            $originalEntity = $entityRepository->find($id);
            if (is_null($originalEntity)) {
                throw new HttpException(404, 'L’entité à modifier n’a pu être trouvée.');
            }
            $pendingEntity = $entityRepository->insertDataFromEntity($originalEntity, new $pendingEntityFQCN());
            $similarPendings = $entityRepository->findSimilarPendings($originalEntity->getId());
        }

        $form = $submissionFormHandler->getForm($entityFQCN, $pendingEntity, $action, $id, $this->getUser());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formEntity = $form->getData();

            if ('edit' == $action) {
                $pendingEntity->setCurrent($originalEntity);
            }
            $changedFields = $entityManager->getRepository($entityFQCN)->getChangedFields($form, $pendingEntity);

            if ('edit' == $action && !(is_countable($changedFields) ? count($changedFields) : 0)) {
                $form->addError(new FormError('Aucune modification n’a été effectuée.'));
            } else {
                $submission = new Submission();
                $submission->setComm($form->get('comm')->getData());
                $submission->setDateSubmitted(new DateTime());
                $submission->setUser($this->getUser());
                $submission->setIp($request->getClientIp());
                $submission->setValidated(false);
                $submission->setDeleted(false);
                $submission->setChangedFields($changedFields);

                $pendingEntity->setSubmission($submission);
                $submission->setPending($pendingEntity);
                $entityManager->persist($submission);
                $entityManager->persist($pendingEntity);
                $entityManager->flush();

                $request->getSession()->getFlashBag()->add('success', 'Ta suggestion a bien été enregistrée sous la référence #' . $submission->getId() . '. Merci de ta contribution.' . (('add' != $action) ? '' : ' [Ajouter ' . $translator->trans('a' . $entityName) . '](/admin/' . $section . '/add)</a>'));

                return $this->redirectToRoute('admin_submission_view', ['id' => $submission->getId()]);
            }
        }

        $breadcrumb = [
            $this->generateUrl('admin') => 'Administration',
            $this->generateUrl('admin_submissions') => 'Suggestions',
        ];

        $shortTitle = ('edit' == $action) ? 'Modifier' : 'Ajouter';

        return $this->render('admin/form.html.twig', [
            'breadcrumb' => $breadcrumb,
            'shortTitle' => $shortTitle,
            'noindex' => true,
            'section' => $section,
            'action' => $action,
            'id' => $id,
            'entityName' => $entityName,
            'originalEntity' => $originalEntity,
            'similarPendings' => $similarPendings,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Edit an entity
     *
     *
     */
    #[Route("/{section<(albums|cases|chapters|characters|dvds|episodes|gadgets|situations|tracks|volumes)>}/edit/{id<[1-9]\d*>}", name: "admin_submission_edit", methods: ["GET", "POST"])]
    public function submissionEdit(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator, SubmissionFormHandler $submissionFormHandler, string $section, int $id): Response
    {
        $this->denyAccessUnlessGranted($this->getParameter('allowed_roles_submission'));

        return $this->formAction($request, $entityManager, $translator, $submissionFormHandler, 'edit', $section, $id, null);
    }

    /**
     * Valid a submission
     */
    #[Route("/submissions/valid/{id<[1-9]\d*>}", name: "admin_submission_valid", methods: ["GET", "POST"])]
    #[IsGranted("ROLE_VALIDATOR")]
    public function submissionValid(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator, SubmissionFormHandler $submissionFormHandler, Submission $submission): Response
    {
        $this->denyAccessUnlessGranted($this->getParameter('allowed_roles_submission')); // Can be greater than previous ROLE_VALIDATOR requirement
        $action = 'valid';
        $entityName = $submission->getType();
        $entityFQCN = $submissionFormHandler->getEntityFQCN($entityName);
        $entityRepository = $entityManager->getRepository($entityFQCN);
        $section = $entityRepository->getRouteSectionName();

        $form = $submissionFormHandler->getForm($entityFQCN, $submission->getPending(), $action, $submission->getId(), $this->getUser());

        $form->get('comm')->setData($submission->getComm());
        $form->get('reason')->setData($submission->getReason());

        $similarPendings = $entityRepository->findSimilarPendings((null !== $submission->getPending()->getCurrent()) ? $submission->getPending()->getCurrent()->getId() : null, $submission->getId());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $flashMessage = $submissionFormHandler->handleValidFormSubmit($form, $submission, $this->getUser());

            $this->addFlash('success', $flashMessage);

            return $this->redirectToRoute('admin_submissions');
        }

        $breadcrumb = [
            $this->generateUrl('admin') => 'Administration',
            $this->generateUrl('admin_submissions') => 'Suggestions',
            $this->generateUrl('admin_section', ['section' => $section]) => $translator->trans($entityName) . 's',
        ];

        return $this->render('admin/form.html.twig', [
            'breadcrumb' => $breadcrumb,
            'shortTitle' => 'Valider',
            'noindex' => true,
            'section' => $section,
            'action' => $action,
            'id' => $submission->getId(),
            'entityName' => $entityName,
            'originalEntity' => $submission->getPending()->getCurrent(),
            'similarPendings' => $similarPendings,
            'form' => $form->createView(),
        ]);
    }
}

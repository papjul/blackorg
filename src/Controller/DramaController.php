<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\CaseEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DramaController.
 */
class DramaController extends AbstractController
{
    /**
     * Drama page controller.
     */
    #[Route("/drama", name: "drama", methods: ["GET"])]
    public function indexAction(EntityManagerInterface $entityManager): Response
    {
        $drama_specials = $entityManager->getRepository(CaseEntity::class)->findBy(['type' => 'drama_special']);
        $drama_episodes = $entityManager->getRepository(CaseEntity::class)->findBy(['type' => 'drama_episode']);

        return $this->render('drama/index.html.twig', [
            'drama_specials' => $drama_specials,
            'drama_episodes' => $drama_episodes,
        ]);
    }

    /**
     * Drama special view page controller.
     */
    #[Route("/drama/live{number<[1-9]\d*>}", name: "drama_special_view", methods: ["GET"])]
    public function dramaSpecialViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findDramaSpecial(number)")] CaseEntity $drama_special): Response
    {
        return $this->dramaViewAction($entityManager, $drama_special);
    }

    /**
     * Drama view page controller.
     */
    private function dramaViewAction(EntityManagerInterface $entityManager, CaseEntity $drama): Response
    {
        $dramaBefore = $entityManager->getRepository(CaseEntity::class)->findOneBy(['number' => $drama->getNumber() - 1, 'type' => $drama->getType()]);
        $dramaAfter = $entityManager->getRepository(CaseEntity::class)->findOneBy(['number' => $drama->getNumber() + 1, 'type' => $drama->getType()]);

        $breadcrumb = [$this->generateUrl('drama') => 'Drama'];
        if ('drama_special' === $drama->getType()) {
            $breadcrumb[$this->generateUrl('drama') . '#drama_specials'] = 'Téléfilms';
        } elseif ('drama_episode' === $drama->getType()) {
            $breadcrumb[$this->generateUrl('drama') . '#drama_episodes'] = 'Série';
        }

        return $this->render('drama/view.html.twig', [
            'breadcrumb' => $breadcrumb,
            'drama' => $drama,
            'dramaBefore' => $dramaBefore,
            'dramaAfter' => $dramaAfter,
        ]);
    }

    /**
     * Drama episode view page controller.
     */
    #[Route("/drama/ep{number<[1-9]\d*>}", name: "drama_episode_view", methods: ["GET"])]
    public function dramaEpisodeViewAction(EntityManagerInterface $entityManager, #[MapEntity(expr: "repository.findDramaEpisode(number)")] CaseEntity $drama_episode): Response
    {
        return $this->dramaViewAction($entityManager, $drama_episode);
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\CaseEntity;
use App\Entity\Character;
use App\Entity\CharacterCategory;
use App\Entity\Gadget;
use App\Twig\AppExtension;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class CharacterController.
 */
class CharacterController extends AbstractController
{
    /**
     * Characters page controller.
     */
    #[Route("/characters", name: "characters", methods: ["GET"])]
    public function indexAction(EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $char_categories = $entityManager->getRepository(CharacterCategory::class)->getCharacterCategories();

        if (!(is_countable($char_categories) ? count($char_categories) : 0)) {
            throw new HttpException(404, 'Aucun personnage n’a été trouvé. ' . $translator->trans('TempAbortMsg'));
        }

        $menu_items = [];
        foreach ($char_categories as $char_cat) {
            $menu_infos = [
                'url' => '#cat' . $char_cat->getId(),
                'name' => $char_cat->getName(),
                'below' => [],
            ];
            if (is_null($char_cat->getParent())) {
                $menu_items[$char_cat->getId()] = $menu_infos;
            } elseif (is_null($char_cat->getParent()->getParent())) {
                $menu_items[$char_cat->getParent()->getId()]['below'][$char_cat->getId()] = $menu_infos;
            } elseif (is_null($char_cat->getParent()->getParent()->getParent())) {
                $menu_items[$char_cat->getParent()->getParent()->getId()]['below'][$char_cat->getParent()->getId()]['below'][$char_cat->getId()] = $menu_infos;
            }
        }

        return $this->render('characters/index.html.twig', [
            'char_categories' => $char_categories,
            'menu_items' => $menu_items,
        ]);
    }

    /**
     * Character view page controller.
     */
    #[Route("/characters/{shortname}", name: "character_view", methods: ["GET"])]
    public function characterViewAction(EntityManagerInterface $entityManager, Character $character): Response
    {
        $cat = $character->getCharacterCategory();
        $breadcrumb = [];
        while (!is_null($cat)) {
            $breadcrumb[$this->generateUrl('characters') . '#cat' . $cat->getId()] = $cat->getName();
            $cat = $cat->getParent();
        }
        $breadcrumb[$this->generateUrl('characters')] = 'Personnages';

        return $this->render('characters/view.html.twig', [
            'breadcrumb' => array_reverse($breadcrumb),
            'character' => $character,
            'genders' => $entityManager->getRepository(Character::class)->getGenders(),
        ]);
    }

    /**
     * Character view appearances page controller.
     */
    #[Route("/characters/{shortname}/appearances", name: "character_appearances", methods: ["GET"])]
    public function characterAppearancesAction(EntityManagerInterface $entityManager, AppExtension $appExtension, Character $character): Response
    {
        $cat = $character->getCharacterCategory();
        $breadcrumb = [];
        $breadcrumb[$appExtension->getCharacterPath($character)] = $character->getDisplayName();
        while (!is_null($cat)) {
            $breadcrumb[$this->generateUrl('characters') . '#cat' . $cat->getId()] = $cat->getName();
            $cat = $cat->getParent();
        }
        $breadcrumb[$this->generateUrl('characters')] = 'Personnages';

        $data = ['characters' => [$character]];
        $cases = $entityManager->getRepository(CaseEntity::class)->manualSearch($data);

        return $this->render('characters/appearances.html.twig', [
            'breadcrumb' => array_reverse($breadcrumb),
            'shortTitle' => 'Apparitions',
            'character' => $character,
            'cases' => $cases,
        ]);
    }

    /**
     * Gadgets page controller.
     */
    #[Route("/gadgets", name: "gadgets", methods: ["GET"])]
    public function gadgetsAction(EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $gadgets = $entityManager->getRepository(Gadget::class)->getGadgets();

        if (!(is_countable($gadgets) ? count($gadgets) : 0)) {
            throw new HttpException(404, 'Aucun gadget n’a été trouvé. ' . $translator->trans('TempAbortMsg'));
        }

        return $this->render('characters/gadgets.html.twig', ['gadgets' => $gadgets]);
    }

    /**
     * Gadget view page controller.
     */
    #[Route("/gadgets/{id<[1-9]\d*>}", name: "gadget_view", methods: ["GET"])]
    public function gadgetViewAction(Gadget $gadget): Response
    {
        $breadcrumb = [$this->generateUrl('gadgets') => 'Gadgets'];

        return $this->render('characters/gadget-view.html.twig', [
            'breadcrumb' => $breadcrumb,
            'gadget' => $gadget,
        ]);
    }

    /**
     * Gadget view uses page controller.
     */
    #[Route("/gadgets/{id<[1-9]\d*>}/uses", name: "gadget_uses", methods: ["GET"])]
    public function gadgetsUsesAction(EntityManagerInterface $entityManager, AppExtension $appExtension, Gadget $gadget): Response
    {
        $cases = $entityManager->getRepository(CaseEntity::class)->manualSearch(['gadgets' => [$gadget]]);

        $breadcrumb = [
            $this->generateUrl('gadgets') => 'Gadgets',
            $appExtension->getGadgetPath($gadget) => $gadget->getDisplayName(),
        ];

        return $this->render('characters/gadget-uses.html.twig', [
            'breadcrumb' => $breadcrumb,
            'shortTitle' => 'Utilisations',
            'gadget' => $gadget,
            'cases' => $cases,
        ]);
    }

    /**
     * Org page controller.
     */
    #[Route("/org", name: "org", methods: ["GET"])]
    public function orgAction(): Response
    {
        $breadcrumb = [$this->generateUrl('characters') => 'Personnages'];

        return $this->render('characters/org.html.twig', ['breadcrumb' => $breadcrumb]);
    }

    /**
     * Relationships page controller.
     */
    #[Route("/relationships", name: "relationships", methods: ["GET"])]
    public function relationshipsAction(): Response
    {
        $breadcrumb = [$this->generateUrl('characters') => 'Personnages'];

        return $this->render('characters/relationships.html.twig', [
            'breadcrumb' => $breadcrumb,
            'shortTitle' => 'Relations',
        ]);
    }
}

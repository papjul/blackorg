<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\CaseEntity;
use App\Entity\Situation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Class DebugController.
 */
#[Route("/debug", methods: ["GET"])]
#[IsGranted("ROLE_SUPER_ADMIN")]
class DebugController extends AbstractController
{
    /**
     * Create a situation for cases with no situations.
     */
    /*#[Route("/fixCasesWithNoSituation", name: "fixCasesWithNoSituation", methods: ["GET"])
    #[IsGranted("ROLE_SUPER_ADMIN")]
    public function fixCasesWithNoSituationAction(EntityManagerInterface $entityManager): Response
    {
        $cases = $entityManager->getRepository(CaseEntity::class)->findBy([], ['id' => 'ASC']);

        foreach ($cases as $case) {
            if (count($case->getSituations()) < 1) {
                echo 'Création d’une situation pour ' . $case . '<br />';
                $situation = new Situation();
                $situation->setCase($case);
                $situation->setNumber(1);
                $entityManager->persist($situation);
            }
        }

        $entityManager->flush();

        return new Response();
    }

    /*
     * Migrate old_victim field to ManyToMany situation type.
     */
    /*#[Route("/migrateOldSituationVictim", name: "migrateOldSituationVictim", methods: ["GET"])
    #[IsGranted("ROLE_SUPER_ADMIN")]
    public function migrateOldSituationVictimAction(EntityManagerInterface $entityManager): Response
    {
        $situations = $entityManager->getRepository(Situation::class)->findAll();
        foreach ($situations as $situation) {
            if (null !== $situation->getOldVictim()) {
                echo $situation->getId().': '.$situation->getOldVictim().' =&gt; ';

                $victimArray = explode(',', $situation->getOldVictim());
                foreach ($victimArray as &$victim) {
                    $victim = trim($victim);
                }
                $situation->setVictims($victimArray);

                print_r($situation->getVictims());
                echo '<br />';
            }
        }

        $entityManager->flush();

        return new Response();
    }*/
}

<?php

declare(strict_types=1);

namespace App\Util;

use Noi\ParsedownRubyText;
use Parsedown;
use Symfony\Component\HtmlSanitizer\HtmlSanitizerInterface;

/**
 * Class Markdown.
 */
class Markdown
{
    private readonly Parsedown $parser;
    private readonly ParsedownRubyText $parserRuby;

    /**
     * Markdown constructor.
     */
    public function __construct(private readonly HtmlSanitizerInterface $sanitizer)
    {
        $this->parser = new Parsedown();
        $this->parser->setBreaksEnabled(true);
        $this->parserRuby = new ParsedownRubyText();
    }

    public function toFurigana(?string $text): ?string
    {
        return $this->sanitizer->sanitize($this->parserRuby->line($text));
    }

    /**
     * @param string|null $method
     */
    public function toHtml(?string $text, string $method = null): ?string
    {
        if ('inline' === $method) {
            $html = $this->parser->line($text);
        } else {
            $html = $this->parser->text($text);
        }

        return $this->sanitizer->sanitize($html);
    }
}

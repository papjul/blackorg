<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\Character;
use App\Entity\PendingInterface;
use App\Entity\SubmittableEntityInterface;
use App\Submission\SubmissionFormHandler;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Class CharacterRepository.
 */
class CharacterRepository extends AbstractSubmittableEntityRepository
{
    /**
     * CharacterRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     */
    public function __construct(ManagerRegistry $registry, PropertyAccessorInterface $propertyAccessor, SubmissionFormHandler $submissionFormHandler)
    {
        parent::__construct($registry, $propertyAccessor, $submissionFormHandler, Character::class);
    }

    /**
     * @return array|mixed
     */
    public function findAll()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('char', 'cc', 'ccp')
            ->from(Character::class, 'char', 'char.id')
            ->leftJoin('char.character_category', 'cc')
            ->leftJoin('cc.parent', 'ccp');

        return $qb->getQuery()->getResult();
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getCharacter(string $shortname): Character
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('char', 'cc', 'ccp', 'c', 'fa')
            ->from(Character::class, 'char')
            ->leftJoin('char.character_category', 'cc')
            ->leftJoin('cc.parent', 'ccp')
            ->leftJoin('char.cases', 'c')
            ->leftJoin('char.first_appearance', 'fa')
            ->where('char.shortname = ?1')
            ->setParameter(1, $shortname);

        return $qb->getQuery()->getSingleResult();
    }

    public function getGenders(): array
    {
        return [
            Character::GENDER_FEMALE => 'Féminin',
            Character::GENDER_MALE => 'Masculin',
            Character::GENDER_OTHER => 'Autre/Indéfini',
        ];
    }

    /**
     * @param PendingInterface $pendingCharacter
     * @return SubmittableEntityInterface
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function createEntity(PendingInterface $pendingCharacter): SubmittableEntityInterface
    {
        // We first check if the entity is the same as the repository we are in
        if (!is_a($pendingCharacter, $this->getPendingEntityName())) {
            throw new UnexpectedTypeException($pendingCharacter, 'PendingInterface');
        }

        $newEntity = new Character();
        $this->insertDataFromEntity($pendingCharacter, $newEntity);
        $newEntity->setOrder($this->getMaxOrder() + 1); // Only on create

        $this->getEntityManager()->persist($newEntity);
        $this->getEntityManager()->flush();

        return $newEntity;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getMaxOrder(): int
    {
        $qb = $this->createQueryBuilder('c')->select('MAX(c.order)');

        return intval($qb->getQuery()->getSingleScalarResult());
    }
}

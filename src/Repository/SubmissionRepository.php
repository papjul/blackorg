<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\Submission;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class SubmissionRepository.
 */
class SubmissionRepository extends ServiceEntityRepository
{
    /**
     * SubmissionRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Submission::class);
    }

    /**
     * @param string|null $section
     * @return array
     */
    public function getSubmissions(string $section = null, bool $validated = false, string $order = 'ASC', ?int $maxResults = null, ?UserInterface $user = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('s')
            ->from(Submission::class, 's', 's.id')
            ->where('s.validated = :validated')
            ->andWhere('s.deleted = false')
            ->setParameter('validated', $validated)
            ->orderBy('s.dateSubmitted', $order);

        if (null !== $maxResults) {
            $qb->setMaxResults($maxResults);
        }

        if (null !== $user) {
            $qb->andWhere('s.user = :user')
                ->setParameter('user', $user);
        }

        if (null !== $section) {
            if ('albums' === $section) {
                $qb->andWhere('s.albumPending IS NOT NULL');
            } elseif ('cases' === $section) {
                $qb->andWhere('s.casePending IS NOT NULL');
            } elseif ('chapters' === $section) {
                $qb->andWhere('s.chapterPending IS NOT NULL');
            } elseif ('characters' === $section) {
                $qb->andWhere('s.characterPending IS NOT NULL');
            } elseif ('dvds' === $section) {
                $qb->andWhere('s.dvdPending IS NOT NULL');
            } elseif ('episodes' === $section) {
                $qb->andWhere('s.episodePending IS NOT NULL');
            } elseif ('gadgets' === $section) {
                $qb->andWhere('s.gadgetPending IS NOT NULL');
            } elseif ('situations' === $section) {
                $qb->andWhere('s.situationPending IS NOT NULL');
            } elseif ('tracks' === $section) {
                $qb->andWhere('s.trackPending IS NOT NULL');
            } elseif ('volumes' === $section) {
                $qb->andWhere('s.volumePending IS NOT NULL');
            }
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getPendingsCount(): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('COUNT(s)')
            ->from(Submission::class, 's', 's.id')
            ->where('s.validated = false')
            ->andWhere('s.deleted = false');

        return intval($qb->getQuery()->getSingleScalarResult());
    }

    public function getPending(int $id): ?Submission
    {
        return $this->findOneBy([
            'id' => $id,
            'validated' => false,
            'deleted' => false,]);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getOldestSubmissionWithIp(): ?Submission
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('s')
            ->from(Submission::class, 's', 's.id')
            ->where('s.ip IS NOT NULL')
            ->addOrderBy('s.dateSubmitted', 'ASC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return Submission
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateSubmission(Submission $submission)
    {
        $this->getEntityManager()->flush();

        return $submission;
    }

    /**
     * @return Submission
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function validateSubmission(Submission $submission, User $user)
    {
        $submission->setValidated(true);
        $submission->setUserValid($user);
        $submission->setDateValid(new DateTime());
        $submission->setIp(null); // Clear IP when submission is validated for privacy reasons
        $this->getEntityManager()->flush();

        return $submission;
    }

    /**
     * @return Submission
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteSubmission(Submission $submission, User $user)
    {
        $submission->setDeleted(true);
        $submission->setUserValid($user);
        $submission->setDateValid(new DateTime());
        $submission->setIp(null); // Clear IP when submission is deleted for privacy reasons

        $this->getEntityManager()->flush();

        return $submission;
    }
}

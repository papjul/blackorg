<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\PendingInterface;
use App\Entity\Volume;
use App\Entity\VolumePending;
use App\Submission\SubmissionFormHandler;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Class VolumeRepository.
 */
class VolumeRepository extends AbstractSubmittableEntityRepository implements EntityWithEventsRepositoryInterface
{
    final public const VOLUME_NUMBER_MIN = 1;
    final public const VOLUME_NUMBER_MAX = 12;

    protected array $types = [
        'original' => 'Manga original',
        'tokubetsu' => 'Tokubetsu (tomes spéciaux)',
        'movie' => 'Basé sur un film',
        'wps' => 'Wild Police Story',
    ];

    /**
     * VolumeRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     */
    public function __construct(ManagerRegistry $registry, PropertyAccessorInterface $propertyAccessor, SubmissionFormHandler $submissionFormHandler)
    {
        parent::__construct($registry, $propertyAccessor, $submissionFormHandler, Volume::class);
    }

    /**
     * @return array|mixed
     */
    public function findAll()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('v')
            ->from(Volume::class, 'v', 'v.id')
            ->orderBy('v.number', 'ASC')
            ->orderBy('v.type', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findOriginal(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'original']);
    }

    public function findTokubetsu(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'tokubetsu']);
    }

    public function findMovie(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'movie']);
    }

    public function findWps(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'wps']);
    }

    /**
     * @return mixed
     */
    public function findByMonth(int $year, string $month)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('v')
            ->from(Volume::class, 'v')
            ->where($qb->expr()->andX($qb->expr()->eq('EXTRACT(YEAR FROM v.date_jp)', ':year'), $qb->expr()->eq('EXTRACT(MONTH FROM v.date_jp)', ':month')))
            ->orWhere($qb->expr()->andX($qb->expr()->eq('EXTRACT(YEAR FROM v.date_fr)', ':year'), $qb->expr()->eq('EXTRACT(MONTH FROM v.date_fr)', ':month')))
            ->setParameter('year', $year)
            ->setParameter('month', $month);

        return $qb->getQuery()->getResult();
    }

    public function getFirstVolume(string $type = 'original'): Volume
    {
        return $this->findOneBy(['type' => $type], ['number' => 'ASC']);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getLatestFrVolume(): ?Volume
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('v')
            ->from(Volume::class, 'v')
            ->where($qb->expr()->isNotNull('v.date_fr'))
            ->orderBy('v.number', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getMaxVolume(string $type = 'original'): int
    {
        $qb = $this->createQueryBuilder('v')
            ->select('MAX(v.number)')
            ->where('v.type = ?1')
            ->setParameter(1, $type);

        return intval($qb->getQuery()->getSingleScalarResult());
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getVolume(int $id): Volume
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('v', 'ch', 'c')
            ->from(Volume::class, 'v')
            ->leftJoin('v.chapters', 'ch')
            ->leftJoin('ch.case', 'c')
            ->where('v.id = ?1')
            ->orderBy('v.number', 'ASC')
            ->setParameter(1, $id);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @return mixed
     */
    public function getVolumes(string $type)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('v', 'ch')
            ->from(Volume::class, 'v')
            ->leftJoin('v.chapters', 'ch')
            ->where('v.type = ?1')
            ->orderBy('v.number', 'ASC')
            ->setParameter(1, $type);

        return $qb->getQuery()->getResult();
    }

    public function getVolumeNumbers(): array
    {
        $volumeNumbers = [];
        for ($i = self::VOLUME_NUMBER_MIN; $i <= self::VOLUME_NUMBER_MAX; ++$i) {
            $volumeNumbers[$i] = $i;
        }

        return $volumeNumbers;
    }

    public function getVolumeTypes(): array
    {
        return $this->types;
    }

    /**
     * {@inheritdoc}
     *
     * @throws NonUniqueResultException
     */
    public function getDefaultEntity($parameter = null): ?PendingInterface
    {
        $volume = new VolumePending();
        if (null === $parameter) {
            return $volume;
        }
        $volume->setType($parameter);
        $latestVolume = $this->getLatestJpVolume(strval($parameter));
        if (null !== $latestVolume) {
            $volume->setNumber($latestVolume->getNumber() + 1);
        }

        return $volume;
    }

    /**
     *
     * @throws NonUniqueResultException
     */
    public function getLatestJpVolume(string $type = 'original'): ?Volume
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('v')
            ->from(Volume::class, 'v')
            ->where('v.type = :type')
            ->setParameter('type', $type)
            ->orderBy('v.number', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

<?php

declare(strict_types=1);

namespace App\Repository;

/**
 * Interface EntityWithEventsRepositoryInterface.
 */
interface EntityWithEventsRepositoryInterface
{
    /**
     * @param int $year
     * @param string month
     * @return mixed
     */
    public function findByMonth(int $year, string $month);
}

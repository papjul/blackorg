<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\Character;
use App\Entity\Gadget;
use App\Submission\SubmissionFormHandler;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Class GadgetRepository.
 */
class GadgetRepository extends AbstractSubmittableEntityRepository
{
    /**
     * GadgetRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     */
    public function __construct(ManagerRegistry $registry, PropertyAccessorInterface $propertyAccessor, private readonly SubmissionFormHandler $submissionFormHandler)
    {
        parent::__construct($registry, $propertyAccessor, $submissionFormHandler, Gadget::class);
    }

    /**
     * @return array|mixed
     */
    public function findAll()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('g')
            ->from(Gadget::class, 'g', 'g.id');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getGadget($id): Gadget
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('g', 'cb', 'ub', 'fu', 'c')
            ->from(Gadget::class, 'g')
            ->leftJoin('g.created_by', 'cb')
            ->leftJoin('g.used_by', 'ub')
            ->leftJoin('g.first_use', 'fu')
            ->leftJoin('g.cases', 'c')
            ->where('g.id = ?1')
            ->setParameter(1, $id);

        return $qb->getQuery()->getSingleResult();
    }

    public function getGadgets(): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('g', 'cb', 'ub', 'fu')
            ->from(Gadget::class, 'g')
            ->leftJoin('g.created_by', 'cb')
            ->leftJoin('g.used_by', 'ub')
            ->leftJoin('g.first_use', 'fu');

        $gadgetsByCreator = [];
        foreach ($qb->getQuery()->getResult() as $gadget) {
            $gadgetsByCreator[$gadget->getCreatedBy()->getDisplayName()][] = $gadget;
        }

        return $gadgetsByCreator;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    public function hasCharacter(Gadget $gadget, int $characterId): bool
    {
        $character = $this->getEntityManager()->find(Character::class, $characterId);

        return $gadget->getUsedBy()->contains($character);
    }
}

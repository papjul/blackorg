<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\Album;
use App\Entity\CaseEntity;
use App\Entity\Chapter;
use App\Entity\Dvd;
use App\Entity\Episode;
use App\Entity\Event;
use App\Entity\Track;
use App\Entity\Volume;
use App\Submission\SubmissionFormHandler;
use App\Twig\AppExtension;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Class EventRepository.
 */
class EventRepository extends AbstractSubmittableEntityRepository implements EntityWithEventsRepositoryInterface
{
    final public const MIN_YEAR = 1994; // Also need to be updated in EventController::index requirements regexp

    /**
     * EventRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     * @param AppExtension $appExtension
     */
    public function __construct(ManagerRegistry $registry, PropertyAccessorInterface $propertyAccessor, SubmissionFormHandler $submissionFormHandler, private readonly AppExtension $appExtension)
    {
        parent::__construct($registry, $propertyAccessor, $submissionFormHandler, Event::class);
    }

    public function getEventsDate(int $year, string $month): array
    {
        $result = [];

        $events = $this->findByMonth($year, $month);
        foreach ($events as $event) {
            $result[$event->getDate()->format('Y-m-d')][] = [
                // 'editlink' => '/admin/events/edit/' . $event->getId(),
                'title' => $event->getTitle(),
                'content' => $event->getContent(),
                'type' => $event->getType(),
            ];
        }

        $episodes = $this->getEntityManager()->getRepository(Episode::class)->findByMonth($year, $month);
        foreach ($episodes as $episode) {
            if ($episode->getDate()->format('Y-m') == $year . '-' . $month) {
                $result[$episode->getDate()->format('Y-m-d')][] = [
                    'link' => '/episodes/' . $episode->getId(),
                    'editlink' => '/admin/episodes/edit/' . $episode->getId(),
                    'title' => 'Épisode ' . $episode->getDisplayNumber() . (($episode->isSpecial()) ? ' (spécial)' : ''),
                    'content' => 'Épisode ' . $episode->getDisplayNumber() . ' — ' . $episode->getTitleFr() . (((!is_null($episode->getPart()) && 0 != $episode->getPart())) ? ' (Partie ' . $episode->getPart() . ')' : ''),
                    'type' => 0,];
            }
            if (!is_null($episode->getDateRemastered()) && $episode->getDateRemastered()->format('Y-m') == $year . '-' . $month) {
                $result[$episode->getDateRemastered()->format('Y-m-d')][] = [
                    'link' => '/episodes/' . $episode->getId(),
                    'editlink' => '/admin/episodes/edit/' . $episode->getId(),
                    'title' => 'Épisode ' . $episode->getDisplayNumber() . ' (remasterisé)',
                    'content' => 'Épisode ' . $episode->getDisplayNumber() . ' — ' . $episode->getTitleFr() . (((!is_null($episode->getPart()) && 0 != $episode->getPart())) ? ' (Partie ' . $episode->getPart() . ')' : ''),
                    'type' => 0,];
            }
        }

        $volumes = $this->getEntityManager()->getRepository(Volume::class)->findByMonth($year, $month);
        foreach ($volumes as $volume) {
            if ($volume->getDateJp()->format('Y-m') == $year . '-' . $month) {
                $result[$volume->getDateJp()->format('Y-m-d')][] = [
                    'link' => '/volumes/' . (('original' !== $volume->getType()) ? $volume->getType() . '/' : '') . $volume->getNumber(),
                    'editlink' => '/admin/volumes/edit/' . $volume->getId(),
                    'title' => 'Tome ' . (('special' == $volume->getType()) ? 'sp. ' : '') . (('movie' == $volume->getType()) ? 'film ' : '') . (('wps' == $volume->getType()) ? 'WPS ' : '') . $volume->getNumber() . ' Japon',
                    'content' => 'Tome ' . (('special' == $volume->getType()) ? 'spécial ' : '') . (('movie' == $volume->getType()) ? 'du film ' : '') . (('wps' == $volume->getType()) ? 'Wild Police Story ' : '') . $volume->getNumber() . ' en vente au Japon',
                    'type' => 1,];
            }
            if (!is_null($volume->getDateFr()) && $volume->getDateFr()->format('Y-m') == $year . '-' . $month) {
                $result[$volume->getDateFr()->format('Y-m-d')][] = [
                    'link' => '/volumes/' . (('original' !== $volume->getType()) ? $volume->getType() . '/' : '') . $volume->getNumber(),
                    'editlink' => '/admin/volumes/edit/' . $volume->getId(),
                    'title' => 'Tome ' . (('wps' == $volume->getType()) ? 'WPS ' : '') . $volume->getNumber() . ' France',
                    'content' => 'Tome ' . (('wps' == $volume->getType()) ? 'Wild Police Story ' : '') . $volume->getNumber() . ' en vente en France',
                    'type' => 1,];
            }
        }

        $cases = $this->getEntityManager()->getRepository(CaseEntity::class)->findByMonth($year, $month);
        foreach ($cases as $case) {
            $result[$case->getDate()->format('Y-m-d')][] = [
                'link' => $this->appExtension->getCasePath($case),
                'editlink' => '/admin/cases/edit/' . $case->getId(),
                'title' => $case->getDisplayName('long'),
                'content' => $case->getDisplayName(),
                'type' => 0,];
        }

        $chapters = $this->getEntityManager()->getRepository(Chapter::class)->findByMonth($year, $month);
        foreach ($chapters as $chapter) {
            $result[$chapter->getDate()->format('Y-m-d')][] = [
                'link' => ((!is_null($chapter->getVolume())) ? '/volumes/' . (('original' !== $chapter->getVolume()->getType()) ? $chapter->getVolume()->getType() . '/' : '') . $chapter->getVolume()->getNumber() : '/chapters#prepublished'),
                'editlink' => '/admin/chapters/edit/' . $chapter->getId(),
                'title' => 'Chapitre ' . (('wps' == $chapter->getVolume()?->getType()) ? 'WPS ' : '') . $chapter->getDisplayNumber(),
                'content' => 'Chapitre ' . (('wps' == $chapter->getVolume()?->getType()) ? 'Wild Police Story ' : '') . $chapter->getDisplayNumber() . ' — ' . $chapter->getTitleFr(),
                'type' => 1,];
        }

        $albums = $this->getEntityManager()->getRepository(Album::class)->findByMonth($year, $month);
        foreach ($albums as $album) {
            $result[$album->getDate()->format('Y-m-d')][] = [
                'link' => '/music/' . $album->getId(),
                'editlink' => '/admin/albums/edit/' . $album->getId(),
                'title' => 'Album ' . $album->getName(),
                'content' => 'L’album ' . $album->getName() . ' est en vente',
                'type' => 2,];
        }

        $tracks = $this->getEntityManager()->getRepository(Track::class)->findByMonth($year, $month);
        foreach ($tracks as $track) {
            $type = 'inconnu';
            if (Album::ALBUM_OPENING == $track->getAlbum()->getId()) {
                $type = 'opening';
            } elseif (Album::ALBUM_ENDING == $track->getAlbum()->getId()) {
                $type = 'ending';
            } elseif (Album::ALBUM_MOVIE_THEME == $track->getAlbum()->getId()) {
                $type = 'movie';
            }
            if (!is_null($track->getDate()) && $track->getDate()->format('Y-m') == $year . '-' . $month) {
                $result[$track->getDate()->format('Y-m-d')][] = [
                    'link' => '/music#' . $type,
                    'editlink' => '/admin/tracks/edit/' . $track->getId(),
                    'title' => ('movie' == $type) ? 'Vente générique film ' . $track->getNumber() : 'Vente de l’' . $type . ' ' . $track->getNumber(),
                    'content' => (('movie' == $type) ? 'Le générique du film ' . $track->getNumber() : 'L’' . $type . ' ' . $track->getNumber()) . ' est en vente — ' . $track->getTitleJp() . ' — ' . $track->getArtist(),
                    'type' => 2,];
            }
            if (!is_null($track->getDateFirstUse()) && $track->getDateFirstUse()->format('Y-m') == $year . '-' . $month) {
                $result[$track->getDateFirstUse()->format('Y-m-d')][] = [
                    'link' => '/music#' . $type,
                    'editlink' => '/admin/tracks/edit/' . $track->getId(),
                    'title' => (('movie' === $type) ? 'Ending de film' : ucfirst($type)) . ' ' . $track->getNumber(),
                    'content' => 'Nouvel ' . (('movie' === $type) ? 'ending de film' : $type) . ' (' . $track->getNumber() . ') — ' . $track->getTitleJp() . ' — ' . $track->getArtist(),
                    'type' => 2,];
            }
        }

        $dvds = $this->getEntityManager()->getRepository(Dvd::class)->findByMonth($year, $month);
        foreach ($dvds as $dvd) {
            $dvdEpisodes = [];
            if (is_countable($dvd->getEpisodes()) ? count($dvd->getEpisodes()) : 0) {
                foreach ($dvd->getEpisodes() as $episode) {
                    $dvdEpisodes[] = $episode->getDisplayNumber();
                }
            }
            $result[$dvd->getDate()->format('Y-m-d')][] = [
                'link' => '/dvds/' . $dvd->getPart(),
                'editlink' => '/admin/dvds/edit/' . $dvd->getId(),
                'title' => 'DVD Part ' . $dvd->getPart() . ' — Vol ' . $dvd->getNumber(),
                'content' => 'DVD Part ' . $dvd->getPart() . ' — Vol ' . $dvd->getNumber() . ', ' . $dvd->getPrice() . ' 円 (' . ($dvd->isTTC() ? 'TTC' : 'HT') . ')' . (count($dvdEpisodes) ? ' (Épisodes ' . implode(', ', $dvdEpisodes) . ')' : ''),
                'type' => 3,];
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function findByMonth(int $year, string $month)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('e')
            ->from(Event::class, 'e')
            ->where($qb->expr()->eq('EXTRACT(YEAR FROM e.date)', ':year'))
            ->andWhere($qb->expr()->eq('EXTRACT(MONTH FROM e.date)', ':month'))
            ->setParameter('year', $year)
            ->setParameter('month', $month);

        return $qb->getQuery()->getResult();
    }
}

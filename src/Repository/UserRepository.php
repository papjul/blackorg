<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    /**
     * UserRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * Returns all users with the ROLE_VALIDATOR role (or greater).
     */
    public function findValidatorUsers()
    {
        return $this->createQueryBuilder('u')
            ->where('JSONB_HGG(u.roles , \'{}\') LIKE :role_validator')
            ->orWhere('JSONB_HGG(u.roles , \'{}\') LIKE :role_admin')
            ->orWhere('JSONB_HGG(u.roles , \'{}\') LIKE :role_super_admin')
            ->setParameter('role_validator', '%"ROLE_VALIDATOR"%')
            ->setParameter('role_admin', '%"ROLE_ADMIN"%')
            ->setParameter('role_super_admin', '%"ROLE_SUPER_ADMIN"%')
            ->getQuery()
            ->getResult();
    }

    /**
     * Returns all users with the ROLE_SUPER_ADMIN role.
     */
    public function findSuperAdminUsers()
    {
        return $this->createQueryBuilder('u')
            ->where('JSONB_HGG(u.roles , \'{}\') LIKE :role_super_admin')
            ->setParameter('role_super_admin', '%"ROLE_SUPER_ADMIN"%')
            ->getQuery()
            ->getResult();
    }
}

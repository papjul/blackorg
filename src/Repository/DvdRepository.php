<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\Dvd;
use App\Submission\SubmissionFormHandler;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Class DvdRepository.
 */
class DvdRepository extends AbstractSubmittableEntityRepository implements EntityWithEventsRepositoryInterface
{
    /**
     * DvdRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     */
    public function __construct(ManagerRegistry $registry, PropertyAccessorInterface $propertyAccessor, SubmissionFormHandler $submissionFormHandler)
    {
        parent::__construct($registry, $propertyAccessor, $submissionFormHandler, Dvd::class);
    }

    /**
     * @return array|mixed
     */
    public function findAll()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d')
            ->from(Dvd::class, 'd', 'd.id')
            ->orderBy('d.part', 'ASC')
            ->orderBy('d.number', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return mixed
     */
    public function findByMonth(int $year, string $month)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d')
            ->from(Dvd::class, 'd')
            ->where($qb->expr()->eq('EXTRACT(YEAR FROM d.date)', ':year'))
            ->andWhere($qb->expr()->eq('EXTRACT(MONTH FROM d.date)', ':month'))
            ->setParameter('year', $year)
            ->setParameter('month', $month);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $part
     *
     * @return mixed
     */
    public function getDvd($part)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'e')
            ->from(Dvd::class, 'd')
            ->leftJoin('d.episodes', 'e')
            ->where('d.part = ?1')
            ->setParameter(1, $part);

        return $qb->getQuery()->getResult();
    }

    public function getDvds(): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('d', 'e')
            ->from(Dvd::class, 'd')
            ->leftJoin('d.episodes', 'e')
            ->addOrderBy('d.part', 'ASC')
            ->addOrderBy('d.number', 'ASC');

        $dvdsByPart = [];
        foreach ($qb->getQuery()->getResult() as $dvd) {
            $dvdsByPart[$dvd->getPart()][] = $dvd;
        }

        return $dvdsByPart;
    }
}

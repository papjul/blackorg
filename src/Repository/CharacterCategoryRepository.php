<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\CharacterCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class CharacterCategoryRepository.
 */
class CharacterCategoryRepository extends ServiceEntityRepository
{
    /**
     * CharacterCategoryRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CharacterCategory::class);
    }

    public function getCharacterCategories(): iterable
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('cc', 'char')
            ->from(CharacterCategory::class, 'cc')
            ->leftJoin('cc.characters', 'char')
            ->addOrderBy('cc.id', 'ASC');

        return $qb->getQuery()->getResult();
    }
}

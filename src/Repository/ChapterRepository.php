<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\Chapter;
use App\Entity\ChapterPending;
use App\Entity\PendingInterface;
use App\Entity\Volume;
use App\Submission\SubmissionFormHandler;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use function Doctrine\ORM\QueryBuilder;

/**
 * Class ChapterRepository.
 */
class ChapterRepository extends AbstractSubmittableEntityRepository implements EntityWithEventsRepositoryInterface
{
    /**
     * ChapterRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     */
    public function __construct(ManagerRegistry $registry, PropertyAccessorInterface $propertyAccessor, SubmissionFormHandler $submissionFormHandler)
    {
        parent::__construct($registry, $propertyAccessor, $submissionFormHandler, Chapter::class);
    }

    public function getChaptersByDate(): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $chapters = $qb->select('c')
            ->from(Chapter::class, 'c')
            ->leftJoin('c.volume', 'v')
            ->where($qb->expr()->isNotNull('c.date'))
            ->andWhere($qb->expr()->orX($qb->expr()->isNull('v.type'), $qb->expr()->eq('v.type', '\'original\'')))
            ->getQuery()->getResult();

        $chaptersByDate = [];
        foreach ($chapters as $chapter) {
            $year = $chapter->getDate()->format('o');
            $week = intval($chapter->getDate()->format('W'));
            if (!array_key_exists($year, $chaptersByDate)) {
                $chaptersByDate[$year] = [];
            }
            $chaptersByDate[$year][$week] = [
                'number' => $chapter->getDisplayNumber(),
                'class' => 'arc' . $chapter->getCase()?->getArc(),
            ];
        }
        return $chaptersByDate;
    }

    /**
     * Find chapters with missing fields (for contributing tasks).
     *
     * @return array|mixed
     */
    public function findChaptersWithMissingFields()
    {
        $qb = $this->createQueryBuilder('c');

        $qb = $qb->select('c')
            ->leftJoin('c.volume', 'v')
            ->where($qb->expr()->isNull('c.title_jp_romaji'))
            ->orWhere($qb->expr()->eq('c.title_jp_romaji', ':empty'))
            ->orWhere($qb->expr()->isNull('c.title_jp_kanji'))
            ->orWhere($qb->expr()->eq('c.title_jp_kanji', ':empty'))
            ->orderBy('v.type', 'DESC')
            ->addOrderBy('c.number', 'ASC')
            ->setParameter('empty', '');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return mixed
     */
    public function findByMonth(int $year, string $month)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')
            ->from(Chapter::class, 'c')
            ->where($qb->expr()->eq('EXTRACT(YEAR FROM c.date)', ':year'))
            ->andWhere($qb->expr()->eq('EXTRACT(MONTH FROM c.date)', ':month'))
            ->setParameter('year', $year)
            ->setParameter('month', $month);

        return $qb->getQuery()->getResult();
    }

    public function getFirstChapter(): Chapter
    {
        return $this->findOneBy([], ['number' => 'ASC']);
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getMaxChapter(string $type = 'original'): int
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('MAX(c.number)')
            ->leftJoin('c.volume', 'v')
            ->where('v.type = :type');
        if ('original' === $type) {
            $qb->orWhere('v IS NULL');
        }
        $qb->setParameter('type', $type);

        return intval($qb->getQuery()->getSingleScalarResult());
    }

    public function getPrepublishedChapters(): array
    {
        return $this->findBy(['volume' => null], ['number' => 'ASC']);
    }

    /**
     * {@inheritdoc}
     *
     * @throws NonUniqueResultException
     */
    public function getDefaultEntity($parameter = null): ?PendingInterface
    {
        $chapter = new ChapterPending();
        if (null === $parameter) {
            return $chapter;
        }
        $latestChapter = $this->getLatestChapter(strval($parameter));
        if (null !== $latestChapter) {
            $chapter->setNumber($latestChapter->getNumber() + 1);
            $chapter->setVolume($latestChapter->getVolume());
            if ('original' === $parameter) {
                $chapter->setDate($latestChapter->getDate()->modify('+7 days'));
            }
            if (null !== $latestChapter->getVolume() && $latestChapter->getVolumeNumber() < VolumeRepository::VOLUME_NUMBER_MAX) {
                $chapter->setVolumeNumber($latestChapter->getVolumeNumber() + 1);
            }
        }

        return $chapter;
    }

    /**
     *
     * @throws NonUniqueResultException
     */
    public function getLatestChapter(string $type = 'original'): ?Chapter
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('c')
            ->leftJoin('c.volume', 'v')
            ->where('v.type = :type');
        if ('original' === $type) {
            $qb->orWhere('v IS NULL');
        }
        $qb->setParameter('type', $type)
            ->addOrderBy('c.number', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Changelog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Changelog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Changelog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Changelog[]    findAll()
 * @method Changelog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChangelogRepository extends ServiceEntityRepository
{
    /**
     * ChangelogRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Changelog::class);
    }

    public function findAllGroupedByDate(): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')
            ->from(Changelog::class, 'c', 'c.id')
            ->addOrderBy('c.date', 'DESC');

        $results = $qb->getQuery()->getResult();

        $resultsGrouped = [];
        foreach ($results as $change) {
            $resultsGrouped[$change->getDate()->format('Y-m-d')][] = $change;
        }

        return $resultsGrouped;
    }
}

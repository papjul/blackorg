<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\CaseEntity;
use App\Entity\Episode;
use App\Entity\EpisodePending;
use App\Entity\PendingInterface;
use App\Submission\SubmissionFormHandler;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\TransactionRequiredException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Class EpisodeRepository.
 */
class EpisodeRepository extends AbstractSubmittableEntityRepository implements EntityWithEventsRepositoryInterface
{
    final public const EPISODE_PART_MIN = 1;
    final public const EPISODE_PART_MAX = 10;

    /**
     * EpisodeRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     */
    public function __construct(ManagerRegistry $registry, PropertyAccessorInterface $propertyAccessor, private readonly SubmissionFormHandler $submissionFormHandler)
    {
        parent::__construct($registry, $propertyAccessor, $submissionFormHandler, Episode::class);
    }

    /**
     * @return mixed
     */
    public function findByMonth(int $year, string $month)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('e')
            ->from(Episode::class, 'e')
            ->where($qb->expr()->andX($qb->expr()->eq('EXTRACT(YEAR FROM e.date)', ':year'), $qb->expr()->eq('EXTRACT(MONTH FROM e.date)', ':month')))
            ->orWhere($qb->expr()->andX($qb->expr()->eq('EXTRACT(YEAR FROM e.date_remastered)', ':year'), $qb->expr()->eq('EXTRACT(MONTH FROM e.date_remastered)', ':month')))
            ->setParameter('year', $year)
            ->setParameter('month', $month);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getEpisode($id): Episode
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('e', 'c', 'ch', 'v', 'op', 'ed', 'd')
            ->from(Episode::class, 'e', 'e.id')
            ->leftJoin('e.cases', 'c')
            ->leftJoin('c.chapters', 'ch')
            ->leftJoin('ch.volume', 'v')
            ->leftJoin('e.opening', 'op')
            ->leftJoin('e.ending', 'ed')
            ->leftJoin('e.dvd', 'd')
            ->where('e.id = ?1')
            ->setParameter(1, $id);

        return $qb->getQuery()->getSingleResult();
    }

    public function getEpisodeParts(): array
    {
        $episodeParts = [];
        for ($i = self::EPISODE_PART_MIN; $i <= self::EPISODE_PART_MAX; ++$i) {
            $episodeParts[$i] = 'Partie ' . $i;
        }

        return $episodeParts;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getEpisodeSeasons(): array
    {
        $episodeSeasons = [];
        $seasonMax = self::getMaxSeason() + 1;
        for ($i = 1; $i <= $seasonMax; ++$i) {
            $episodeSeasons[$i] = $i;
        }

        return $episodeSeasons;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getMaxSeason(): int
    {
        $qb = $this->createQueryBuilder('e')->select('MAX(e.season)');

        return intval($qb->getQuery()->getSingleScalarResult());
    }

    /**
     *
     * @return mixed
     * @throws Exception
     */
    public function getEpisodes(?int $page = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('e', 'c', 'ch', 'v')
            ->from(Episode::class, 'e', 'e.id')
            ->leftJoin('e.cases', 'c')
            ->leftJoin('c.chapters', 'ch')
            ->leftJoin('ch.volume', 'v')
            ->addOrderBy('e.season', 'ASC')
            ->addOrderBy('e.number', 'ASC');

        if (null === $page) {
            return $qb->getQuery()->getResult();
        } else {
            return $this->createPaginator($qb, $page);
        }
    }

    /**
     *
     * @throws Exception
     */
    private function createPaginator(QueryBuilder $queryBuilder, int $currentPage, int $pageSize = Episode::NUM_ITEMS): array
    {
        $currentPage = $currentPage < 1 ? 1 : $currentPage;
        $firstResult = ($currentPage - 1) * $pageSize;

        $query = $queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($pageSize)
            ->getQuery();

        $paginator = new Paginator($query);
        $numResults = $paginator->count();
        $hasPreviousPage = $currentPage > 1;
        $hasNextPage = ($currentPage * $pageSize) < $numResults;

        return [
            'results' => $paginator->getIterator(),
            'currentPage' => $currentPage,
            'hasPreviousPage' => $hasPreviousPage,
            'hasNextPage' => $hasNextPage,
            'previousPage' => $hasPreviousPage ? $currentPage - 1 : null,
            'nextPage' => $hasNextPage ? $currentPage + 1 : null,
            'numPages' => (int)ceil($numResults / $pageSize),
            'haveToPaginate' => $numResults > $pageSize,
        ];
    }

    /**
     * @throws Exception
     */
    public function getEpisodesForStats()
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('e', 'c')
            ->from(Episode::class, 'e')
            ->leftJoin('e.cases', 'c')
            ->getQuery()->getResult();
    }

    public function getLatestEpisodes(int $quantity = 15): Paginator
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('e', 'c', 'ch', 'v')
            ->from(Episode::class, 'e', 'e.id')
            ->leftJoin('e.cases', 'c')
            ->leftJoin('c.chapters', 'ch')
            ->leftJoin('ch.volume', 'v')
            ->orderBy('e.number', 'DESC')
            ->setMaxResults($quantity);

        return new Paginator($qb->getQuery(), $fetchJoinCollection = true);
    }

    /**
     * @return mixed
     */
    public function getSeasonList(int $season)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('e', 'c', 'ch', 'v')
            ->from(Episode::class, 'e', 'e.id')
            ->leftJoin('e.cases', 'c')
            ->leftJoin('c.chapters', 'ch')
            ->leftJoin('ch.volume', 'v')
            ->where('e.season = ?1')
            ->orderBy('e.number', 'ASC')
            ->setParameter(1, $season);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $caseId
     *
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    public function hasCase(Episode $episode, $caseId): bool
    {
        $case = $this->getEntityManager()->find(CaseEntity::class, $caseId);

        return $episode->getCases()->contains($case);
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function getDefaultEntity($parameter = null): ?PendingInterface
    {
        $latestEpisode = $this->getLatestEpisode();
        $episode = new EpisodePending();
        $episode->setNumber($latestEpisode->getNumber() + 1);
        $episode->setSeason($latestEpisode->getSeason());
        $episode->setDate($latestEpisode->getDate()->modify('+7 days'));
        $episode->setOpening($latestEpisode->getOpening());
        $episode->setEnding($latestEpisode->getEnding());
        if (1 == $latestEpisode->getPart()) { // Episodes are generally in 2-parts so if latest episode was part 1, we fill in more fields for the (expected) 2nd part
            $episode->setPart(2);
            $episode->setTitleFr($latestEpisode->getTitleFr());
            $episode->setTitleJpRomaji($latestEpisode->getTitleJpRomaji());
            $episode->setTitleJpKanji($latestEpisode->getTitleJpKanji());
        }

        return $episode;
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getLatestEpisode(): Episode
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('e')
            ->from(Episode::class, 'e', 'e.id')
            ->addOrderBy('e.number', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getSingleResult();
    }
}

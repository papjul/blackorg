<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\Album;
use App\Submission\SubmissionFormHandler;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Class AlbumRepository.
 */
class AlbumRepository extends AbstractSubmittableEntityRepository implements EntityWithEventsRepositoryInterface
{
    protected array $albumTypes = [
        'opening' => 'Génériques d’ouverture (dits “openings”)',
        'ending' => 'Génériques de clôture (dits “endings”)',
        'movie' => 'Génériques de clôture de films',
        'tv_ost' => 'Bande originale de la série',
        'movie_ost' => 'Bande originale des films',
        'other' => 'Autres albums',
    ];

    /**
     * AlbumRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     */
    public function __construct(ManagerRegistry $registry, PropertyAccessorInterface $propertyAccessor, SubmissionFormHandler $submissionFormHandler)
    {
        parent::__construct($registry, $propertyAccessor, $submissionFormHandler, Album::class);
    }

    public function getAlbumTypes(): array
    {
        return $this->albumTypes;
    }

    /**
     * @return mixed
     */
    public function getAlbums(string $type)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('a')
            ->from(Album::class, 'a')
            ->where('a.type = ?1')
            ->setParameter(1, $type);

        return $qb->getQuery()->getResult();
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getAlbum(int $id): Album
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('a', 't')
            ->from(Album::class, 'a')
            ->leftJoin('a.tracks', 't')
            ->where('a.id = ?1')
            ->orderBy('t.number', 'ASC')
            ->setParameter(1, $id);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getOpeningAlbum(): Album
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('a', 't', 'eo')
            ->from(Album::class, 'a')
            ->leftJoin('a.tracks', 't')
            ->leftJoin('t.episodesOp', 'eo')
            ->where('a.type = ?1')
            ->orderBy('t.number', 'ASC')
            ->setParameter(1, 'opening');

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getEndingAlbum(): Album
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('a', 't', 'ee')
            ->from(Album::class, 'a')
            ->leftJoin('a.tracks', 't')
            ->leftJoin('t.episodesEd', 'ee')
            ->where('a.type = ?1')
            ->orderBy('t.number', 'ASC')
            ->setParameter(1, 'ending');

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getMovieAlbum(): Album
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('a', 't')
            ->from(Album::class, 'a')
            ->leftJoin('a.tracks', 't')
            ->where('a.type = ?1')
            ->orderBy('t.number', 'ASC')
            ->setParameter(1, 'movie');

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @return mixed
     */
    public function findByMonth(int $year, string $month)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('a')
            ->from(Album::class, 'a')
            ->where($qb->expr()->eq('EXTRACT(YEAR FROM a.date)', ':year'))
            ->andWhere($qb->expr()->eq('EXTRACT(MONTH FROM a.date)', ':month'))
            ->setParameter('year', $year)
            ->setParameter('month', $month);

        return $qb->getQuery()->getResult();
    }
}

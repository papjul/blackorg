<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\CaseEntity;
use App\Entity\PendingInterface;
use App\Entity\Situation;
use App\Entity\SituationPending;
use App\Entity\SituationType;
use App\Submission\SubmissionFormHandler;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Class SituationRepository.
 */
class SituationRepository extends AbstractSubmittableEntityRepository
{
    /**
     * SituationRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     */
    public function __construct(ManagerRegistry $registry, PropertyAccessorInterface $propertyAccessor, SubmissionFormHandler $submissionFormHandler)
    {
        parent::__construct($registry, $propertyAccessor, $submissionFormHandler, Situation::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    public function hasType(Situation $situation, int $situationTypeId): bool
    {
        $situationType = $this->getEntityManager()->find(SituationType::class, $situationTypeId);

        return $situation->getTypes()->contains($situationType);
    }

    /**
     * {@inheritdoc}
     *
     */
    public function getDefaultEntity($parameter = null): ?PendingInterface
    {
        $situationPending = new SituationPending();
        $caseId = intval($parameter);
        if (null === $caseId || 0 === $caseId) {
            return $situationPending;
        } else {
            $case = $this->getEntityManager()->getRepository(CaseEntity::class)->find($caseId);
            if (null === $case) {
                throw new NotFoundHttpException();
            }
        }
        $situationPending->setCase($case);
        $situationPending->setNumber((is_countable($case->getSituations()) ? count($case->getSituations()) : 0) + 1);

        return $situationPending;
    }
}

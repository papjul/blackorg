<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\Album;
use App\Entity\PendingInterface;
use App\Entity\Track;
use App\Entity\TrackPending;
use App\Submission\SubmissionFormHandler;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Class TrackRepository.
 */
class TrackRepository extends AbstractSubmittableEntityRepository implements EntityWithEventsRepositoryInterface
{
    /**
     * TrackRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     */
    public function __construct(ManagerRegistry $registry, PropertyAccessorInterface $propertyAccessor, SubmissionFormHandler $submissionFormHandler)
    {
        parent::__construct($registry, $propertyAccessor, $submissionFormHandler, Track::class);
    }

    /**
     * @return mixed
     */
    public function findByMonth(int $year, string $month)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('t')
            ->from(Track::class, 't')
            ->where($qb->expr()->andX($qb->expr()->eq('EXTRACT(YEAR FROM t.date)', ':year'), $qb->expr()->eq('EXTRACT(MONTH FROM t.date)', ':month')))
            ->orWhere($qb->expr()->andX($qb->expr()->eq('EXTRACT(YEAR FROM t.date_first_use)', ':year'), $qb->expr()->eq('EXTRACT(MONTH FROM t.date_first_use)', ':month')))
            ->setParameter('year', $year)
            ->setParameter('month', $month);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return mixed
     */
    public function getOpenings()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('t')
            ->from(Track::class, 't', 't.id')
            ->leftJoin('t.album', 'a')
            ->where('a.type = ?1')
            ->orderBy('t.number', 'ASC')
            ->setParameter(1, 'opening');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return mixed
     */
    public function getEndings()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('t')
            ->from(Track::class, 't', 't.id')
            ->leftJoin('t.album', 'a')
            ->where('a.type = ?1')
            ->orderBy('t.number', 'ASC')
            ->setParameter(1, 'ending');

        return $qb->getQuery()->getResult();
    }

    public function findMovieEnding(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'album' => 3]);
    }

    /**
     * {@inheritdoc}
     *
     * @throws NonUniqueResultException
     */
    public function getDefaultEntity($parameter = null): ?PendingInterface
    {
        $trackPending = new TrackPending();
        $albumId = intval($parameter);
        if (null === $albumId || 0 === $albumId) {
            return $trackPending;
        } else {
            $album = $this->getEntityManager()->getRepository(Album::class)->find($albumId);
            if (null === $album) {
                throw new NotFoundHttpException();
            }
        }
        $latestTrack = $this->getLatestTrack($album);
        $trackPending->setAlbum($album);
        if (null !== $latestTrack) {
            $trackPending->setNumber($latestTrack->getNumber() + 1);
        }

        return $trackPending;
    }

    /**
     *
     * @throws NonUniqueResultException
     */
    public function getLatestTrack(Album $album): ?Track
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('t')
            ->from(Track::class, 't', 't.id')
            ->where('t.album = :album')
            ->setParameter('album', $album)
            ->addOrderBy('t.number', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

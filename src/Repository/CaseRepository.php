<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\CaseAbstract;
use App\Entity\CaseEntity;
use App\Entity\CasePending;
use App\Entity\Character;
use App\Entity\Gadget;
use App\Entity\PendingInterface;
use App\Entity\Situation;
use App\Entity\SubmittableEntityInterface;
use App\Form\CaseFormType;
use App\Submission\SubmissionFormHandler;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Class CaseRepository.
 */
class CaseRepository extends AbstractSubmittableEntityRepository implements EntityWithEventsRepositoryInterface
{
    protected array $types = [
        'manga' => 'Manga',
        'anime' => 'Anime (HS)',
        'movie' => 'Film',
        'movie_special' => 'Film spécial',
        'ova' => 'OVA',
        'magic_file' => 'Magic File',
        'dc_special' => 'Spécial',
        'mk_special' => 'Magic Kaito',
        'tanpenshu' => 'Recueil d’histoires courtes de Gōshō Aoyama',
        'drama_special' => 'Drama live',
        'drama_episode' => 'Drama épisode',
        'wps' => 'Wild Police Story partie'
    ];

    protected array $arcs = [
        1 => 'Conan',
        2 => 'Haibara',
        3 => 'Vermouth',
        4 => 'Nanatsu no Ko',
        5 => 'Kir',
        6 => 'Bourbon',
        7 => 'Rum',
    ];

    /**
     * CaseRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     */
    public function __construct(ManagerRegistry $registry, PropertyAccessorInterface $propertyAccessor, private readonly SubmissionFormHandler $submissionFormHandler)
    {
        parent::__construct($registry, $propertyAccessor, $submissionFormHandler, CaseEntity::class);
    }

    public function getFormClass(): string
    {
        return CaseFormType::class;
    }

    public function getRouteSectionName(): string
    {
        return 'cases';
    }

    /**
     * Create the entity in the database.
     *
     * @param PendingInterface $pending
     * @return SubmittableEntityInterface
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function createEntity(PendingInterface $pending): SubmittableEntityInterface
    {
        // We first check if the entity is the same as the repository we are in
        if (!is_a($pending, $this->getPendingEntityName())) {
            throw new UnexpectedTypeException($pending, 'PendingInterface');
        }

        $newEntity = $this->insertDataFromEntity($pending, new CaseEntity());

        // Automatically create a situation linked to this entity
        $newEntitySituation = new Situation();
        $newEntitySituation->setCase($newEntity);
        $newEntitySituation->setNumber(1);

        $this->getEntityManager()->persist($newEntity);
        $this->getEntityManager()->persist($newEntitySituation);
        $this->getEntityManager()->flush();

        return $newEntity;
    }

    /**
     * {@inheritdoc}
     */
    public function getPendingEntityName(): string
    {
        return CasePending::class;
    }

    public function getArcs(): array
    {
        return $this->arcs;
    }

    public function getCaseTypes(): array
    {
        return $this->types;
    }

    public function getAbstractEntityName(): string
    {
        return CaseAbstract::class;
    }

    /**
     * @return mixed
     */
    public function findByMonth(int $year, string $month)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')
            ->from(CaseEntity::class, 'c')
            ->where($qb->expr()->in('c.type', ['movie', 'movie_special', 'ova', 'magic_file', 'dc_special', 'mk_special', 'mk1412', 'tanpenshu', 'drama_special', 'drama_episode']))
            ->andWhere($qb->expr()->eq('EXTRACT(YEAR FROM c.date)', ':year'))
            ->andWhere($qb->expr()->eq('EXTRACT(MONTH FROM c.date)', ':month'))
            ->setParameter('year', $year)
            ->setParameter('month', $month);

        return $qb->getQuery()->getResult();
    }

    /**
     * Find cases with missing fields (for contributing tasks).
     *
     * @return array|mixed
     */
    public function findCasesWithMissingFields()
    {
        $qb = $this->createQueryBuilder('c');

        $qb = $qb->select('COUNT(ch) AS HIDDEN nbCharacters', 'COUNT(g) AS HIDDEN nbGadgets', 'c')
            ->leftJoin('c.characters', 'ch')
            ->leftJoin('c.gadgets', 'g')
            ->groupBy('c')
            ->having($qb->expr()->isNull('c.summary'))
            ->orHaving($qb->expr()->eq('c.summary', ':empty'))
            ->orHaving($qb->expr()->eq('COUNT(ch)', 0))
            //->orHaving($qb->expr()->eq('nbGadgets', 0))
            ->andHaving($qb->expr()->neq('c.type', ':tanpenshu'))
            ->orderBy('c.type', 'ASC')
            ->addOrderBy('c.number', 'ASC')
            ->setParameter('empty', '')
            ->setParameter('tanpenshu', 'tanpenshu');

        return $qb->getQuery()->getResult();
    }

    /**
     * Returns all cases containing plot elements.
     *
     * @return array|mixed
     */
    public function findAllWithPlot()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c, e, ch, v, char, g')
            ->from(CaseEntity::class, 'c', 'c.id')
            ->leftJoin('c.episodes', 'e')
            ->leftJoin('c.chapters', 'ch')
            ->leftJoin('ch.volume', 'v')
            ->leftJoin('c.first_appearances', 'char')
            ->leftJoin('c.first_uses', 'g')
            ->where($qb->expr()->andX($qb->expr()->isNotNull('c.plot_essential'), $qb->expr()->neq('c.plot_essential', ':empty')))
            ->orWhere($qb->expr()->andX($qb->expr()->isNotNull('c.plot_recommended'), $qb->expr()->neq('c.plot_recommended', ':empty')))
            ->orWhere($qb->expr()->andX($qb->expr()->isNotNull('c.plot_minor'), $qb->expr()->neq('c.plot_minor', ':empty')))
            ->orderBy('c.arc', 'ASC')
            ->addOrderBy('c.date', 'ASC')
            ->setParameter('empty', '');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array|mixed
     */
    public function findAll()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')
            ->from(CaseEntity::class, 'c', 'c.id')
            ->orderBy('c.arc', 'ASC')
            ->orderBy('c.date', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findManga(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'manga']);
    }

    public function findAnime(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'anime']);
    }

    public function findMovie(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'movie']);
    }

    public function findSpecialMovie(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'movie_special']);
    }

    public function findOva(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'ova']);
    }

    public function findMagicFile(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'magic_file']);
    }

    public function findDcSpecial(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'dc_special']);
    }

    public function findMkSpecial(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'mk_special']);
    }

    public function findWps(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'wps']);
    }

    public function findMk1412(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'mk1412']);
    }

    public function findTanpenshu(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'tanpenshu']);
    }

    public function findDramaSpecial(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'drama_special']);
    }

    public function findDramaEpisode(int $number): ?object
    {
        return $this->findOneBy(['number' => $number, 'type' => 'drama_episode']);
    }

    /**
     * @return mixed
     */
    public function getArcCases(int $arc)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c', 'e', 'ch', 'v', 'fa', 'fu')
            ->from(CaseEntity::class, 'c')
            ->leftJoin('c.episodes', 'e')
            ->leftJoin('c.chapters', 'ch')
            ->leftJoin('ch.volume', 'v')
            ->leftJoin('c.first_appearances', 'fa')
            ->leftJoin('c.first_uses', 'fu')
            ->where('c.arc = ?1')
            ->orderBy('c.date', 'ASC')
            ->setParameter(1, $arc);

        return $qb->getQuery()->getResult();
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getCase(int $id): CaseEntity
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c', 'e', 'ch', 'v', 'fa', 'fu', 's', 'char', 'g')
            ->from(CaseEntity::class, 'c')
            ->leftJoin('c.episodes', 'e')
            ->leftJoin('c.chapters', 'ch')
            ->leftJoin('ch.volume', 'v')
            ->leftJoin('c.first_appearances', 'fa')
            ->leftJoin('c.first_uses', 'fu')
            ->leftJoin('c.situations', 's')
            ->leftJoin('c.characters', 'char')
            ->leftJoin('c.gadgets', 'g')
            ->where('c.id = ?1')
            ->setParameter(1, $id);

        return $qb->getQuery()->getSingleResult();
    }

    public function getCases(): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c', 'e', 'ch', 'v', 'fa', 'fu')
            ->from(CaseEntity::class, 'c')
            ->leftJoin('c.episodes', 'e')
            ->leftJoin('c.chapters', 'ch')
            ->leftJoin('ch.volume', 'v')
            ->leftJoin('c.first_appearances', 'fa')
            ->leftJoin('c.first_uses', 'fu')
            ->orderBy('c.arc', 'ASC')
            ->addOrderBy('c.date', 'ASC');

        $casesByArc = [];
        foreach ($qb->getQuery()->getResult() as $case) {
            $casesByArc[$case->getArc()][] = $case;
        }

        return $casesByArc;
    }

    /**
     * @param $characterId
     *
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     * @throws ORMException
     */
    public function hasCharacter(CaseEntity $case, $characterId): bool
    {
        $character = $this->getEntityManager()->find(Character::class, $characterId);

        return $case->getCharacters()->contains($character);
    }

    /**
     * @param $gadgetId
     *
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     * @throws ORMException
     */
    public function hasGadget(CaseEntity $case, $gadgetId): bool
    {
        $gadget = $this->getEntityManager()->find(Gadget::class, $gadgetId);

        return $case->getGadgets()->contains($gadget);
    }

    /**
     * @return mixed
     */
    public function manualSearch(array $criterias)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')->from(CaseEntity::class, 'c');

        // Traitement de l‘intrigue
        if (isset($criterias['plot']) && is_array($criterias['plot']) && count($criterias['plot'])) {
            $aExpr = [];
            if (in_array('chardev', $criterias['plot'])) {
                $aExpr[] = $qb->expr()->andX($qb->expr()->isNotNull('c.plot_chardev'), $qb->expr()->neq('c.plot_chardev', ':empty'));
            }
            if (in_array('romance', $criterias['plot'])) {
                $aExpr[] = $qb->expr()->andX($qb->expr()->isNotNull('c.plot_romance'), $qb->expr()->neq('c.plot_romance', ':empty'));
            }
            if (in_array('org', $criterias['plot'])) {
                $aExpr[] = $qb->expr()->andX($qb->expr()->isNotNull('c.plot_org'), $qb->expr()->neq('c.plot_org', ':empty'));
            }
            if (count($aExpr) > 1) {
                $orExpr = $qb->expr()->orX();
                foreach ($aExpr as $expr) {
                    $orExpr->add($expr);
                }
                $qb->andWhere($orExpr);
            } else {
                $qb->andWhere($aExpr[0]);
            }
            $qb->setParameter('empty', '');
        }

        // Traitement des types
        if (isset($criterias['type']) && is_array($criterias['type']) && count($criterias['type'])) {
            $aExpr = [];
            foreach ($criterias['type'] as $type) {
                $aExpr[] = $qb->expr()->eq('c.type', ':type' . $type);
            }

            if (count($aExpr) > 1) {
                $orExpr = $qb->expr()->orX();
                foreach ($aExpr as $expr) {
                    $orExpr->add($expr);
                }
                $qb->andWhere($orExpr);
            } else {
                $qb->andWhere($aExpr[0]);
            }

            foreach ($criterias['type'] as $type) {
                $qb->setParameter('type' . $type, $type);
            }
        }

        // Traitement des arcs
        if (isset($criterias['arc']) && is_array($criterias['arc']) && count($criterias['arc'])) {
            $aExpr = [];
            foreach ($criterias['arc'] as $arc) {
                $aExpr[] = $qb->expr()->eq('c.arc', ':arc' . $arc);
            }

            if (count($aExpr) > 1) {
                $orExpr = $qb->expr()->orX();
                foreach ($aExpr as $expr) {
                    $orExpr->add($expr);
                }
                $qb->andWhere($orExpr);
            } else {
                $qb->andWhere($aExpr[0]);
            }

            foreach ($criterias['arc'] as $arc) {
                $qb->setParameter('arc' . $arc, $arc);
            }
        }

        // Traitement des personnages
        if (isset($criterias['characters']) && is_array($criterias['characters']) && count($criterias['characters'])) {
            $aExpr = [];
            foreach ($criterias['characters'] as $char) {
                $aExpr[] = $qb->expr()->eq('ch.id', ':ch' . $char->getId());
            }

            if (count($aExpr) > 1) {
                $orExpr = $qb->expr()->orX();
                foreach ($aExpr as $expr) {
                    $orExpr->add($expr);
                }
                $qb->join('c.characters', 'ch', 'WITH', $orExpr);
            } else {
                $qb->join('c.characters', 'ch', 'WITH', $aExpr[0]);
            }

            foreach ($criterias['characters'] as $char) {
                $qb->setParameter('ch' . $char->getId(), $char->getId());
            }
        }

        // Traitement des gadgets
        if (isset($criterias['gadgets']) && is_array($criterias['gadgets']) && count($criterias['gadgets'])) {
            $aExpr = [];
            foreach ($criterias['gadgets'] as $gadget) {
                $aExpr[] = $qb->expr()->eq('g.id', ':g' . $gadget->getId());
            }

            if (count($aExpr) > 1) {
                $orExpr = $qb->expr()->orX();
                foreach ($aExpr as $expr) {
                    $orExpr->add($expr);
                }
                $qb->join('c.gadgets', 'g', 'WITH', $orExpr);
            } else {
                $qb->join('c.gadgets', 'g', 'WITH', $aExpr[0]);
            }

            foreach ($criterias['gadgets'] as $gadget) {
                $qb->setParameter('g' . $gadget->getId(), $gadget->getId());
            }
        }

        $qb->orderBy('c.arc', 'ASC')->orderBy('c.date', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     *
     * @throws NonUniqueResultException
     */
    public function getDefaultEntity($parameter = null): ?PendingInterface
    {
        $case = new CasePending();
        if (null === $parameter) {
            return $case;
        }
        $case->setType($parameter);
        $latestCase = $this->getLatestCase(strval($parameter));
        if (null !== $latestCase) {
            $case->setNumber($latestCase->getNumber() + 1);
            $case->setArc($latestCase->getArc());
        }

        return $case;
    }

    /**
     *
     * @throws NonUniqueResultException
     */
    public function getLatestCase(string $type = 'manga'): ?CaseEntity
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')
            ->from(CaseEntity::class, 'c', 'c.id')
            ->where('c.type = :type')
            ->setParameter('type', $type)
            ->addOrderBy('c.number', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

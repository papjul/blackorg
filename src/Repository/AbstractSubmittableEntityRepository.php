<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PendingInterface;
use App\Entity\Submission;
use App\Entity\SubmittableEntityInterface;
use App\Submission\SubmissionFormHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Class AbstractSubmittableEntityRepository
 * Needs to be extended by all repositories of entities that can be submitted.
 * TODO: Duplicated portions of code.
 */
abstract class AbstractSubmittableEntityRepository extends ServiceEntityRepository
{
    /**
     * AbstractSubmittableEntityRepository constructor.
     * @param ManagerRegistry $registry
     * @param PropertyAccessorInterface $propertyAccessor
     * @param SubmissionFormHandler $submissionFormHandler
     * @param string $entityClass
     */
    public function __construct(ManagerRegistry $registry, private readonly PropertyAccessorInterface $propertyAccessor, private readonly SubmissionFormHandler $submissionFormHandler, string $entityClass)
    {
        parent::__construct($registry, $entityClass);
    }

    /**
     * Use to find the associated form of the entity of the repository.
     *
     * @throws ReflectionException
     */
    public function getFormClass(): string
    {
        return '\App\Form\\' . $this->getEntityNameWithoutNamespace() . 'FormType';
    }

    /**
     * @throws ReflectionException
     */
    public function getEntityNameWithoutNamespace(): string
    {
        return (new ReflectionClass($this->getEntityName()))->getShortName();
    }

    /**
     * Use to find the associated name for the route.
     *
     * @throws ReflectionException
     */
    public function getRouteSectionName(): string
    {
        return strtolower($this->getEntityNameWithoutNamespace()) . 's';
    }

    /**
     * @return mixed
     */
    public function findSimilarPendings(?int $currentId = null, ?int $submissionIdToExclude = null, bool $validated = false, bool $deleted = false)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('s')
            ->from($this->getPendingEntityName(), 'p')
            ->join(Submission::class, 's', Join::WITH, 'p.submission = s.id')
            ->where('p.current = :current_id')
            ->andWhere('s.validated = :validated')
            ->andWhere('s.deleted = :deleted')
            ->setParameter('current_id', $currentId)
            ->setParameter('validated', $validated)
            ->setParameter('deleted', $deleted);

        if (null !== $submissionIdToExclude) {
            $qb->andWhere('s.id != :submissionIdToExclude')
                ->setParameter('submissionIdToExclude', $submissionIdToExclude);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return string
     */
    public function getPendingEntityName(): string
    {
        return $this->getEntityName() . 'Pending';
    }

    /**
     * Create the entity in the database.
     *
     * @return SubmittableEntityInterface
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function createEntity(PendingInterface $pending): SubmittableEntityInterface
    {
        // We first check if the entity is the same as the repository we are in
        if (!is_a($pending, $this->getPendingEntityName())) {
            throw new UnexpectedTypeException($pending, 'PendingInterface');
        }

        $entityName = $this->getEntityName();
        $newEntity = $this->insertDataFromEntity($pending, new $entityName());

        $this->getEntityManager()->persist($newEntity);
        $this->getEntityManager()->flush();

        return $newEntity;
    }

    /**
     * Put data from an existing submittable or pending entity into
     * an existing submittable or pending entity.
     *
     * @param object $fromEntity Entity from which we fetch data
     * @param object $toEntity Entity into which we will put data
     * @return object
     * @return object
     * @return object
     * @throws Exception
     * @throws Exception
     * @throws Exception
     */
    public function insertDataFromEntity(object $fromEntity, object $toEntity): object
    {
        // We first check if the entity is the same as the repository we are in
        if (!is_a($fromEntity, $this->getEntityName()) && !is_a($fromEntity, $this->getPendingEntityName())) {
            throw new UnexpectedTypeException($fromEntity, 'SubmittableEntityInterface|PendingInterface');
        }
        if (!is_a($toEntity, $this->getEntityName()) && !is_a($toEntity, $this->getPendingEntityName())) {
            throw new UnexpectedTypeException($toEntity, 'SubmittableEntityInterface|PendingInterface');
        }
        // TODO: Check that Pending entity is related to the correct submittableEntity
        // Can we simplify this with annotations on Entity class?

        $metadata = $this->getEntityManager()->getClassMetadata($this->getEntityName());
        $fields = $metadata->getFieldNames();
        $associationMappings = $metadata->getAssociationMappings();
        $metadataPending = $this->getEntityManager()->getClassMetadata($this->getPendingEntityName());
        $associationMappingsPending = $metadataPending->getAssociationMappings();

        foreach ($fields as $field) {
            if ('id' === $field || 'order' === $field) {
                continue;
            }

            // Set value of field in Pending entity with value from Entity
            $this->propertyAccessor->setValue($toEntity, $field, $this->propertyAccessor->getValue($fromEntity, $field));
        }

        foreach ($associationMappings as $field => $mapping) {
            if (array_key_exists($field, $associationMappingsPending)) {
                if (ClassMetadata::ONE_TO_MANY == $mapping['type']) {
                    continue;
                } elseif (ClassMetadata::MANY_TO_MANY == $mapping['type']) {
                    $this->propertyAccessor->setValue($toEntity, $field, $this->propertyAccessor->getValue($fromEntity, $field));
                } elseif (ClassMetadata::MANY_TO_ONE == $mapping['type']) {
                    $this->propertyAccessor->setValue($toEntity, $field, $this->propertyAccessor->getValue($fromEntity, $field));
                } else {
                    throw new Exception('Unexpected field type');
                }
            }
        }

        return $toEntity;
    }

    /**
     * Update the entity in the database.
     *
     * @return SubmittableEntityInterface
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function updateEntity(PendingInterface $pending): SubmittableEntityInterface
    {
        // We first check if the entity is the same as the repository we are in
        if (!is_a($pending, $this->getPendingEntityName())) {
            throw new UnexpectedTypeException($pending, 'PendingInterface');
        }

        $this->insertDataFromEntity($pending, $pending->getCurrent());
        $this->getEntityManager()->flush();

        return $pending->getCurrent();
    }

    /**
     * Returns an array containing only the changed fields between two entities.
     *
     * @return array
     * @throws ReflectionException
     * @throws Exception
     */
    public function getChangedFields(FormInterface $form, PendingInterface $newEntity): array
    {
        if (!is_a($newEntity, $this->getPendingEntityName())) {
            throw new UnexpectedTypeException($newEntity, 'PendingInterface');
        }
        $oldEntity = $newEntity->getCurrent();

        $changedFields = [];

        $reflectionMethod = new ReflectionMethod($newEntity::class, 'getCurrent');
        $metadata = $this->getEntityManager()->getClassMetadata($reflectionMethod->getReturnType()->getName());
        $fields = $metadata->getFieldNames();
        $associationMappings = $metadata->getAssociationMappings();

        foreach ($fields as $field) {
            if ('id' === $field) {
                continue;
            }

            if ($form->has($field) && (null === $oldEntity || $this->propertyAccessor->getValue($oldEntity, $field) != $this->propertyAccessor->getValue($newEntity, $field))) {
                $changedFields[] = $field;
            }
        }

        foreach ($associationMappings as $field => $mapping) {
            if (ClassMetadata::ONE_TO_MANY == $mapping['type']) {
                continue;
            } elseif (ClassMetadata::MANY_TO_MANY == $mapping['type']) {
                if ($form->has($field)) { // A ManyToMany relationship is not necessarily on this side
                    $newEntities = $this->submissionFormHandler->entitiesToIds($this->propertyAccessor->getValue($newEntity, $field));

                    if (null === $oldEntity) {
                        $changedFields[] = $field;
                    } else {
                        $oldEntities = $this->submissionFormHandler->entitiesToIds($this->propertyAccessor->getValue($oldEntity, $field));
                        if (!empty(array_diff($oldEntities, $newEntities)) || !empty(array_diff($newEntities, $oldEntities))) {
                            $changedFields[] = $field;
                        }
                    }
                }
            } elseif (ClassMetadata::MANY_TO_ONE == $mapping['type']) {
                $newValue = $this->propertyAccessor->getValue($newEntity, $field);
                if (null === $oldEntity) {
                    $changedFields[] = $field;
                } else {
                    $oldValue = $this->propertyAccessor->getValue($oldEntity, $field);
                    if (null === $oldValue || null === $newValue) {
                        if (null !== $oldValue || null !== $newValue) {
                            $changedFields[] = $field;
                        }
                    } else {
                        if ($oldValue->getId() != $newValue->getId()) {
                            $changedFields[] = $field;
                        }
                    }
                }
            } else {
                throw new Exception('Unexpected field type');
            }
        }

        return $changedFields;
    }

    /**
     * Returns the default pending entity object when adding a suggestion.
     * @param null $parameter
     * @return PendingInterface|null
     */
    public function getDefaultEntity($parameter = null): ?PendingInterface
    {
        $pendingEntityClass = $this->getPendingEntityName();

        return new $pendingEntityClass();
    }
}

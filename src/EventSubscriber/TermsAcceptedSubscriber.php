<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Bundle\SecurityBundle\Security;
use Twig\Environment;

/**
 * Class TermsAcceptedSubscriber.
 */
class TermsAcceptedSubscriber implements EventSubscriberInterface
{
    /**
     * MaintenanceSubscriber constructor.
     */
    public function __construct(private readonly ParameterBagInterface $params, private readonly Environment $twig, private readonly Security $security, private readonly EntityManagerInterface $entityManager)
    {
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        $events = [];
        $events[KernelEvents::CONTROLLER][] = ['onKernelControllerTermsAccepted'];

        return $events;
    }

    /**
     * Returns an "accept terms" page if last accepted terms are too old.
     *
     * @param ControllerEvent $event the event to process
     */
    public function onKernelControllerTermsAccepted(ControllerEvent $event)
    {
        if (null !== $this->security->getUser()) {
            if (!$this->params->has('latest_privacy') || !$this->params->has('latest_cla')) {
                return;
            }

            // We give access to the user to the privacy policy and CLA pages so he can read them before approving
            $attributes = $event->getRequest()->attributes;
            if ($attributes->has('_route') && ('privacy' === $attributes->get('_route') || 'cla' === $attributes->get('_route'))) {
                return;
            }

            $latest_privacy = $this->params->get('latest_privacy');
            $latest_cla = $this->params->get('latest_cla');
            $user = $this->security->getUser();
            if ($user->getAcceptedCla() < $latest_cla || $user->getAcceptedPrivacy() < $latest_privacy) {
                $request = $event->getRequest()->request;
                if ($request->has('accepted_privacy') && intval($request->get('accepted_privacy')) === $latest_privacy && intval($request->get('accepted_privacy')) !== $user->getAcceptedPrivacy()) {
                    $user->setAcceptedPrivacy($latest_privacy);
                }
                if ($request->has('accepted_cla') && intval($request->get('accepted_cla')) === $latest_cla && intval($request->get('accepted_cla')) !== $user->getAcceptedCla()) {
                    $user->setAcceptedCla($latest_cla);
                }
                $this->entityManager->flush();

                // We check again now in case form was submitted and user data changed
                if ($user->getAcceptedCla() < $latest_cla || $user->getAcceptedPrivacy() < $latest_privacy) {
                    $event->setController(
                        fn() => new Response($this->twig->render('user/acceptedTerms.html.twig'))
                    );
                }
            }
        }
    }
}

<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\EventSubscriber;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Bundle\SecurityBundle\Security;

/**
 * Class LastLoginSubscriber.
 */
class LastLoginSubscriber implements EventSubscriberInterface
{
    /**
     * MaintenanceSubscriber constructor.
     */
    public function __construct(private readonly Security $security, private readonly EntityManagerInterface $entityManager)
    {
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        $events = [];
        $events[KernelEvents::CONTROLLER][] = ['onKernelControllerLastLogin'];

        return $events;
    }

    /**
     * Returns an "accept terms" page if last accepted terms are too old.
     *
     * @param ControllerEvent $event the event to process
     */
    public function onKernelControllerLastLogin(ControllerEvent $event)
    {
        if (null !== $this->security->getUser()) {
            $user = $this->security->getUser();
            $now = new DateTime();
            // Only update once every 5 minutes, that's more than enough
            if ($user->getLastLogin() === null || $user->getLastLogin()->getTimestamp() < $now->getTimestamp() - 300) {
                $user->setLastLogin($now);
                $this->entityManager->flush();
            }
        }
    }
}

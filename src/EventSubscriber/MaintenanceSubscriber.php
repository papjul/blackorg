<?php

declare(strict_types=1);

/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\EventSubscriber;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Bundle\SecurityBundle\Security;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class MaintenanceSubscriber.
 * Parts of this class are from Drupal Project under compatible GPLv2+ license.
 */
class MaintenanceSubscriber implements EventSubscriberInterface
{
    /**
     * MaintenanceSubscriber constructor.
     */
    public function __construct(private readonly ParameterBagInterface $params, private readonly Environment $twig, private readonly Security $security)
    {
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        $events = [];
        $events[KernelEvents::REQUEST][] = ['onKernelRequestMaintenance', 30];
        $events[KernelEvents::EXCEPTION][] = ['onKernelRequestMaintenance'];

        return $events;
    }

    /**
     * Returns the site maintenance page if the site is offline.
     *
     * @param RequestEvent $event the event to process
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function onKernelRequestMaintenance(RequestEvent $event)
    {
        $request = $event->getRequest();

        if ($this->params->has('maintenance') && $this->params->get('maintenance')) {
            if (null !== $this->security->getToken() && $this->security->isGranted('ROLE_SUPER_ADMIN')) {
                $request->getSession()->getFlashBag()->add('notice', 'Black Org est actuellement en mode de maintenance, pense à désactiver ce mode quand tu as terminé.');
            } else {
                if (!$request->attributes->has('_maintenance_access') || !$request->attributes->get('_maintenance_access')) {
                    $response = new Response($this->twig->render('maintenance.html.twig'));
                    $response->setStatusCode(503);
                    $event->setResponse($response);
                }
            }
        }
    }
}

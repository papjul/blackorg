<?php

declare(strict_types=1);
/**
 * This file is part of Black Org.
 *
 * Black Org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Black Org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Tests;

use Generator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ApplicationAvailabilityFunctionalTest.
 */
class ApplicationAvailabilityFunctionalTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful(string $url): void
    {
        $client = static::createClient();
        $client->request('GET', $url);

        $this->assertResponseIsSuccessful();
    }

    /**
     * @dataProvider urlAdminProvider
     */
    public function testAdminPageIsSuccessful(string $url): void
    {
        $client = static::createClient();
        $client->request('GET', $url);

        $this->assertResponseIsSuccessful();
    }

    /**
     * @dataProvider urlRedirectProvider
     */
    public function testPageRedirects(string $url): void
    {
        $client = static::createClient();
        $client->request('GET', $url);

        $this->assertResponseRedirects();
    }

    public function urlRedirectProvider(): Generator
    {
        yield ['/episodes/season/1'];
    }

    public function urlAdminProvider(): Generator
    {
        yield ['/admin'];
        yield ['/admin/albums'];
        yield ['/admin/cases'];
        yield ['/admin/chapters'];
        yield ['/admin/characters'];
        yield ['/admin/dvds'];
        yield ['/admin/episodes'];
        yield ['/admin/gadgets'];
        yield ['/admin/situations'];
        yield ['/admin/tracks'];
        yield ['/admin/volumes'];
        yield ['/admin/submissions'];
        yield ['/admin/albums/add'];
        yield ['/admin/cases/add'];
        yield ['/admin/cases/add/manga'];
        yield ['/admin/cases/add/anime'];
        yield ['/admin/cases/add/movie'];
        yield ['/admin/cases/add/movie_special'];
        yield ['/admin/cases/add/ova'];
        yield ['/admin/cases/add/magic_file'];
        yield ['/admin/cases/add/dc_special'];
        yield ['/admin/cases/add/mk_special'];
        yield ['/admin/cases/add/mk1412'];
        yield ['/admin/cases/add/tanpenshu'];
        yield ['/admin/cases/add/drama_special'];
        yield ['/admin/cases/add/drama_episode'];
        yield ['/admin/cases/add/wps'];
        yield ['/admin/chapters/add'];
        yield ['/admin/chapters/add/original'];
        yield ['/admin/chapters/add/tokubetsu'];
        yield ['/admin/chapters/add/movie'];
        yield ['/admin/chapters/add/wps'];
        yield ['/admin/characters/add'];
        yield ['/admin/dvds/add'];
        yield ['/admin/episodes/add'];
        yield ['/admin/gadgets/add'];
        yield ['/admin/situations/add'];
        yield ['/admin/situations/add/1'];
        yield ['/admin/tracks/add'];
        yield ['/admin/tracks/add/1'];
        yield ['/admin/tracks/add/2'];
        yield ['/admin/tracks/add/3'];
        yield ['/admin/tracks/add/4'];
        yield ['/admin/volumes/add'];
        yield ['/admin/volumes/add/original'];
        yield ['/admin/volumes/add/tokubetsu'];
        yield ['/admin/volumes/add/movie'];
        yield ['/admin/volumes/add/wps'];
        yield ['/admin/albums/edit/1']; // TODO: Replace with fixtures
        yield ['/admin/cases/edit/1'];
        yield ['/admin/chapters/edit/1'];
        yield ['/admin/characters/edit/1'];
        yield ['/admin/dvds/edit/1'];
        yield ['/admin/episodes/edit/1'];
        yield ['/admin/gadgets/edit/1'];
        yield ['/admin/situations/edit/1'];
        yield ['/admin/tracks/edit/1'];
        yield ['/admin/volumes/edit/1'];
        //yield ['/admin/submissions/view/1']; // TODO: Replace with fixtures
        //yield ['/admin/submissions/valid/1']; // TODO: Replace with fixtures
    }

    public function urlProvider(): Generator
    {
        yield ['/'];

        // Anime
        yield ['/episodes'];
        yield ['/episodes/1'];
        yield ['/episodes/page/1'];
        yield ['/movies'];
        yield ['/movies/1'];
        yield ['/movies/1sp'];
        yield ['/specials'];
        yield ['/specials/ova1'];
        yield ['/specials/mf1'];
        yield ['/dvds'];
        yield ['/dvds/1'];

        // Cases
        yield ['/cases'];
        yield ['/cases/arc/1'];
        yield ['/cases/arc/all'];
        yield ['/cases/1'];

        // Characters
        yield ['/characters'];
        yield ['/characters/Shinichi_Kudo'];
        yield ['/characters/Shinichi_Kudo/appearances'];
        yield ['/gadgets'];
        yield ['/gadgets/1'];
        yield ['/gadgets/1/uses'];
        yield ['/org'];
        yield ['/relationships'];

        // Contact
        yield ['/contact'];

        // Drama
        yield ['/drama'];

        // Event
        yield ['/news'];
        yield ['/news/2015/1'];

        // Manga
        yield ['/chapters'];
        yield ['/chapters/tokubetsu'];
        yield ['/chapters/movie'];
        yield ['/chapters/wps'];
        yield ['/volumes'];
        yield ['/volumes/1'];
        yield ['/volumes/tokubetsu'];
        yield ['/volumes/tokubetsu/1'];
        yield ['/volumes/movie'];
        yield ['/volumes/movie/1'];
        yield ['/volumes/wps'];
        yield ['/volumes/wps/1'];

        // Music
        yield ['/music'];
        yield ['/music/4'];

        // Pages
        yield ['/anime'];
        yield ['/aptx'];
        yield ['/author'];
        yield ['/changelog'];
        yield ['/cla'];
        yield ['/contact'];
        yield ['/contributing'];
        yield ['/faq'];
        yield ['/games'];
        yield ['/links'];
        yield ['/privacy'];
        yield ['/progress'];
        yield ['/sitemap'];
        yield ['/stats'];

        // Search
        yield ['/search'];

        // Security
        yield ['/user/login'];
        yield ['/user/register'];
    }
}

# Black Org

Source code for website Black Org https://black-org.fr/


# Installation

First, clone this repo.

After setting up the `.env` file, initialize the database:
```
php bin/console doctrine:schema:create
```

At the moment, there is no plan to open source the data, so you will have to initialize data by yourself.


# Installation or update

If database was modified (rarely happens):
```
php bin/console doctrine:schema:create
```
If needed, add the `--force` argument.

Then execute:
```
composer install
npm install
```

Then execute Webpack Encore (options are `dev` or `prod`, and you can pass the `--watch` argument to listen to changes):
```
npx encore dev
```

# Tests

Currently, tests are very basic, they check that pages are correctly loading.

It is better than nothing:
```
php bin/phpunit
```
# Maintenance

For developers.


# Update Compose

Check `composer.json` for updates and run:
```
composer update
```

Then check recipes that can be updated:
```
composer recipes
```

Then, for each package to update:
```
composer recipes:update vendor/packages
```

Commit between each package to update.


# Update NPM

Check `packages.json` for updates and run:
```
npm update
```

Then, run:
```
npx encore dev
```

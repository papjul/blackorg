import {Controller} from '@hotwired/stimulus';

import "chartjs-adapter-date-fns";
import {fr} from "date-fns/locale";

export default class extends Controller {
    connect() {
        this.element.addEventListener("chartjs:pre-connect", this._onPreConnect);
    }

    disconnect() {
        // You should always remove listeners when the controller is disconnected to avoid side effects
        this.element.removeEventListener("chartjs:pre-connect", this._onPreConnect);
    }

    _onPreConnect(event) {
        event.detail.options.scales = {
            x: {
                adapters: {
                    date: {
                        locale: fr
                    }
                },
                type: "time",
                time: {
                    unit: "year"
                    //parser: "dd/MM/yyyy", // FIXME: Doesn't display this way in the tooltip, maybe use plugins tooltip when it works?
                }
            }
        };
    }
}
import {Controller} from '@hotwired/stimulus';

export default class extends Controller {
    connect() {
        this.element.addEventListener("chartjs:pre-connect", this._onPreConnect);
    }

    disconnect() {
        // You should always remove listeners when the controller is disconnected to avoid side effects
        this.element.removeEventListener("chartjs:pre-connect", this._onPreConnect);
    }

    _onPreConnect(event) {
        event.detail.options.plugins = {
            tooltip: {
                callbacks: {
                    label: function (context) {
                        return "Épisode " + context.parsed.x + " : " + context.parsed.y.toString().replace('.', ',') + " %";
                    }
                }
            }
        }
        event.detail.options.scales = {
            y: {
                beginAtZero: true,
                ticks: {
                    callback: function(value, index, ticks) {
                        return value + " %";
                    }
                }
            }
        };
    }
}
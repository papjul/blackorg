import './styles/app.scss';

// Bootstrap theme
import 'bootstrap/js/src/alert.js';
import 'bootstrap/js/src/collapse.js';
import 'bootstrap/js/src/dropdown.js';
import 'bootstrap/js/src/modal.js';

// Bootstrap theme
import Tooltip from 'bootstrap/js/dist/tooltip';

// Enable Bootstrap tooltips
const tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
tooltipTriggerList.map((tooltipTriggerEl) => {
    return new Tooltip(tooltipTriggerEl)
})

const themeSwitcher = document.getElementById('theme-switcher');
if (themeSwitcher) {
    themeSwitcher.addEventListener('click', (e) => {
        e.preventDefault();

        let theme;
        if (document.getElementsByTagName("html")[0].dataset.bsTheme == "light") {
            document.getElementsByTagName("html")[0].dataset.bsTheme = "dark";
            theme = 'dark';
        } else {
            document.getElementsByTagName("html")[0].dataset.bsTheme = "light";
            theme = 'light';
        }
        let xhttp = new XMLHttpRequest();
        xhttp.open('POST', '/ajax/theme/' + theme, true);
        xhttp.send();
    });
}

// start the Stimulus application
import './bootstrap.js';
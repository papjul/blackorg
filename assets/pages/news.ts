const changeMonth = () => {
    let monthSelector = document.getElementById('m') as HTMLSelectElement;
    let month = (monthSelector[monthSelector.selectedIndex] as HTMLOptionElement)?.value;
    let yearSelector = document.getElementById('y') as HTMLSelectElement;
    let year = (yearSelector[yearSelector.selectedIndex] as HTMLOptionElement)?.value;
    window.location.href = '/news/' + year + '/' + month;
}

document.getElementById('m')?.addEventListener("change", changeMonth);
document.getElementById('y')?.addEventListener("change", changeMonth);
document.getElementById('submit')?.addEventListener("click", changeMonth);
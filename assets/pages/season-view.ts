function changeSeasonAuto(this: HTMLSelectElement) {
    window.location.href = '/episodes/page/' + this.options[this.selectedIndex].value;
}

const seasonOptions = document.getElementsByClassName('chg_season') as HTMLCollectionOf<HTMLOptionElement>;
for (const seasonSelector of seasonOptions) {
    seasonSelector.addEventListener('change', changeSeasonAuto);
}
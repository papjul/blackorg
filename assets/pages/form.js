import "../styles/form.scss";
import "./form.ts";

// TODO: Migrate to TypeScript (types does not exist)
import EasyMDE from "easymde/dist/easymde.min.js";
import Choices from "choices.js";
import "choices.js/public/assets/styles/choices.min.css";

/* EasyMDE */
const easyMdeTextareas = document.getElementsByClassName("easymde-textarea");
for (const easymdeTextarea of easyMdeTextareas) {
    new EasyMDE({
        autoDownloadFontAwesome: false,
        spellChecker: false,
        //forceSync: true, // Needed to have required field not blocking the submit button
        element: easymdeTextarea
    });
}

/* Choices.js fields */
let options = {
    removeItemButton: true,
    shouldSort: false,
    placeholderValue: "Appuyer pour voir des choix",
    loadingText: "Chargement…",
    noResultsText: "Aucun résultat trouvé",
    noChoicesText: "Aucun choix à proposer",
    itemSelectText: "",
    addItemText: (value) => {
        return `Appuyer sur Entrée pour ajouter <strong>"${value}"</strong>`;
    },
};

// TODO: Remove icon missing
function callbackOnCreateTemplates(template) {
    const {itemSelectText} = this.config;
    return {
        item: ({ classNames }, data) => {
            let label;
            if (data.label.includes("|")) {
                const info = data.label.split("|");
                label = "<img src=\"" + String(info[0]) + "\" alt=\"test\" width=\"20\" height=\"20\" /> " + String(info[1]);
            } else {
                label = String(data.label);
            }
            return template(
                "\
            <div\
              class=\"" +
                String(classNames.item) +
                " " +
                String(
                    data.highlighted
                        ? classNames.highlightedState
                        : classNames.itemSelectable
                ) +
                "\"\
            data-item\
            data-id=\"" +
                String(data.id) +
                "\"\
            data-value=\"" +
                String(data.value) +
                "\"\
            " +
                String(data.active ? "aria-selected=\"true\"" : "") +
                "\
            " +
                String(data.disabled ? "aria-disabled=\"true\"" : "") +
                "\
            >" +
                label +
                "\
          </div>\
        "
            );
        },
        choice: ({ classNames }, data) => {
            let label;
            if (data.label.includes("|")) {
                const info = data.label.split("|");
                label = "<img src=\"" + String(info[0]) + "\" alt=\"\" /> " + String(info[1]);
            } else {
                label = String(data.label);
            }
            return template(
                "\
            <div\
              class=\"" +
                String(classNames.item) +
                " " +
                String(classNames.itemChoice) +
                " " +
                String(
                    data.disabled
                        ? classNames.itemDisabled
                        : classNames.itemSelectable
                ) +
                "\"\
            data-select-text=\"" +
                String(itemSelectText) +
                "\"\
            data-choice \
            " +
                String(
                    data.disabled
                        ? "data-choice-disabled aria-disabled=\"true\""
                        : "data-choice-selectable"
                ) +
                "\
            data-id=\"" +
                String(data.id) +
                "\"\
            data-value=\"" +
                String(data.value) +
                "\"\
            " +
                String(
                    data.groupId > 0 ? "role=\"treeitem\"" : "role=\"option\""
                ) +
                "\
            >" +
                label +
                "\
          </div>\
        "
            );
        },
    };
};
if (document.getElementById("case_form_gadgets")) {
    new Choices(document.getElementById("case_form_gadgets"), {
        ...options, allowHTML: true,
        callbackOnCreateTemplates: callbackOnCreateTemplates,
    });
}
if (document.getElementById("case_form_characters")) {
    new Choices(document.getElementById("case_form_characters"), {
        ...options, allowHTML: true,
        callbackOnCreateTemplates: callbackOnCreateTemplates,
    });
}
if (document.getElementById("chapter_form_case")) {
    new Choices(document.getElementById("chapter_form_case"), {...options, allowHTML: false});
}
if (document.getElementById("character_form_first_appearance")) {
    new Choices(document.getElementById("character_form_first_appearance"), {...options, allowHTML: false});
}
if (document.getElementById("episode_form_cases")) {
    new Choices(document.getElementById("episode_form_cases"), {...options, allowHTML: false});
}
if (document.getElementById("episode_form_opening")) {
    new Choices(document.getElementById("episode_form_opening"), {...options, allowHTML: false});
}
if (document.getElementById("episode_form_ending")) {
    new Choices(document.getElementById("episode_form_ending"), {...options, allowHTML: false});
}
if (document.getElementById("gadget_form_created_by")) {
    new Choices(document.getElementById("gadget_form_created_by"), {
        ...options, allowHTML: true,
        callbackOnCreateTemplates: callbackOnCreateTemplates,
    });
}
if (document.getElementById("gadget_form_first_use")) {
    new Choices(document.getElementById("gadget_form_first_use"), {...options, allowHTML: false});
}
if (document.getElementById("gadget_form_used_by")) {
    new Choices(document.getElementById("gadget_form_used_by"), {
        ...options, allowHTML: true,
        callbackOnCreateTemplates: callbackOnCreateTemplates,
    });
}
if (document.getElementById("situation_form_case")) {
    new Choices(document.getElementById("situation_form_case"), {...options, allowHTML: false});
}
if (document.getElementById("situation_form_types")) {
    new Choices(document.getElementById("situation_form_types"), {...options, allowHTML: false});
}
if (document.getElementById("situation_form_suspects")) {
    new Choices(document.getElementById("situation_form_suspects"), options);
}
if (document.getElementById("situation_form_victims")) {
    new Choices(document.getElementById("situation_form_victims"), options);
}
if (document.getElementById("situation_form_culprits")) {
    new Choices(document.getElementById("situation_form_culprits"), options);
}
if (document.getElementById("search_form_plot")) {
    new Choices(document.getElementById("search_form_plot"), {...options, allowHTML: false});
}
if (document.getElementById("search_form_type")) {
    new Choices(document.getElementById("search_form_type"), {...options, allowHTML: false});
}
if (document.getElementById("search_form_arc")) {
    new Choices(document.getElementById("search_form_arc"), {...options, allowHTML: false});
}
if (document.getElementById("search_form_gadgets")) {
    new Choices(document.getElementById("search_form_gadgets"), {
        ...options, allowHTML: true,
        callbackOnCreateTemplates: callbackOnCreateTemplates,
    });
}
if (document.getElementById("search_form_characters")) {
    new Choices(document.getElementById("search_form_characters"), {
        ...options, allowHTML: true,
        callbackOnCreateTemplates: callbackOnCreateTemplates,
    });
}
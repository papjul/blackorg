import {Types} from "choices.js/src/scripts/interfaces/types";

function checkCheckboxFromFieldset(this: any, event: any) {
    event.stopPropagation();
    event.preventDefault();
    for (const checkbox of this.parentNode.parentNode.getElementsByTagName("input")) {
        checkbox.checked = true;
    }
    return false;
}

function uncheckCheckboxFromFieldset(this: any, event: any) {
    event.stopPropagation();
    event.preventDefault();
    for (const checkbox of this.parentNode.parentNode.getElementsByTagName("input")) {
        checkbox.checked = false;
    }
    return false;
}

const checkAllCheckboxesButtons = document.getElementsByClassName("checkAllCheckboxesButton");
for (const checkAllCheckboxesButton of checkAllCheckboxesButtons) {
    checkAllCheckboxesButton.addEventListener("click", checkCheckboxFromFieldset);
}

const uncheckAllCheckboxesButtons = document.getElementsByClassName("uncheckAllCheckboxesButton");
for (const uncheckAllCheckboxesButton of uncheckAllCheckboxesButtons) {
    uncheckAllCheckboxesButton.addEventListener("click", uncheckCheckboxFromFieldset);
}

const chapterFormVolume = document.getElementById("chapter_form_volume") as HTMLSelectElement;
if (chapterFormVolume) {
    if (chapterFormVolume.selectedIndex === 0) {
        (document.getElementById("chapter_form_volume_number") as HTMLSelectElement).selectedIndex = 0;
        (document.getElementById("chapter_form_volume_number") as HTMLInputElement).disabled = true;
    }
    chapterFormVolume.onchange = function () {
        if (chapterFormVolume.selectedIndex === 0) {
            (document.getElementById("chapter_form_volume_number") as HTMLSelectElement).selectedIndex = 0;
            (document.getElementById("chapter_form_volume_number") as HTMLInputElement).disabled = true;
        } else {
            (document.getElementById("chapter_form_volume_number") as HTMLInputElement).disabled = false;
            (document.getElementById("chapter_form_volume_number") as HTMLSelectElement).selectedIndex = 1;
        }
    };
}

const episodeFormPart = document.getElementById("episode_form_part") as HTMLSelectElement;
if (episodeFormPart) {
    if (episodeFormPart.selectedIndex === 0) {
        (document.getElementById("episode_form_part_title") as HTMLInputElement).value = "";
        (document.getElementById("episode_form_part_title") as HTMLInputElement).disabled = true;
    }
    episodeFormPart.onchange = function () {
        if (episodeFormPart.selectedIndex === 0) {
            (document.getElementById("episode_form_part_title") as HTMLInputElement).value = "";
            (document.getElementById("episode_form_part_title") as HTMLInputElement).disabled = true;
        } else {
            (document.getElementById("episode_form_part_title") as HTMLInputElement).disabled = false;
        }
    };
}


/* Choices.js fields */
// Unable to make it work with TypeScript
/*import Choices from "choices.js/src/index";
import {TemplateOptions} from "choices.js/src/scripts/interfaces/templates";
import {ChoiceFull} from "choices.js/src/scripts/interfaces/choice-full";

let options = {
    removeItemButton: true,
    shouldSort: false,
    placeholderValue: "Appuyer pour voir des choix",
    loadingText: "Chargement…",
    noResultsText: "Aucun résultat trouvé",
    noChoicesText: "Aucun choix à proposer",
    itemSelectText: "",
    addItemText: (value: string) => {
        return `Appuyer sur Entrée pour ajouter <strong>"${value}"</strong>`;
    },
};

// TODO: Remove icon missing
const callbackOnCreateTemplates = (strToEl: Types.StrToEl) => {
    return {
        item: ({ classNames }: TemplateOptions, data: ChoiceFull, removeItemButton: boolean): HTMLDivElement => {
            let label;
            if (typeof data.label === "string" && data.label.includes("|")) {
                const info = data.label.split("|");
                label = "<img src=\"" + String(info[0]) + "\" alt=\"test\" width=\"20\" height=\"20\" /> " + String(info[1]);
            } else {
                label = String(data.label);
            }
            return strToEl(
                "\
            <div\
              class=\"" +
                String(classNames.item) +
                " " +
                String(
                    data.highlighted
                        ? classNames.highlightedState
                        : classNames.itemSelectable
                ) +
                "\"\
            data-item\
            data-id=\"" +
                String(data.id) +
                "\"\
            data-value=\"" +
                String(data.value) +
                "\"\
            " +
                String(data.active ? "aria-selected=\"true\"" : "") +
                "\
            " +
                String(data.disabled ? "aria-disabled=\"true\"" : "") +
                "\
            >" +
                label +
                "\
          </div>\
        "
            ) as HTMLDivElement;
        },
        choice: ({ classNames }: TemplateOptions, data: ChoiceFull, selectText: string): HTMLOptionElement => {
            let label;
            if (typeof data.label === "string" && data.label.includes("|")) {
                const info = data.label.split("|");
                label = "<img src=\"" + String(info[0]) + "\" alt=\"test\" /> " + String(info[1]);
            } else {
                label = String(data.label);
            }
            return strToEl(
                "\
            <div\
              class=\"" +
                String(classNames.item) +
                " " +
                String(classNames.itemChoice) +
                " " +
                String(
                    data.disabled
                        ? classNames.itemDisabled
                        : classNames.itemSelectable
                ) +
                "\"\
            data-select-text=\"" +
                String(selectText) +
                "\"\
            data-choice \
            " +
                String(
                    data.disabled
                        ? "data-choice-disabled aria-disabled=\"true\""
                        : "data-choice-selectable"
                ) +
                "\
            data-id=\"" +
                String(data.id) +
                "\"\
            data-value=\"" +
                String(data.value) +
                "\"\
            " +
                String(
                    data.group != null && data.group.id > 0 ? "role=\"treeitem\"" : "role=\"option\""
                ) +
                "\
            >" +
                label +
                "\
          </div>\
        "
            ) as HTMLOptionElement;
        },
    };
}


if (document.getElementById("case_form_gadgets")) {
    new Choices(document.getElementById("case_form_gadgets") as HTMLSelectElement, {
        ...options, allowHTML: true,
        // @ts-ignore
        callbackOnCreateTemplates: callbackOnCreateTemplates,
    });
}
if (document.getElementById("case_form_characters")) {
    new Choices(document.getElementById("case_form_characters") as HTMLSelectElement, {
        ...options, allowHTML: true,
        // @ts-ignore
        callbackOnCreateTemplates: callbackOnCreateTemplates,
    });
}
if (document.getElementById("chapter_form_case")) {
    new Choices(document.getElementById("chapter_form_case") as HTMLSelectElement, {...options, allowHTML: false});
}
if (document.getElementById("character_form_first_appearance")) {
    new Choices(document.getElementById("character_form_first_appearance") as HTMLSelectElement, {...options, allowHTML: false});
}
if (document.getElementById("episode_form_cases")) {
    new Choices(document.getElementById("episode_form_cases") as HTMLSelectElement, {...options, allowHTML: false});
}
if (document.getElementById("episode_form_opening")) {
    new Choices(document.getElementById("episode_form_opening") as HTMLSelectElement, {...options, allowHTML: false});
}
if (document.getElementById("episode_form_ending")) {
    new Choices(document.getElementById("episode_form_ending") as HTMLSelectElement, {...options, allowHTML: false});
}
if (document.getElementById("gadget_form_created_by")) {
    new Choices(document.getElementById("gadget_form_created_by") as HTMLSelectElement, {
        ...options, allowHTML: true,
        // @ts-ignore
        callbackOnCreateTemplates: callbackOnCreateTemplates,
    });
}
if (document.getElementById("gadget_form_first_use")) {
    new Choices(document.getElementById("gadget_form_first_use") as HTMLSelectElement, {...options, allowHTML: false});
}
if (document.getElementById("gadget_form_used_by")) {
    new Choices(document.getElementById("gadget_form_used_by") as HTMLSelectElement, {
        ...options, allowHTML: true,
        // @ts-ignore
        callbackOnCreateTemplates: callbackOnCreateTemplates,
    });
}
if (document.getElementById("situation_form_case")) {
    new Choices(document.getElementById("situation_form_case") as HTMLSelectElement, {...options, allowHTML: false});
}
if (document.getElementById("situation_form_types")) {
    new Choices(document.getElementById("situation_form_types") as HTMLSelectElement, {...options, allowHTML: false});
}
if (document.getElementById("situation_form_suspects")) {
    new Choices(document.getElementById("situation_form_suspects") as HTMLSelectElement, options);
}
if (document.getElementById("situation_form_victims")) {
    new Choices(document.getElementById("situation_form_victims") as HTMLSelectElement, options);
}
if (document.getElementById("situation_form_culprits")) {
    new Choices(document.getElementById("situation_form_culprits") as HTMLSelectElement, options);
}
if (document.getElementById("search_form_plot")) {
    new Choices(document.getElementById("search_form_plot") as HTMLSelectElement, {...options, allowHTML: false});
}
if (document.getElementById("search_form_type")) {
    new Choices(document.getElementById("search_form_type") as HTMLSelectElement, {...options, allowHTML: false});
}
if (document.getElementById("search_form_arc")) {
    new Choices(document.getElementById("search_form_arc") as HTMLSelectElement, {...options, allowHTML: false});
}
if (document.getElementById("search_form_gadgets")) {
    new Choices(
        document.getElementById("search_form_gadgets") as HTMLSelectElement,
        {
            ...options,
            allowHTML: true,
            // @ts-ignore
            callbackOnCreateTemplates: callbackOnCreateTemplates,
        }
    );
}
if (document.getElementById("search_form_characters")) {
    new Choices(document.getElementById("search_form_characters") as HTMLSelectElement,
        {
            ...options,
            allowHTML: true,
            // @ts-ignore
            callbackOnCreateTemplates: callbackOnCreateTemplates,
        }
    );
}*/

const updatePlotCases = () => {
    const cases = document.getElementById('plotTable')?.getElementsByTagName('tbody')[0]?.getElementsByTagName('tr') as HTMLCollectionOf<HTMLTableRowElement>;

    if (cases) {
        const essentialChecked: boolean = (document.getElementById('essentialCheckbox') as HTMLInputElement)?.checked;
        const recommendedChecked: boolean = (document.getElementById('recommendedCheckbox') as HTMLInputElement)?.checked;
        const minorChecked: boolean = (document.getElementById('minorCheckbox') as HTMLInputElement)?.checked;
        const nonCanonChecked: boolean = (document.getElementById('nonCanonCheckbox') as HTMLInputElement)?.checked;

        const arcCheckboxes = document.getElementsByName('arcCheckboxes[]') as NodeListOf<HTMLInputElement>;
        let arcClasses: string[] = [];
        for (const arcCheckbox of arcCheckboxes) {
            if (arcCheckbox.checked) {
                arcClasses.push('arc' + arcCheckbox.value);
            }
        };

        for (const caseEl of cases) {
            let display = '';
            for (const caseClass of caseEl.classList) {
                if (caseClass.startsWith("arc")) {
                    if (arcClasses.includes(caseClass)) {
                        break;
                    } else {
                        display = 'hide';
                    }
                }
            }
            if (display === 'hide') {
                caseEl.style.display = 'none';
                continue;
            }
            if (!nonCanonChecked && caseEl.classList.contains('noncanon')) {
                caseEl.style.display = 'none';
                continue;
            }
            if (essentialChecked && caseEl.classList.contains('essential')) {
                caseEl.style.display = 'table-row';
                continue;
            }
            if (recommendedChecked && caseEl.classList.contains('recommended')) {
                caseEl.style.display = 'table-row';
                continue;
            }
            if (minorChecked && caseEl.classList.contains('minor')) {
                caseEl.style.display = 'table-row';
                continue;
            }
            caseEl.style.display = 'none';
        }
    }
}

let arcCheckboxes = document.getElementsByName('arcCheckboxes[]');
arcCheckboxes.forEach((arcCheckbox) => {
    arcCheckbox.addEventListener('change', updatePlotCases);
});
document.getElementById('essentialCheckbox')?.addEventListener('change', updatePlotCases);
document.getElementById('recommendedCheckbox')?.addEventListener('change', updatePlotCases);
document.getElementById('minorCheckbox')?.addEventListener('change', updatePlotCases);
document.getElementById('nonCanonCheckbox')?.addEventListener('change', updatePlotCases);

updatePlotCases();

const showHideNonCanonCases = () => {
    const checked: boolean = (document.getElementById('hide_noncanon') as HTMLInputElement)?.checked;
    const nonCanonCases = document.getElementsByClassName('noncanon') as HTMLCollectionOf<HTMLElement>;
    for (const caseElement of nonCanonCases) {
        if (checked) {
            caseElement.style.display = 'none';
        } else {
            caseElement.style.removeProperty('display');
        }
    }
}

document.getElementById('hide_noncanon')?.addEventListener('click', showHideNonCanonCases);